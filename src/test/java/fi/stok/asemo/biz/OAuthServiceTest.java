/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.OAuthAccessCredentials;
import fi.stok.asemo.biz.ejb.entity.schema.OAuthAccessor;
import fi.stok.asemo.biz.ejb.entity.schema.OAuthClient;
import fi.stok.asemo.biz.ejb.entity.schema.OAuthRequestCredentials;
import javax.ejb.EJB;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

/**
 *
 * @author Juha Loukkola
 */
@UsingDataSet
public class OAuthServiceTest extends org.jboss.arquillian.testng.Arquillian {

    @EJB
    private OAuthService service;

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(OAuthAccessor.class)
                .addClass(OAuthRequestCredentials.class)
                .addClass(OAuthAccessCredentials.class)
                .addClass(OAuthClient.class)
                .addClass(OAuthService.class)
                .addPackage("net.oath")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

//    @Test(enabled = false, groups = { "arquillian" })
//    @ShouldMatchDataSet("datasets/expected-fi.stok.asemo.biz.OAuthServiceTest#testSaveAccessor.yml")
//    public void testSaveAccessor() throws OAuthException {
//        OAuthAccessor peristentAccessor = new OAuthAccessor();
//        peristentAccessor.setName("testAccessor");
//        peristentAccessor.setClient(service.getClientByToken("token"));
//        peristentAccessor.setRequestCredentials(
//                service.generateRequestCredentials(peristentAccessor.getClient().getToken()));
//        service.saveAccessor(peristentAccessor);
//    }

}
