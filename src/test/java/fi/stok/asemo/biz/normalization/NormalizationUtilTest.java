/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.LogLevels;
import fi.stok.asemo.biz.ejb.entity.schema.DoubleValue;
import fi.stok.asemo.biz.ejb.entity.schema.NumericValue;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

/**
 *
 * @author Juha Loukkola
 */
public class NormalizationUtilTest {
    
    /**
     * Test of durationWeightedAverage method, of class NormalizationUtil.
     * 1. One value at the start of the interval.
     * 2. One value before the start of the interval.
     * 3. One value at the start of the interval and one at the end.
     * 4. One value before, one value within and one value after the interval.
     * 5. One value after the interval.
     * 6. One value before the interval and one after.
     * 7. One value before, one value within the interval.
     * 8. Start and end are same.
     * 9. Date set is empty.
     * 10. A common case.
     * 
     * E1. Unordered data set.
     */
    @Test(groups = {"unit_tests"})
    public void testDurationWeightedAverage() throws Exception {
        System.out.println("durationWeightedAverage");

        long startTime = new Date().getTime();
        
        Number[][] values =     {{3},   {3},    {3,-3}, {1,2,3},    {3},    {1,2},  {3, -2},    {1,2,3},    {},     {1,2,-3,4,-5,5,6}};
        long [][] timeOffsets = {{0},   {-1},   {0,5},  {-5,3,10},  {7},    {-2,7}, {-3, 2},    {-5,3,10},  {},     {-2000,1000,6000,11000,15000,20000,26000}};
        long endTimeOffsets []= {5,     5,      5,      5,          5,      5,       5,         0,          5,      30000};
        Number expResults [] =  {3.0,  null,    3.0,    7.0/5.0,    null,   null,    0.0,       null,       null,   (1+10-15+16-25+30+24)/30.0};   
        
        for (int i=0; i < expResults.length; i++) {
            System.out.println("\tTest case " + (i+1));
            List<? extends NumericValue> data = this.initializeData(values[i], startTime, timeOffsets[i]);
            Number result = NormalizationUtil.durationWeightedAverage(data, startTime, startTime + endTimeOffsets[i]);
            assertEquals(expResults[i], result);
        }

        // Test that Exception is thrown when data is in wrong order
        boolean thrown = false;
        Number[] valuesE1 = {1.0, 3.0, -1.5, -2.0};
        long[] timeOffsetsE1 = {0, 1000, -3000, 7000};
        try {
            System.out.println("\tTest case E1");
            NormalizationUtil.durationWeightedAverage(
                    this.initializeData(valuesE1, startTime, timeOffsetsE1),
                    startTime, 
                    startTime + 10000);
        } catch (Exception ex) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    /**
     * Test of average method, of class NormalizationUtil.
     */
    @Test(groups = {"unit_tests"})
    public void testAverage() {
        System.out.println("average");
        long startTime = new Date().getTime();
        Number[] values = {1.0, 3.0, -1.5, -2.0};
        long[] timeOffsets = {0, 1000, 3000, 7000};
        List<? extends NumericValue> data = this.initializeData(values, startTime, timeOffsets);
        Number expResult = 0.5 / 4;
        Number result = NormalizationUtil.average(data);
        assertEquals(expResult, result);
    }

    /**
     * Test of arrangeDate method, of class NormalizationUtil.
     */
    @Test(groups = {"unit_tests"})
    public void testArrangeDate() throws Exception {
        Map<Long, Date> resolutionToResultMap = new HashMap<Long, Date>();
        resolutionToResultMap.put(1000l, new GregorianCalendar(2013, 1, 3, 13, 22, 37).getTime());
        resolutionToResultMap.put(5000l, new GregorianCalendar(2013, 1, 3, 13, 22, 35).getTime());
        resolutionToResultMap.put(LogLevels.MIN, new GregorianCalendar(2013, 1, 3, 13, 22).getTime());
        resolutionToResultMap.put(LogLevels.FIFE_MINS, new GregorianCalendar(2013, 1, 3, 13, 20).getTime());
        resolutionToResultMap.put(LogLevels.TEN_MINS, new GregorianCalendar(2013, 1, 3, 13, 20).getTime());
        resolutionToResultMap.put(LogLevels.HOUR, new GregorianCalendar(2013, 1, 3, 13, 0).getTime());
        resolutionToResultMap.put(LogLevels.TREE_HOURS, new GregorianCalendar(2013, 1, 3, 12, 0).getTime());
        resolutionToResultMap.put(LogLevels.TWELVE_HOURS, new GregorianCalendar(2013, 1, 3, 12, 0).getTime());
        resolutionToResultMap.put(LogLevels.DAY, new GregorianCalendar(2013, 1, 3, 0, 0).getTime());

        System.out.println("arrangeDate");
        GregorianCalendar calendar = new GregorianCalendar(2013, 1, 3, 13, 22, 37);
        calendar.set(Calendar.MILLISECOND, 345);
        Long date = calendar.getTime().getTime();
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2");
        NormalizationUtil instance = new NormalizationUtil();
        Date result;

        for (Long resolution : resolutionToResultMap.keySet()) {
            assertEquals(resolutionToResultMap.get(resolution), new Date(instance.arrangeDate(date, resolution, timeZone)));
        }

        // Test that an Exception is thrown when resoluion is not a factor of a minute, a hour or a day
        boolean thrown = false;
        try {
            instance.arrangeDate(date, 7000l, timeZone);
        } catch (Exception ex) {
            thrown = true;
        }
        assertTrue(thrown);

        // Test that an Exception is thrown when resoluion is greater than a day
        thrown = false;
        try {
            instance.arrangeDate(date, LogLevels.DAY + 1, timeZone);
        } catch (Exception ex) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    private <T extends Number> List<NumericValue> initializeData(T[] values, long startTime, long[] timeOffsets) {
        List<NumericValue> data = new ArrayList<NumericValue>();
        for (int i = 0; i < values.length; i++) {
            NumericValue value = new DoubleValue();
            value.setValue(values[i]);
            value.setTimeMeasured(startTime + timeOffsets[i]);
            data.add(value);
        }
        return data;
    }
}