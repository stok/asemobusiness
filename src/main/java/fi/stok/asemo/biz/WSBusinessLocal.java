/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.NumericValue;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.exception.NodeAlreadyRegistered;
import fi.stok.asemo.biz.exception.NodeNotAuthorized;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Juha Loukkola
 */
@Local
public interface WSBusinessLocal {

    // Node Related Functionality
    String registerNode(String mac) throws NodeNotAuthorized, NodeAlreadyRegistered;
    boolean authenticateNode(String mac, String secret);
    void updateNodeStatus(String mac, String ipAddress, Date time, String request, String response, String requestMessageContent);

    // Data Streams Related Functionality
    void storeValue(String id, String time, String value);
    void storeValue(ValueGroup valueGroup, String time, String val);
    void saveValue(Value value);
    void addValues(InputStream csv, ValueGroup vg);
    String storeValues(List<List<String>> values, Instrument instrument, boolean rollBackOnError) throws Exception;
    Value getLastValue(Variable variable);
    Value getLastValue(ValueGroup valueGroup);
    Value getFirstValue(ValueGroup valueGroup);
    Value getFirstValue(Variable variable);
    Value getValueBefere(ValueGroup valueGroup, Date date);
    Value getValueAtOrBefere(ValueGroup valueGroup, Date date);
    List<Value> getValues(ValueGroup valueGroup, Date from, Date to);
    List<NumericValue> getNumericValues(ValueGroup valueGroup, Date from, Date to);
    List<NumericValue> getNumericValuesAfter(ValueGroup valueGroup, Date from, int MaxResults);
    List<NumericValue> getNumericValuesAtOrAfter(ValueGroup valueGroup, Date from, int MaxResults);
    boolean isIntervalEmpty(ValueGroup valueGroup, Date from, Date to);
    
}
