/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.transfer;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "timeSerie")
@XmlAccessorType(XmlAccessType.FIELD)
public class TimeSerie {

    public String uuid; // For R1 use instrument uuid. For R2 use variable uuid
    public Long from;
    public Long to;
    public Long resolution;
}
