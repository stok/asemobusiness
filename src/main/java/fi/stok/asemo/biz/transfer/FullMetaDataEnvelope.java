/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.transfer;

import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class FullMetaDataEnvelope {

    public Customer customer;
    
    @XmlElementWrapper(name = "timeSeries")
    @XmlElement(name = "timeSerie")
    public List<TimeSerie> timeSeries = new ArrayList<TimeSerie>();
}

