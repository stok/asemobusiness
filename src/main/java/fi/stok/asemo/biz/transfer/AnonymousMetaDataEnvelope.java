/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.transfer;

import fi.stok.asemo.biz.ejb.entity.schema.Residence;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public class AnonymousMetaDataEnvelope {

    public Residence residence;
    public List<TimeSerie> timeSeries;

    private class TimeSerie {

        public String uuid; // For R1 use instrument uuid. For R2 use variable uuid
        public Date from;
        public Date to;
        public Long resolution;
    }
}
