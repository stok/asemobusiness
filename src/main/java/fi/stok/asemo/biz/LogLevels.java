/*
 * TODO: These should be dynamically configurable and stored e.g. in DB
 * 
 */
package fi.stok.asemo.biz;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 * 
 * TODO: Properties could be used instead of this class 
 */
public class LogLevels {

    public static final Long INF = new Long(0);
    public static final Long RAW = new Long(1);
    public static final Long SECOND = new Long(1000);
    public static final Long HALF_MIN = new Long(30 * 1000);
    public static final Long MIN = new Long(60 * 1000);
    public static final Long FIFE_MINS = new Long(5 * MIN);
    public static final Long TEN_MINS = new Long(10 * MIN);
    public static final Long FIFTEEN_MINS = new Long(15 * MIN);
    public static final Long TWENTY_MINS = new Long(20 * MIN);
    public static final Long HALF_HOUR = new Long(30 * MIN);
    public static final Long HOUR = new Long(60 * MIN);
    public static final Long TWO_HOURS = new Long(2 * HOUR);
    public static final Long TREE_HOURS = new Long(3 * HOUR);
    public static final Long SIX_HOURS = new Long(6 * HOUR);
    public static final Long TWELVE_HOURS = new Long(12 * HOUR);
    public static final Long DAY = new Long(24 * HOUR);
    public static final Long WEEK = new Long(7 * DAY);
    public static final Long MONTH = new Long(30 * DAY);
    public static final Long TREE_MONTHS = new Long(3 * MONTH);
    public static final Long HALF_YEAR = new Long(6 * MONTH);
    public static final Long YEAR = new Long(12 * MONTH);
    

    public static final List<Long> samplingPeriods = new ArrayList<Long>();
    static {
        samplingPeriods.add(RAW);
        samplingPeriods.add(HALF_MIN);
        samplingPeriods.add(MIN);
        samplingPeriods.add(FIFE_MINS);
        samplingPeriods.add(TEN_MINS);
        samplingPeriods.add(FIFTEEN_MINS);
        samplingPeriods.add(TWENTY_MINS);
        samplingPeriods.add(HALF_HOUR);
        samplingPeriods.add(HOUR);
        samplingPeriods.add(TWO_HOURS);
        samplingPeriods.add(TREE_HOURS);
        samplingPeriods.add(SIX_HOURS);
        samplingPeriods.add(TWELVE_HOURS);
        samplingPeriods.add(DAY);
        samplingPeriods.add(WEEK);
        samplingPeriods.add(MONTH);
    }
    
    public static final List<LogLevel> levels = new ArrayList<LogLevel>();
    static {
        levels.add(new LogLevel(RAW, MONTH, true));
        levels.add(new LogLevel(HALF_MIN, HALF_YEAR, false));
        levels.add(new LogLevel(MIN, YEAR, true));
        levels.add(new LogLevel(FIFE_MINS, INF, true));
        levels.add(new LogLevel(TEN_MINS, INF, false));
        levels.add(new LogLevel(FIFTEEN_MINS, INF, true));
        levels.add(new LogLevel(TWENTY_MINS, INF, false));
        levels.add(new LogLevel(HALF_HOUR, INF, true));
        levels.add(new LogLevel(HOUR, INF, true));
        levels.add(new LogLevel(TWO_HOURS, INF, false));
        levels.add(new LogLevel(TREE_HOURS, INF, true));
        levels.add(new LogLevel(SIX_HOURS, INF, true));
        levels.add(new LogLevel(TWELVE_HOURS, INF, true));
        levels.add(new LogLevel(DAY, INF, true));
        levels.add(new LogLevel(WEEK, INF, false));
        levels.add(new LogLevel(MONTH, INF, false));    
    }
  
    public static class LogLevel {
        private Long samplingPeriod;
        private Long rotationPeriod;
        private boolean enabled;

        public LogLevel(Long samplingPeriod, Long rotationPeriod, boolean enabled) {
            this.samplingPeriod = samplingPeriod;
            this.rotationPeriod = rotationPeriod;
            this.enabled = enabled;
        }

        public Long getSamplingPeriod() {
            return samplingPeriod;
        }

        public Long getRotationPeriod() {
            return rotationPeriod;
        }

        public boolean isEnabled() {
            return enabled;
        }
        
    }
    
}
