/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public interface DataSchemaDAO {

    DataSchema createDataSchema(DataSchema schema);
    
    DataSchema createListDataSchema(String name, String description, List<VariableType> elements);
    
    void createOrUpdateDataSchema(DataSchema schema);

    void deleteDataSchema(String uuid);

    DataSchema getDataSchema(int id);
    
    DataSchema getDataSchema(String uuid);

    Collection<DataSchema> getDataSchemas();
    
    boolean isDataSchemaUuidUnique(String uuid);
}
