/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.ListDataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.ejb.Remove;

/**
 *
 * @author Juha Loukkola
 */
@Stateful
@LocalBean
public class ListDataSchemaEditor {

    @EJB
    private DataSchemaDAO dataSchemaDAO;
    private ListDataSchema schema;
    private List<VariableType> elements;

    // Called when editing is started
    public void init(String uuid) {
        // Load schema
        if (uuid != null) {
            this.schema = (ListDataSchema) this.dataSchemaDAO.getDataSchema(uuid);
            this.elements = this.schema.getElements();
        } else {
            this.schema = new ListDataSchema();
            this.elements = new ArrayList<VariableType>();
        }
    }

    public List<VariableType> getElements() {
        return this.elements;
    }

    public void addElement(VariableType element) {
        this.elements.add(element);
    }

    public void removeElement(int index) {
        this.elements.remove(index);
    }

    public void moveUp(int index) {
        if (index > 0) {
            Collections.swap(elements, index, index - 1);
        }
    }

    public void moveDown(int index) {
        if (index < this.elements.size()-1) {
            Collections.swap(elements, index, index + 1);
        }
    }

    public void updateElement(int index, String name, String description, String unitOfMeasure, String dataType, boolean hidden, boolean store) {
        
    }

    @Remove
    public void save() {
        this.schema.setElements(elements);
        this.dataSchemaDAO.createDataSchema(schema);
    }

    @Remove
    public void cancel() {
    }
}
