/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;

/**
 *
 * @author Juha Loukkola
 */
@RolesAllowed("ADMIN")
@Local
public interface CustomerDAO {

    void importCustomer(Customer customer);
    
    Customer createCustomer(String name, String streetArddess, String zipcode, String city, String email, String phone);
    
    void updateCustomer(int id, String name, String streetArddess, String zipcode, String city, String email, String phone);
    
    void deleteCustomer(int id);

    Customer getCustomer(int id);
    
    Customer getCustomer(String uuid);

    Collection<Customer> getCustomers();
}
