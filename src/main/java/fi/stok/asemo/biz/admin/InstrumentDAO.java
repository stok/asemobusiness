/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.exception.UuidNotUniqueException;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public interface InstrumentDAO {

    void createOrUpdateInstrument(Instrument instrument) throws UuidNotUniqueException;

    void deleteInstrument(int id);

    Instrument getInstrument(int id);

    Instrument getInstrument(String uuid);

    List<Instrument> getInstruments();

    List<Instrument> getNonAttachedInstruments();

    boolean isInstrumentUuidUnique(String uuid);
    
    String addValues(String uuid, List<List<String>> values, boolean rollBackOnError) throws Exception;
}
