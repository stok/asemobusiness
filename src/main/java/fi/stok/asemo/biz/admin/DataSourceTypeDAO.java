/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import java.util.Collection;

/**
 *
 * @author Juha Loukkola
 */
public interface DataSourceTypeDAO {
    
    void importDataSourceType(DataSourceType type);

    DataSourceType createDataSourceType(DataSourceType type);
    
    void createOrUpdateDataSourceType(DataSourceType type);

    void deleteDataSourceType(String uuid);

    DataSourceType getDataSourceType(int id);

    DataSourceType getDataSourceType(String uuid);

    Collection<DataSourceType> getDataSourceTypes();

    boolean isDataSourceTypeUuidUnique(String uuid);
}
