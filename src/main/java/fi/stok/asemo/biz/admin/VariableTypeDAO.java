/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.VariableType;

/**
 *
 * @author Juha Loukkola
 */
public interface VariableTypeDAO {

    VariableType get(int id);

    VariableType get(String uuid);
}
