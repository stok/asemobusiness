package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.exception.PasswordException;
import java.util.Collection;

/**
 *
 * @author Juha Loukkola
 */
public interface UserDAO {

    User createUser(String username, String password, int customerId, Collection<String> groups) throws PasswordException;
    
    User createUser(String username, String password, Collection<String> groups) throws PasswordException;
    
    void addUserToGroup(int userId, String groupName);
    
    void removeUserFromGoup(int userId, String groupName);
    
    void createOrUpdateUser(User user);

    void deleteUser(int id);

    User getUser(int id);

    Collection<User> getUsers();

    boolean isUsernameUnique(String uname);
}
