package fi.stok.asemo.biz.admin;

import javax.ejb.Local;

/**
 * A collecting facade for all admin business methods.
 * 
 * @author Juha Loukkola
 */
@Local
public interface AdminBusiness extends CustomerDAO, UserDAO, NodeDAO, InstrumentDAO, DataSourceTypeDAO, DataSchemaDAO {
}
