package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import java.util.Collection;

/**
 *
 * @author Juha Loukkola
 */
public interface UserGroupDAO {

    void createUserGroup(String name);
    
    UserGroup getUserGroup(String name);

    Collection<UserGroup> getUserGroups();

//    void saveUserGroup(UserGroup group);
}
