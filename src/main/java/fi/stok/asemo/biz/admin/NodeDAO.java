/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.Node;
import java.util.Collection;

/**
 *
 * @author Juha Loukkola
 */
public interface NodeDAO {

    void createOrUpdateNode(Node node);

    void deleteNode(int id);

    Node getNode(int id);

    Node getNode(String uuid);

    Collection<Node> getNodes();

    Collection<Node> getNonAttachedNodes();

    boolean isNodeUuidUnique(String uuid);
}
