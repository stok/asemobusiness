/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.DataSource;

/**
 *
 * @author Juha Loukkola
 */
public interface DataSourceDAO {
    
    void addDataSource(DataSource dataSource);
    
    void createDataSource();

    void deleteDataSource(String uuid);
    
    void updateDataSource();
    
    DataSource getDataSource(String uuid);
}
