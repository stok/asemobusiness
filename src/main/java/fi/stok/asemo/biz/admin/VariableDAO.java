/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.admin;

import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import java.util.Date;
import java.util.List;
import org.eclipse.persistence.queries.CursoredStream;

/**
 *
 * @author Juha Loukkola
 */
public interface VariableDAO {

    void addVariable(Variable variable);

    void deleteVariable(String uuid);

    Variable getVariable(String uuid);

    void updateVariable(Variable variable);
    
    Value getValue(String uuid, Long logLevel, Long timestamp) throws ClassNotFoundException;

    List<Value> getValues(String uuid, Long logLevel, Date from, Date to) throws Exception;

    List<Value> getValues(String uuid, Long logLevel, Date from, Date to, int maxReuslts) throws Exception;
    
    List<Object[]> getPartialValues(String uuid, Long logLevel, Date from, Date to, int maxReuslts) throws Exception;
    
    CursoredStream getValueStream(String variableUuid, Long logLevel, Date from, Date to) throws Exception;
}
