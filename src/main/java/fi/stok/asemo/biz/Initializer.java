/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import fi.stok.asemo.biz.exception.PasswordException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;

/**
 *
 * @author Juha Loukkola
 */
//@Startup
@Singleton
@LocalBean
@Lock(LockType.WRITE)
public class Initializer {

    private static final String USER_GROUPS = "ADMIN,USER,DEMO";
    private static final String ADMIN_GROUP = "ADMIN";
    private static final String ADMIN_USER = "admin";  
    private static final String ADMIN_PASSWORD = "salasana";
    
    @Resource(name = "AsemoInitializationSettings")
    private Properties initializationSettings;
    @EJB
    private Business biz;

    @PostConstruct
    void init() {

        // Setup user groups(roles)
        String str = initializationSettings.getProperty("userGroups", Initializer.USER_GROUPS);
        String[] roles = str.split("[,]");
        for (String role : roles) {
            if (this.biz.getUserGroup(role) == null) {
                UserGroup group = new UserGroup(role);
                this.biz.addUserGroup(group);
            }
        }

        // Setup admin user
        String adminUser = this.initializationSettings.getProperty("adminUser", Initializer.ADMIN_USER);
        String adminPassword = this.initializationSettings.getProperty("adminPassword", Initializer.ADMIN_PASSWORD);
        String adminGroup = this.initializationSettings.getProperty("adminGroup", Initializer.ADMIN_GROUP);
        // Check if admin user is already been created if not create a new
        User admin = this.biz.getUser(adminUser);
        if (admin == null) {
            try {
                admin = new User(adminUser, adminPassword);
                this.biz.addUser(admin);
                this.biz.addUserIntoRole(admin, this.biz.getUserGroup(adminGroup));
            } catch (PasswordException ex) {
                Logger.getLogger(Initializer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
