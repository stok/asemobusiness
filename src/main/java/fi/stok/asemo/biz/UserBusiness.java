/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.Residence;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public interface UserBusiness {
    
//    void updateContactInformation(ContactInformation info);
    void changePassword(String password);
    void updateResidenceInformation(Residence residence);
    User loginUser(String username, String password);
    User getUser(int id);
    List<Value> getLog(int userId, int streamId, Long logLevel, Date from, Date to);
    
}
