/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.biz.ejb.entity.schema.NumericValue;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import fi.stok.asemo.biz.exception.NodeAlreadyRegistered;
import fi.stok.asemo.biz.exception.NodeNotAuthorized;
import fi.stok.asemo.biz.ejb.instruments.Preprocessor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class WSBusinessBean implements WSBusinessLocal {

    private static final Logger LOG = Logger.getLogger("fi.stok.asemo.biz.ejb.WSBusinessBean");
    @PersistenceContext
    private EntityManager em;
    @Resource
    private EJBContext context;

    public WSBusinessBean() {
    }

    // <editor-fold defaultstate="collapsed" desc="Node Related Functionality">
    @Override
    public String registerNode(String mac) throws NodeNotAuthorized, NodeAlreadyRegistered {
        Node node = em.createNamedQuery("Node.findByUuid", Node.class).setParameter("uuid", mac).getSingleResult();
        node.createSecret();
        return node.getSharedSecret();
    }

    @Override
    public boolean authenticateNode(String mac, String secret) {
        try {
            return (em.createNamedQuery("Node.findByUuid", Node.class).setParameter("uuid", mac).getSingleResult() != null);
        } catch (NoResultException ex) {
            //TODO: Examine posibility to use exception.
            return false;
        }
    }

    @Override
    public void updateNodeStatus(String mac, String ipAddress, Date time, String request, String response, String requestMessageContent) {
        Node node = em.createNamedQuery("Node.findByUuid", Node.class).setParameter("uuid", mac).getSingleResult();
        node.setIpAddress(ipAddress);
        node.setLastHeard(time);
        node.setLastRmiFunction(request);
        node.setLastRmiResponse(response);
        node.setLastRmiMessage(requestMessageContent);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Data Streams Related Functionality">
    @Override
    public void storeValue(String id, String time, String val) {
        try {
            ValueGroup valueGroup = this.em.find(ValueGroup.class, Integer.parseInt(id));
            Class dataType = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(Value.class);
            Value value = (Value) dataType.newInstance();
            value.setValueGroup(valueGroup);
            value.setTimeStored(new Date().getTime());
            value.setTimeMeasured(Long.parseLong(time) * 1000l);
            value.parseAndSetValue(val);
            this.em.persist(value);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void storeValue(ValueGroup valueGroup, String time, String val) {
        try {
            Class dataType = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(Value.class);
            Value value = (Value) dataType.newInstance();
            value.setValueGroup(valueGroup);
            value.setTimeStored(new Date().getTime());
            value.setTimeMeasured(Long.parseLong(time) * 1000l);
            value.parseAndSetValue(val);
            this.em.persist(value);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void saveValue(Value value) {
        try {
            em.merge(value);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public void addValues(InputStream csv, ValueGroup vg) {
        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(csv));
            String csvLine;
            int insertCounter = 0;
            while ((csvLine = bReader.readLine()) != null) {
                String tokens[] = csvLine.split("[\\s+,]");
                if (tokens.length == 2) {
                    this.storeValue(vg, tokens[0], tokens[1]);
                    insertCounter++;
                }
                if (insertCounter >= 100000) {
                    insertCounter = 0;
                    this.em.flush();
                    this.em.clear();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
     * TODO: Refactor to throw exceptions instead of returning error strings
     *
     * Store raw data into DB. Preprocess --> process
     *
     */
    @Override
    public String storeValues(List<List<String>> values, Instrument instrument, boolean rollBackOnError) throws Exception {
        String response = "";
        instrument = this.em.find(Instrument.class, instrument.getId());

        // Resolve schema and get elements of schema
        DataSchema dataSchema = instrument.getDataSource().getType().getDataSchema();
        ArrayList<VariableType> schemaElements = new ArrayList(dataSchema.getSchemaElementCollection());

        // Instantiate preprocessor if available
        Preprocessor preprocessor = null;
        if (instrument.getDataSource().getType().getPreprocessor() != null) {
            try {
                Class preprocessorClass = Class.forName(instrument.getDataSource().getType().getPreprocessor()).asSubclass(fi.stok.asemo.biz.ejb.instruments.Preprocessor.class);
                preprocessor = (Preprocessor) preprocessorClass.newInstance();
            } catch (Exception ex) {
                Logger.getLogger(WSBusinessBean.class.getName()).log(Level.SEVERE,
                        "Exception instantiating preprocessor for instrument {0}. Exception: {1}",
                        new Object[]{instrument.getUuid(), ex.getMessage()});
                throw new Exception();
            }
        }

        try {
            // Load data type classes and variables into lookup lists
            List<Class> dataTypeClasses = new ArrayList<Class>();
            List<Variable> variables = new ArrayList<Variable>();
            for (VariableType se : schemaElements) {
                dataTypeClasses.add(Class.forName(se.getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class));
                variables.add(instrument.getDataSource().getVariable(se));//getValueGroup());//variables.add(this.getVariable(instrument, se));
            }
            // Process lines
            for (int j = 0; j < values.size(); j++) {
                try {
                    List<String> line = values.get(j);
                    // Preprocess a line
                    if (preprocessor != null) {
                        // We are preprocessign only one line at a time but prepocess() takes list of lines
                        // so we give it only a list of length one.
                        List<List<String>> lines = new ArrayList<List<String>>();
                        lines.add(line);
                        line = preprocessor.preprocess(lines, instrument).get(0);
                    }

                    // There must be on value per schema element plus timestamp value
                    if (line.size() == schemaElements.size() + 1) {
                        String type = instrument.getDataSource().getType().getName();

                        // The first column represents a timestamp
                        Long timestamp = Long.parseLong(line.get(0)) * 1000l;

                        /*
                         * The rest of the columns represent values. Create a new
                         * Value object for every value/column.
                         */
                        for (int i = 0; i < schemaElements.size(); i++) {
                            VariableType schemaElement = schemaElements.get(i);
                            String token = line.get(i + 1);
                            try {
                                // Resolve data type of the value
                                Class dataTypeClass = dataTypeClasses.get(i);
                                // Construct and persist a new Value object
                                Value value = null; // = (Value) dataTypeClass.newInstance();
                                Variable variable = variables.get(i);
                                value = (Value) dataTypeClass.newInstance();
                                ValueGroup valueGroup = variable.getValueGroup(LogLevels.RAW);
                                value.setValueGroup(valueGroup);
                                value.setTimeStored(new Date().getTime());
                                value.setTimeMeasured(timestamp);
                                value.parseAndSetValue(token);
                                this.em.persist(value);
                                this.em.flush();
                            } catch (Exception ex) {
                                LOG.log(Level.WARNING, "Error while storing a value at index {2} at line {3} for instrument {0}. Exception: {1}",
                                        new Object[]{instrument.getUuid(), ex.getMessage(), i, j});
                                if (rollBackOnError) {
                                    this.context.setRollbackOnly();
                                    throw new Exception();
                                }
                                response += "Error while storing a value at index " + i + " at line " + j
                                        + " for instrument " + instrument.getUuid() + ". Exception " + ex.getMessage() + ".\n";
                            }
                        }
                    } else {
                        LOG.log(Level.INFO, "Error while storing values for an instrument {0}: wrong number of variables at line {1}",
                                new Object[]{instrument.getUuid(), j});
                        if (rollBackOnError) {
                            this.context.setRollbackOnly();
                            throw new Exception();
                        }
                        response += "Error while storing values for an instrument: " + instrument.getUuid()
                                + ": wrong number of variables at line " + j + ".\n";
                    }
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Error while storing a values at line {2} for instrument: {0}. Exception: {1}",
                            new Object[]{instrument.getUuid(), ex.getMessage(), j});
                    if (rollBackOnError) {
                        this.context.setRollbackOnly();
                        throw new Exception();
                    }
                    response += "Error while storing a values at line " + j + " for instrument: "
                            + instrument.getUuid() + ". Exception: " + ex.getMessage() + ".\n";
                }
            }
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error while storing values for an instrument {0}. Exception: {1}", new Object[]{instrument.getUuid(), e.getMessage()});
            if (rollBackOnError) {
                this.context.setRollbackOnly();
                throw new Exception();
            }
            response += "Error while storing values for an instrument " + instrument.getUuid() + ". Exception: " + e.getMessage() + ".\n";
        }
        if (response.isEmpty()) {
            response = "OK";
        }
        return response;

    }

    @Override
    public Value getLastValue(Variable variable) {
        return getLastValue(variable.getValueGroup(LogLevels.RAW));
    }

    @Override
    public Value getLastValue(ValueGroup valueGroup) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class);
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findLastByData", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public Value getFirstValue(ValueGroup valueGroup) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class);
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findFirstByData", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public Value getFirstValue(Variable variable) {
        return getFirstValue(variable.getValueGroup(LogLevels.RAW));
    }

    @Override
    public List<Value> getValues(ValueGroup valueGroup, Date from, Date to) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class);
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setParameter("from", from);
            query.setParameter("to", to);
            return query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Gets the first value before date in a valueGroup
     *
     * @param valueGroup
     * @param date
     * @return
     */
    @Override
    public Value getValueBefere(ValueGroup valueGroup, Date date) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class);
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findNBeforeDateByValueGroup", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setParameter("from", date.getTime());
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public Value getValueAtOrBefere(ValueGroup valueGroup, Date date) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class);
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findNBeforeOrAtDateByValueGroup", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setParameter("from", date.getTime());
            query.setMaxResults(1);
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public List<NumericValue> getNumericValues(ValueGroup valueGroup, Date from, Date to) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.NumericValue.class);
            TypedQuery<NumericValue> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setParameter("from", from.getTime());
            query.setParameter("to", to.getTime());
            return query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public List<NumericValue> getNumericValuesAfter(ValueGroup valueGroup, Date from, int MaxResults) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.NumericValue.class);
            TypedQuery<NumericValue> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findNAfterDateByValueGroup", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setParameter("from", from.getTime());
            query.setMaxResults(MaxResults);
            return query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public List<NumericValue> getNumericValuesAtOrAfter(ValueGroup valueGroup, Date from, int MaxResults) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.NumericValue.class);
            TypedQuery<NumericValue> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findNAfterOrAtDateByValueGroup", dataTypeClass);
            query.setParameter("valueGroup", valueGroup);
            query.setParameter("from", from.getTime());
            query.setMaxResults(MaxResults);
            return query.getResultList();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean isIntervalEmpty(ValueGroup valueGroup, Date from, Date to) {
        try {
            Class dataTypeClass = Class.forName(valueGroup.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.NumericValue.class);
            TypedQuery<NumericValue> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass)
                    .setParameter("valueGroup", valueGroup)
                    .setParameter("from", from.getTime())
                    .setParameter("to", to.getTime())
                    .setMaxResults(1);
            NumericValue value = query.getSingleResult();
            return false;
        } catch (NoResultException ex) {
            return true;
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }
// </editor-fold>
}
