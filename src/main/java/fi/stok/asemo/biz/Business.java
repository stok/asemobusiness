    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.DataSource;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.biz.ejb.entity.schema.Residence;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import fi.stok.asemo.biz.exception.EmailNotConfiguredException;
import fi.stok.asemo.biz.exception.IncorrectPasswordException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import fi.stok.asemo.biz.exception.UuidNotUniqueException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Juha Loukkola
 */
@Local
public interface Business {

    
    
    /* Customer functionality (Roles: Admin)
     *************************************************************************/
    void addCustomer(Customer customer) throws PasswordException, UuidNotUniqueException;
    void createCustomer(Customer customer);
    void updateCustomer(Customer customer);
    void deleteCustomer(int id);
    Customer getCustomer(int id);
    void saveCustomer(Customer customer);
    List<Customer> getCustomers();

    /* User functionality
     *************************************************************************/
    @Deprecated void addUser(User user);
    void createUser(User user);
    void updateUser(User user);
    void deleteUser(int id);
    User getUser(int id);
    User getUser(String username);
    void saveUser(User user);
    
    List<User> getUsers();
    List<User> getNonAttachedUsers();

    boolean isUsernameUnique(String uname);

    // TODO: rename to userAuthenticatedInRole
    boolean userInRole(User user, String role);
    @Deprecated void addUserIntoRole(User user, UserGroup role);
    User authenticateUser(String uname, String passwd) throws IncorrectPasswordException, UserNotFoundException, PasswordException;
    void processPasswordRequest(String uname) throws UserNotFoundException, EmailNotConfiguredException;
    
    /* UserGroup functionality
     *************************************************************************/
    UserGroup getUserGroup(String name);
    List<UserGroup> getUserGroups();
    void addUserGroup(UserGroup group);   
    void saveUserGroup(UserGroup group);
    
    /* Node functionality
     *************************************************************************/
    @Deprecated void addNode(Node node);
    void createNode(Node node);
    void deleteNode(int id);
    void updateNode(Node node);
    Node getNode(int id);
    Node getNode(String uuid);
    @Deprecated void saveNode(Node node);
    List<Node> getAllNodes();
    List<Node> getNonAttachedNodes();
    boolean isUuidUnique(String uuid);

    /* Instrument functionality
     *************************************************************************/
    Instrument createInstrument(String uuid, int typeId) throws UuidNotUniqueException;
    Instrument createInstrument(String uuid, int typeId, int nodeId) throws UuidNotUniqueException;
    void attachInstrumentToNode(Instrument instrument, Node node);
    void updateInstrument(int id, Integer nodeId);
    void deleteInstrument(int id);
    Instrument getInstrument(int id);
    Instrument getInstrument(String uuid);
    void saveInstrument(Instrument instrument);
    List<Instrument> getInstruments();
    List<Instrument> getInstrumentsByUser(int userId);
    List<Instrument> getNonAttachedInstruments();
    boolean isUuidTaken(String uuid);

    /* Data source type functionality
     *************************************************************************/
    void createDataSourceType(DataSourceType dataSourceType);
    void updateDataSourceType(DataSourceType dataSourceType);
    void createOrUpdateDataSourceType(DataSourceType dataSourceType);
    
    void deleteDataSourceType(int id);
    DataSourceType getDataSourceType(int id);
    List<DataSourceType> getDataSourceTypes();

    /* Residence functionality
     *************************************************************************/
    void addResidence(Residence residence);
    void updateResidence(Residence residence);
  
    /* Log data & annotation related functionality
     *************************************************************************/
    @Deprecated List<Value> getValues(User user, int instrument_id, VariableType schemaElement, Long logLevel, Date from, Date to) throws Exception;
    List<Value> getValues(int streamId, Long logLevel, Date from, Date to) throws Exception;
//    sStream getValueStream(ValueGroup valueGroup, Date from, Date to);
    Long getNearestLogLevel(Long normalizationPeriod);  
    
    /* Stream related functionality
     *************************************************************************/
    Stream getStream(int id);
    
    
    /* Data schema related functionality
     *************************************************************************/
    List<DataSchema> getDataSchemas();
    DataSchema getDataSchema(int id);
    void saveDataSchema(DataSchema schema);
    
    void createDataSchema(String name, String description, List<VariableType>elements);
    void updateDataSchema(Integer id, String name, String description, List<VariableType>elements);
    void deleteDataSchema(int id);
    
    void addVariableTypeToSchema(int id, String name, String description, String unitOfMeasure, Boolean hidden, Boolean store);
    void removeVariableType(int id);
    void updateVariableType(int id, String name, String description, String unitOfMeasure, Boolean hidden, Boolean store);
    
    VariableType getSchemaElement(int id);
    void saveSchemaElement(VariableType element);
    void deleteSchemaElement(VariableType element);
    
    /* Variable related functionality
     *************************************************************************/
    Variable getVariable(int id);
    void setupVariables(DataSource dataSource, Customer customer);
    
    /* ValueGroup related functionality
     *************************************************************************/
    ValueGroup getValueGroup(int id);

    /* MISC. functionality
     *************************************************************************/
    void setUuids(String className);
    
}
