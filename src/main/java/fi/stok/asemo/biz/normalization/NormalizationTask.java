package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.WSBusinessLocal;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author Juha Loukkola
 */
public class NormalizationTask {

    private Variable variable;
    private Long logLevel;
    private Date from;
    private Date to;
    private boolean beingProcessed;
    private int priority;
    @EJB
    private WSBusinessLocal biz;

//    public NormalizationTask(Variable var, Long logLevel, int maxSegments) {
//        NormalizationUtil nUtil = new NormalizationUtil();
//        ValueGroup target = var.getValueGroup(logLevel);
//        ValueGroup source = var.getValueGroup(nUtil.findSourceLogLevel(var, logLevel));
//
//        if (source != null) {
//
//            // Resolve start of the first non-normalized segment        
//            Date start = null;
//            Value lastTargetValue = this.biz.getLastValue(target);
//            if (lastTargetValue == null) {
//                //If there is no points in the target time serie then normalize all data from the begining of the source time serie
//                start = this.biz.getFirstValue(source).getTimeMeasured();
//            } else {
//                start = new Date(lastTargetValue.getTimeMeasured().getTime() + target.getSamplingPeriod());
//            }
//
//            // Try to allign the start date
//            try {
//                start = NormalizationUtil.arrangeDate(start, target.getSamplingPeriod(), TimeZone.getTimeZone("GMT+2"));
//            } catch (Exception ex) {
//                Logger.getLogger(OldNormalizer.class
//                        .getName()).log(Level.SEVERE, null, ex);
//            }
//            this.variable = var;
//            this.logLevel = logLevel;
//            this.from = start;
//            this.to = new Date(Math.min(start.getTime() + logLevel * maxSegments, this.biz.getLastValue(source).getTimeMeasured().getTime()));
//            this.priority = (int) ((to.getTime() - start.getTime()) / logLevel);
//        }
//    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public Long getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(Long logLevel) {
        this.logLevel = logLevel;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public boolean isBeingProcessed() {
        return beingProcessed;
    }

    public void setBeingProcessed(boolean beingProcessed) {
        this.beingProcessed = beingProcessed;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    public static Comparator<NormalizationTask> PriorityComparator = new Comparator<NormalizationTask>() {
        @Override
        public int compare(NormalizationTask t1, NormalizationTask t2) {
            Integer priority1 = t1.getPriority();
            Integer priority2 = t2.getPriority();
            return priority1.compareTo(priority2);
        }
    };
}
