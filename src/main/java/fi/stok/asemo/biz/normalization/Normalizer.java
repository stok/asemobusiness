/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.WSBusinessLocal;
import fi.stok.asemo.biz.ejb.entity.schema.NumericValue;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class Normalizer {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private WSBusinessLocal biz;

    /**
     *
     * @param target
     * @param source
     * @param from
     * @param to
     */
    public long normalizeValueGroup(ValueGroup target, ValueGroup source, Long from, Long to) {
        long workDone = 0;
        try {
            List<NumericValue> sourceData = new ArrayList<NumericValue>();
            // Add valua before the start of the first segment as the firt value of source data set
            NumericValue firstValue = (NumericValue) biz.getValueBefere(source, new Date(from));
            if (firstValue != null) {
                sourceData.add(firstValue);
            }
            // Get rest of the data
            List<NumericValue> values = biz.getNumericValues(source, new Date(from), new Date(to));
            if (values.size() > 0) {
                sourceData.addAll(values);
                // Normalize
                workDone = this.normalizeSegments(target, sourceData, (int) ((to - from) / target.getSamplingPeriod()), from);
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return workDone;
    }

    /**
     *
     * @param target
     * @param data
     * @param numSegments
     * @param from
     */
    public long normalizeSegments(ValueGroup target, List<? extends NumericValue> data, int numSegments, Long from) {
        long workDone = 0;
        try {
            Long segmentLength = target.getSamplingPeriod();
            while (numSegments > 0) {
                Long to = from + segmentLength;
                List<? extends NumericValue> segmentData = NormalizationUtil.segment(data, from, to);
                if (segmentData != null && !segmentData.isEmpty()) {
                    workDone += this.normalizeSegment(target, segmentData, from, to);
                    from = to;
//                  this.em.flush();
                    numSegments--;
                } else {
                    from = NormalizationUtil.nextSegment(data, segmentLength, from);
                    // No more data --> We are done here
                    if (from == null) {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return workDone;
    }

    /**
     * Normalizes one segment of data
     *
     * @param segmentData
     * @param start
     * @param end
     * @param orientation
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public long normalizeSegment(ValueGroup target, List<? extends NumericValue> segmentData, Long start, Long end) {
        long workDone = 0;
        try {
            Number normalized = NormalizationUtil.durationWeightedAverage(segmentData, start, end);
            workDone++;
            if (normalized != null) {
                // Add normalized value to the log level
                Class dataTypeClass = Class.forName(target.getVariable().getType().getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.NumericValue.class);
                NumericValue normalizedValue = (NumericValue) dataTypeClass.newInstance();
                normalizedValue.setValueGroup(target);
                normalizedValue.setTimeMeasured(start);
                normalizedValue.setTimeStored(new Date().getTime());
                normalizedValue.setValue(normalized);
                this.em.persist(normalizedValue);
//                this.em.flush();
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return workDone;
    }
}
