package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.normalization.NormalizationCoordinator;
import fi.stok.asemo.biz.normalization.Normalizer;
import fi.stok.asemo.biz.normalization.NormalizationUtil;
import fi.stok.asemo.biz.ejb.entity.schema.NormalizationJob;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

/**
 *
 * @author Juha Loukkola
 */
@Singleton
@LocalBean
public class NormalizationJobRunner {


    @EJB
    private NormalizationCoordinator coordinator;
    @EJB
    private Normalizer normalizer;


    @Lock(LockType.WRITE)
    public void runJobs(long maxWorkEstimate) {
        long workDone = 0;
        //TODO: get jobs from the coordinator to be run
        while (workDone < maxWorkEstimate) {
            NormalizationJob job = this.coordinator.getJob();
            if (job == null) {
                break;
            }
            workDone += this.runJob(job);
            this.coordinator.reportJob(job);
        }
    }

    /**
     * To prevent multiple
     *
     * @param job
     */
    public long runJob(NormalizationJob job) {
        long workDone = 0;
            try {
                job.setTimeStarted(new Date());
                workDone = this.normalizer.normalizeValueGroup(job.getValueGroup(), NormalizationUtil.findSourceValueGroup(job.getValueGroup()), job.getFrom(), job.getTo());
                job.setStatus("COMPLETED");
            } catch (Exception ex) {
                job.setStatus("FAILED");
            }
            job.setTimeEnded(new Date());
            job.setWorkDone(workDone);
        return workDone;
    }
    
}
