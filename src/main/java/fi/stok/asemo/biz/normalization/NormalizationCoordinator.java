package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.BusinessBean;
import fi.stok.asemo.biz.CronJobs;
import fi.stok.asemo.biz.LogLevels;
import fi.stok.asemo.biz.WSBusinessLocal;
import fi.stok.asemo.biz.ejb.entity.schema.NormalizationJob;
import fi.stok.asemo.biz.ejb.entity.schema.NumericValue;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 *
 * @author Juha Loukkola
 */
//@TransactionManagement(TransactionManagementType.BEAN)
@Stateless
@LocalBean
public class NormalizationCoordinator {

    private static final String READY = "READY";
    private static final String PROCESSING = "PROCESSING";
    private static final String COMPLETED = "COMPLETED";
    private static final String FAILED = "FAILED";
    private static final Long MAX_WORK_ESTIMATE = 3000l;
    private static final Logger LOG = Logger.getLogger(NormalizationCoordinator.class.getName());
    @EJB
    private WSBusinessLocal biz;
    @PersistenceContext
    private EntityManager em;

    /**
     *
     *
     */
    public void updateSchedule() {
        this.findAndAddNewTimeSeries(MAX_WORK_ESTIMATE);
        NormalizationJobSchedulingPolicy.schedule(this.getReadyJobs(), MAX_WORK_ESTIMATE);
    }

    public NormalizationJob getJob() {
        try {
            NormalizationJob nextJob = this.em.createNamedQuery(NormalizationJob.class.getSimpleName() + ".findByStatusOrderedByPriorityDesc", NormalizationJob.class)
                    .setParameter("status", READY)
                    .setMaxResults(1)
                    .getSingleResult();
            nextJob.setStatus(PROCESSING);
            return nextJob;
        } catch (NoResultException ex) {
            return null;
        }
    }

    public void reportJob(NormalizationJob job) {
        if (job.getStatus().equals(COMPLETED)) {
            // NICE!
        } else if (job.getStatus().equals(FAILED)) {
            // WELL SHIT! Add next job.
        } else {
            // Errornous status
        }
//        try {
//            this.addNextJob(job.getTo(), job.getValueGroup(), MAX_WORK_ESTIMATE);
//        } catch (Exception ex) {
//            LOG.log(Level.SEVERE, null, ex);
//        }
    }

    /**
     * TODO: Move guery to DAO
     *
     */
    public void findAndAddNewTimeSeries(Long maxWorkEstimate) {
        try {
            // Find all time series that dont currently have any pending jobs.
            List<ValueGroup> timeSeries = this.em.createQuery("SELECT vg FROM ValueGroup vg WHERE NOT EXISTS (SELECT j FROM NormalizationJob j WHERE (j.status = '" + PROCESSING + "' OR j.status = '" + READY + "') AND j.valueGroup = vg)", ValueGroup.class).getResultList();
            // Add new job for all time series that are enabled, don't represent raw data and are numeric
            for (ValueGroup vg : timeSeries) {
                if (vg.isEnabled() && vg.getSamplingPeriod() > LogLevels.RAW) {
                    try {
                        // Test if time serie is numeric and therefore can be normalized
                        Class.forName(vg.getVariable().getType().getDataType()).asSubclass(NumericValue.class);
                        this.addFirstJob(vg, maxWorkEstimate);
                    } catch (ClassNotFoundException ex) {
                        LOG.log(Level.SEVERE, null, ex);
                        continue;
                    } catch (ClassCastException ex) {
                        // The variable is not type of NumericValue and cant therefore be normalized so ignore it.
                        continue;
                    }
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }

    }

    public void addFirstJob(ValueGroup valueGroup, Long maxWorkEstimate) throws Exception {
        // Resolve start of the first non-normalized segment        
        Long start;
        Value lastValue = this.biz.getLastValue(valueGroup);

        if (lastValue == null) {
            //If there is no points in the target time serie then normalize all data from the begining of the source time serie
            ValueGroup source = NormalizationUtil.findSourceValueGroup(valueGroup);
            Value firstSourceValue = this.biz.getFirstValue(source);
            if (firstSourceValue == null) {
                return;
            }
            start = firstSourceValue.getTimeMeasured();
        } else {
            start = lastValue.getTimeMeasured() + valueGroup.getSamplingPeriod();
        }
        this.addNextJob(start, valueGroup, maxWorkEstimate);
    }

    public NormalizationJob addNextJob(Long from, ValueGroup valueGroup, Long maxWorkEstimate) throws Exception {
        from = NormalizationUtil.arrangeDate(from, valueGroup.getSamplingPeriod(), TimeZone.getTimeZone("GMT+2"));
        Long workEstimate = this.estimateWork(valueGroup, from, maxWorkEstimate);

        if (workEstimate == 0l) {
            List<NumericValue> values = this.biz.getNumericValuesAtOrAfter(NormalizationUtil.findSourceValueGroup(valueGroup), new Date(from), 1);
            if (!values.isEmpty()) {
                from = values.get(0).getTimeMeasured();
                from = NormalizationUtil.arrangeDate(from, valueGroup.getSamplingPeriod(), TimeZone.getTimeZone("GMT+2"));
                workEstimate = this.estimateWork(valueGroup, from, maxWorkEstimate);
            } else {
                return null;
            }
        }

        return this.addJob(from, valueGroup, workEstimate);
    }

    /**
     *
     * @param queue a queue where the job is to be added. Should be in managed
     * state in an entity manager.
     * @param start
     * @param valueGroup
     * @param maxWorkEstimate
     */
    public NormalizationJob addJob(Long start, ValueGroup valueGroup, Long workEstimate) throws Exception {
//        start = NormalizationUtil.arrangeDate(start, valueGroup.getSamplingPeriod(), TimeZone.getTimeZone("GMT+2"));
//        Long workEstimate = estimateWork(valueGroup, start, maxWorkEstimate);
        if (workEstimate > 0) {
            NormalizationJob job = new NormalizationJob(
                    valueGroup,
                    start,
                    start + workEstimate * valueGroup.getSamplingPeriod(),
                    workEstimate);
            this.em.persist(job);
            return job;
        } else {
            return null;
        }
    }

    public Long estimateWork(ValueGroup valueGroup, Long start, Long maxWorkEstimate) {
        Variable variable = valueGroup.getVariable();
        ValueGroup source = NormalizationUtil.findSourceValueGroup(valueGroup);
        if (!this.biz.isIntervalEmpty(source, new Date(start), new Date(start + maxWorkEstimate * valueGroup.getSamplingPeriod()))) {
            Long lastSourceValue = this.biz.getLastValue(source).getTimeMeasured();
            Long workEstimate = Math.min(maxWorkEstimate, (lastSourceValue - start) / valueGroup.getSamplingPeriod());
            if (workEstimate > 0) {
                if (!this.biz.isIntervalEmpty(source, new Date(start), new Date(start + workEstimate * valueGroup.getSamplingPeriod()))) {
                    return workEstimate;
                }
            }
        }
        return 0l;
    }

    private List<NormalizationJob> getReadyJobs() {
        return getJobs(READY);
    }

    private List<NormalizationJob> getProcessedJobs() {
        return getJobs(PROCESSING);
    }

    private List<NormalizationJob> getCompletedJobs() {
        return getJobs(COMPLETED);
    }

    private List<NormalizationJob> getJobs(String status) {
        return this.em.createNamedQuery(NormalizationJob.class.getSimpleName() + ".findByStatusOrderedByPriorityDesc", NormalizationJob.class)
                .setParameter("status", status)
                .getResultList();
    }

    public void removeCompletedJobs(Long age) {
        java.sql.Date from = new java.sql.Date(new Date().getTime() - age);
        this.em.createNamedQuery("NormalizationJob.deleteOlderThanByStatus", NormalizationJob.class)
                .setParameter("status", COMPLETED)
                .setParameter("from", from)
                .executeUpdate();
//        this.em.createQuery("DELETE FROM NormalizationJob nj WHERE nj.status = COMPLETED AND nj.timeEnded <" + from).executeUpdate();
    }
}
