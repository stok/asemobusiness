package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.ejb.entity.schema.NormalizationJob;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public class NormalizationJobSchedulingPolicy {

//    private int maxWorkEstimate;
    public NormalizationJobSchedulingPolicy(int maxWorkEstimate) {
//        this.maxWorkEstimate = maxWorkEstimate;
    }

    /**
     * TSLV Time since last normalized value in days. Prioritize time series
     * that are behind others. WE Work estimate. Prioritize larger jobs over
     * small ones. TSA Time since added in minutes. Prioritize jobs that have
     * been longer on the queue. This should dominate over all other parameters
     * when a job have been waiting more than 60 min.
     */
    private static int calculatePriority(NormalizationJob job, Long maxWorkEstimate) {
        Long TSA = Math.max((new Date().getTime() - job.getTimeAdded().getTime()) / 60000, 0); // Range from 0 to Long.MAX_VALUE / 60000
        int WE = (int) (10 * job.getWorkEstimate() / maxWorkEstimate); // Ranges form 0 to 10
        //Long TSLV = job.getFrom().getTime() / 86400000; // Ranges from 0 to 

        return (int) Math.min(TSA + WE, Integer.MAX_VALUE);
    }

    /**
     * 
     * @param queue
     * @param maxWorkEstimate 
     */
    public static void schedule(List<NormalizationJob> queue, Long maxWorkEstimate) {
        for (NormalizationJob job : queue) {
            if (job != null) {
                job.setPriority(NormalizationJobSchedulingPolicy.calculatePriority(job, maxWorkEstimate));
            }
        }
        //Sort job queue
        //Collections.sort(queue, NormalizationJob.DescendingPriorityComparator);
    }
}
