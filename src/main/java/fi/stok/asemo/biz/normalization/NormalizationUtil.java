package fi.stok.asemo.biz.normalization;

import fi.stok.asemo.biz.LogLevels;
import fi.stok.asemo.biz.ejb.entity.schema.IntegerValue;
import fi.stok.asemo.biz.ejb.entity.schema.NumericValue;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juha Loukkola
 */
public class NormalizationUtil {

    /**
     * Finds the first suitable source log level for a given log level.
     *
     * @param variable
     * @param logLevel
     * @return
     */
    public static ValueGroup findSourceValueGroup(ValueGroup valueGroup) {
        Variable variable = valueGroup.getVariable();
        int index = Collections.binarySearch(variable.getValueGroups(), variable.getValueGroup(valueGroup.getSamplingPeriod()), ValueGroup.SamplingPeriodComparator);
        ListIterator<ValueGroup> logLevelsIterator = variable.getValueGroups().listIterator(index);
        while (logLevelsIterator.hasPrevious()) {
            ValueGroup vg = logLevelsIterator.previous();
            if (vg.isEnabled()) {
                return vg;
            }
        }
        return null;
    }

    /**
     * Finds the first suitable source log level for a given log level.
     *
     * @param variable
     * @param logLevel
     * @return
     */
    public static Long findSourceLogLevel(ValueGroup valueGroup) {
        ValueGroup vg = NormalizationUtil.findSourceValueGroup(valueGroup);
        if (vg == null) {
            return null;
        } else {
            return vg.getSamplingPeriod();
        }
    }

    // Tekes a segment from a time series.
    public static List<? extends NumericValue> segment(List<? extends NumericValue> data, Long start, Long end) throws IndexOutOfBoundsException {
        try {
            List<? extends NumericValue> results;
            Value value = new IntegerValue();
            value.setTimeMeasured(start);
            int startIndex = Collections.binarySearch(data, value, Value.TimeMeasuredComparator);

            value.setTimeMeasured(end);
            int endIndex = Collections.binarySearch(data, value, Value.TimeMeasuredComparator);

            // Exclude/Include start/end values
            // Resolve the index of the first value to be includen into a data set        
            if (startIndex < 0) {
                startIndex = (startIndex == -1) ? 0 : Math.abs(startIndex) - 2;
            }
            // Resolve the index of the last value to be includen into a data set
            if (endIndex < 0) {
                endIndex = (endIndex == -1) ? 0 : Math.abs(endIndex) - 1;
            }
            results = data.subList(startIndex, endIndex);

            // An empty segment results null as return value
            if ((results.size() == 1 && results.get(0).getTimeMeasured() < start) || results.isEmpty()) {
                return null;
            }

            return results;
        } catch (Exception ex) {
            Logger.getLogger(NormalizationUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Calculates a duration weighted average of values representing a time
     * series. The wight of a value is its duration in a time series data.
     *
     * @param timeSeriesData a list of values representing a time series over
     * which the average is calculated. The list must be ordered by timeMeasured
     * in ascending order. The data set can hold values from longer period than
     * the start and end dates define.
     * @param start the start time for the segment.
     * @param end the end time for the segment.
     * @see Normalizer.IntervalOrientation
     * @return a weighted average of values.
     * @throws Exception if the data is not in ascending order.
     */
    public static Number durationWeightedAverage(List<? extends NumericValue> timeSeriesData, Long start, Long end) throws Exception {
        Number integral = null;

        Iterator<? extends NumericValue> iterator = timeSeriesData.iterator();
        NumericValue value = iterator.hasNext() ? iterator.next() : null;
        while (value != null) {
            NumericValue nextValue = iterator.hasNext() ? iterator.next() : null;

            // Handle values before the segment.
            if (value.getTimeMeasured() < start) {
                // If all the values are before the segment return null
                if (nextValue == null) {
                    return null;
                } // If next value is inside the segment excluding the start the value has an effect on the integral
                else if (nextValue.getTimeMeasured() > start && nextValue.getTimeMeasured() < end) {
                    integral = value.getValue().doubleValue() * (nextValue.getTimeMeasured() - start);
                }
                // iterate
                value = nextValue;
                continue;
            }
            // Skip data points that are after segment excluding end.
            if (end < value.getTimeMeasured()) {
                break;
            }
            long t_i = value.getTimeMeasured();
            long t_ii;

            if (nextValue == null || nextValue.getTimeMeasured() > end) {
                t_ii = end;
            } // Data points in wrong order.
            else if (value.getTimeMeasured() > nextValue.getTimeMeasured()) {
                throw new Exception();
            } else {
                t_ii = nextValue.getTimeMeasured();
            }

            if (integral == null) {
                integral = 0.0;
            }
            integral = integral.doubleValue() + value.getValue().doubleValue() * (t_ii - t_i);
            // iterate
            value = nextValue;
        }
        if (integral != null && end > start) {
            return integral.doubleValue() / (end - start);
        }
        return null;
    }

    /**
     * Calculates an average of values.
     *
     * @param data a list of values over which the average is calculated.
     * @return an average of values.
     */
    public static Number average(List<? extends NumericValue> data) {
        if (!data.isEmpty()) {
            Number sum = 0;
            for (NumericValue value : data) {
                sum = sum.doubleValue() + value.getValue().doubleValue();
            }
            return sum.doubleValue() / data.size();
        } else {
            return null;
        }
    }

    /**
     * Arranges a date so that it is a product of specified resolution within a
     * minute, a hour or a day.
     *
     * @param date a date to be arranged
     * @param resolution the resolution of a date in milliseconds
     * @param timeZone the time zone of the date
     * @return an arranged date.
     * @throws Exception if the resolution is not a factor of a minute, a hour
     * or a day.
     * @throws Exception if the resolution is greater than LogLevels.DAY.
     */
    // TODO: Take day light saving into account.
    public static Long arrangeDate(Long date, Long resolution, TimeZone timeZone) throws Exception {
        GregorianCalendar calendar = new GregorianCalendar(timeZone);
        calendar.setTimeInMillis(date);

        Long logLevel = 0l;
        Long factorOf = 0l;
        int amount = 0;

        if (resolution >= LogLevels.SECOND && resolution < LogLevels.MIN) {
            logLevel = LogLevels.SECOND;
            factorOf = LogLevels.MIN;
            amount = calendar.get(Calendar.SECOND);
            calendar.set(Calendar.MILLISECOND, 0);
        } else if (resolution >= LogLevels.MIN && resolution < LogLevels.HOUR) {
            logLevel = LogLevels.MIN;
            factorOf = LogLevels.HOUR;
            amount = calendar.get(Calendar.MINUTE);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        } else if (resolution >= LogLevels.HOUR && resolution < LogLevels.DAY) {
            logLevel = LogLevels.HOUR;
            factorOf = LogLevels.DAY;
            amount = calendar.get(Calendar.HOUR);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        } else if (resolution.equals(LogLevels.DAY)) {
            logLevel = LogLevels.DAY;
            factorOf = LogLevels.DAY;
            amount = calendar.get(Calendar.DAY_OF_WEEK);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
        } else {
            throw new Exception();
        }

        if (factorOf % resolution == 0) {
            int segmentLength = (int) (resolution / logLevel);
            int arrangmentOffset = amount % segmentLength;
            return new Date(calendar.getTime().getTime() - arrangmentOffset * logLevel).getTime();
        } else {
            throw new Exception();
        }
    }
    
    /**
     * Calculates the starting date of the next segment containing data. This
     * method is mainly used for skipping empty sections.
     *
     * @param data
     * @param segmentLength
     * @param from
     * @return
     */
    public static Long nextSegment(List<? extends NumericValue> data, Long segmentLength, Long from) {
        try {
            // Find first value >= from
            Value value = new IntegerValue();
            value.setTimeMeasured(from);
            int startIndex = Collections.binarySearch(data, value, Value.TimeMeasuredComparator);
            if (startIndex < 0) {
                startIndex = Math.abs(startIndex + 1);
            }
            if (startIndex < data.size()) {
                // Find the number of segment it resides from "from"
                int segmentOffset = (int) ((data.get(startIndex).getTimeMeasured() - from) / segmentLength);

                // Return the start of that segment
                try {
                    return NormalizationUtil.arrangeDate(data.get(startIndex).getTimeMeasured(), segmentLength, TimeZone.getTimeZone("GMT+2"));
                } catch (Exception ex) {
                    return from + segmentLength * segmentOffset;
                }
            } else {
                return null;
            }
        } catch (Exception ex) {
            Logger.getLogger(NormalizationUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
