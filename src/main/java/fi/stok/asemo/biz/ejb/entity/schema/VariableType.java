package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "variableType")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = { "uuid", "name", "description", "unitOfMeasure", "dataType", "dataSchema","hidden", "store", "valueGroupSettings" })
@Entity
@Table(name = "variable_type")
@NamedQueries({
    @NamedQuery(name = "VariableType.findAll", query = "SELECT se FROM VariableType se"),
    @NamedQuery(name = "VariableType.findById", query = "SELECT se FROM VariableType se WHERE se.id = :id"),
    @NamedQuery(name = "VariableType.findByUuid", query = "SELECT vt FROM VariableType vt WHERE vt.uuid = :uuid"),
    @NamedQuery(name = "VariableType.findByName", query = "SELECT se FROM VariableType se WHERE se.name = :name")})
public class VariableType implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;
    
    @XmlID
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
    
    @Size(max = 36)
    @Basic(optional = false)
    @Column(name = "name", length = 36)
    private String name;
    
    @Size(max = 255)
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    
    @Size(max = 8)
    @Basic(optional = false)
    @Column(name = "unit_of_measure", length = 8)
    private String unitOfMeasure;
    
    @Size(max = 128)
    @Basic(optional = false)
    @Column(name = "data_type", length = 128)
    // Fully qualified class name of the dataType
    private String dataType;
    
//    @XmlIDREF
//    @XmlElementRef
    @XmlTransient
    @ManyToOne(optional = false)
    @JoinColumn(name = "data_schema", referencedColumnName = "id")
    private DataSchema dataSchema;
    
    @Basic(optional = false)
    @Column(name = "hidden")
    private boolean hidden = false;
    
    @Basic(optional = false)
    @Column(name = "store")
    private boolean store = true;
    
    @XmlTransient
//    @XmlElementWrapper(name = "valueGroupSettings")
//    @XmlElement(name = "valueGroupSettings")
    @OneToMany(mappedBy = "variableType", targetEntity = ValueGroupSettings.class,
            fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn
    private List<ValueGroupSettings> valueGroupSettings;

    public VariableType() {
    }

    public VariableType(String name, String description, String unitOfMeasure, Boolean hidden, Boolean store) {
        this.name = name;
        this.description = description;
        this.unitOfMeasure = unitOfMeasure;
        this.hidden = hidden;
        this.store = store;
    }

    public DataSchema getDataSchema() {
        return dataSchema;
    }

    public void setDataSchema(DataSchema dataSchema) {
        this.dataSchema = dataSchema;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public boolean isHidden() {
        return hidden;
    }

    public boolean getHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isStore() {
        return store;
    }

    public boolean getStore() {
        return store;
    }

    public void setStore(boolean store) {
        this.store = store;
    }

    public List<ValueGroupSettings> getValueGroupSettings() {
        return valueGroupSettings;
    }

    public void setValueGroupSettings(List<ValueGroupSettings> valueGroupSettings) {
        this.valueGroupSettings = valueGroupSettings;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VariableType)) {
            return false;
        }
        VariableType other = (VariableType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "VariableType[ id=" + id + " ]";
    }

    public void afterUnmarshal(Unmarshaller u, Object parent) {
        this.dataSchema = (DataSchema) parent;
    }
}
