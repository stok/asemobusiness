/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "dataSchema")
//@XmlTransient
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Table(name = "data_schema")
@NamedQueries({
    @NamedQuery(name = "DataSchema.findAll", query = "SELECT ds FROM DataSchema ds"),
    @NamedQuery(name = "DataSchema.findByUuid", query = "SELECT ds FROM DataSchema ds WHERE ds.uuid = :uuid")
})
public abstract class DataSchema implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)  
    @Column(name = "id", nullable = false)
    protected Integer id;
    
    @XmlID
    @XmlElement
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
    
    @Basic(optional = false)  
    @Column(name = "type")
    protected String type;
    
    @Basic(optional = false)   
    @Column(name = "name")
    protected String name;
    
    @Size(max = 25000)
    @Basic(optional = false)  
    @Column(name = "description", length = 25000)
    protected String description;
    
    public DataSchema() {
//        this.uuid = UUID.randomUUID().toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

//    public void setType(String type) {
//        this.type = type;
//    }
    
    public abstract Collection<VariableType> getSchemaElementCollection();
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataSchema)) {
            return false;
        }
        DataSchema other = (DataSchema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataSchema[ id=" + id + " ]";
    }
}
