package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "instrument")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "uuid", "enabled", "node", "dataSource" })
@Entity
@Table(name = "instrument")
@NamedQueries({
    @NamedQuery(name = "Instrument.findAll", query = "SELECT i FROM Instrument i"),
    @NamedQuery(name = "Instrument.findById", query = "SELECT i FROM Instrument i WHERE i.id = :id"),
    @NamedQuery(name = "Instrument.findByUuid", query = "SELECT i FROM Instrument i WHERE i.uuid = :uuid"),
    @NamedQuery(name = "Instrument.findByNodeIsNull", query = "SELECT i FROM Instrument i WHERE i.node IS NULL")
})
public class Instrument implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;

    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    
//    @NotNull
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
    
    @XmlIDREF
//    @XmlInverseReference(mappedBy="instruments")
    @ManyToOne(optional = false)
    @JoinColumn(name = "node_id", referencedColumnName = "id")
    private Node node;
    
//    @NotNull
    @Column(name = "enabled", nullable = false)
    private boolean enabled = true;
    
//    @NotNull
    @OneToOne(optional = true, cascade={CascadeType.PERSIST})
    @JoinColumn(name = "data_source_id", referencedColumnName = "id", nullable = false)
    private DataSource dataSource;

    public Instrument() {
    }

    public Instrument(String uuid, DataSourceType type) {
        this.uuid = uuid;
        this.dataSource = new DataSource(uuid, type);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instrument)) {
            return false;
        }
        Instrument other = (Instrument) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Instrument[ id=" + id + " ]";
    }
}
