/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "stream_operator" //uniqueConstraints =
//@UniqueConstraint(columnNames = {"customer", "instrument", "type"})
)
@NamedQueries({
    @NamedQuery(name = "StreamOperator.findAll", query = "SELECT so FROM StreamOperator so")
})
public class StreamOperator implements Serializable {

    private static final long serialVersionUID = 1L;
//    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    protected Integer id;
    @Basic(optional = false)
    @Column(name = "operator_class_name")
    private String operatorClassName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOperatorClassName() {
        return operatorClassName;
    }

    public void setOperatorClassName(String operatorClassName) {
        this.operatorClassName = operatorClassName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StreamOperator other = (StreamOperator) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StreamOperator{" + "id=" + id + '}';
    }
}
