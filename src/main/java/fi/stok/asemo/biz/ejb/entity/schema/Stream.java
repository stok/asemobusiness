/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "stream")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"variable", "name", "description"})
@Entity
@Table(name = "stream" //uniqueConstraints =
        //@UniqueConstraint(columnNames = {"customer", "instrument", "type"})
        )
@NamedQueries({
    @NamedQuery(name = "Stream.findAll", query = "SELECT s FROM Stream s")
})
public class Stream implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    protected Integer id;
    
    @XmlIDREF
    @ManyToOne(optional = false)
    @JoinColumn(name = "variable_id", referencedColumnName = "id")
    private Variable variable;
    
    @XmlTransient
//    @XmlInverseReference(mappedBy = "user")
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_settings_id", referencedColumnName = "id")
    private UserSettings userSettings;
    
    @Size(max = 36)
    @Basic(optional = false)
    @Column(name = "name", length = 36)
    private String name;
    
    @Size(max = 255)
    @Basic(optional = false)
    @Column(name = "description")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stream other = (Stream) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{ id=" + id + " }";
    }

    public void afterUnmarshal(Unmarshaller u, Object parent) {
        this.userSettings = (UserSettings) parent;
    }
}
