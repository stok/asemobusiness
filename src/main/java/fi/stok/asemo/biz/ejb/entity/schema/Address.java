/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Juha Loukkola
 */
@Embeddable
public class Address implements Serializable {
    @XmlElement
    @Size(max = 255)
    @Column(name = "street_address", length = 255)
    private String streetAddress;
    
    @XmlElement
    @Size(max = 10)
    @Column(name = "zip_code", length = 10)
    private String zipCode;
    
    @XmlElement
    @Size(max = 255)
    @Column(name = "city", length = 255)  
    private String city;
    
    @Size(max = 255)
    @Column(name = "country", length = 255)
    private String country;

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
}
