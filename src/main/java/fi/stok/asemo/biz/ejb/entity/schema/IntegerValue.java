package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Table(name = "integer_value",
uniqueConstraints =
@UniqueConstraint(columnNames = {"value_group_id", "time_measured"}))
@NamedQueries({
    @NamedQuery(name = "IntegerValue.findAll", query = "SELECT m FROM IntegerValue m"),
    @NamedQuery(name = "IntegerValue.findByValueGroupAndTimeMeasured", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "IntegerValue.findTimeMeasuredRangeByData", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from AND m.timeMeasured <= :to ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "IntegerValue.findFirstByData", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "IntegerValue.findLastByData", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "IntegerValue.findNAfterDateByValueGroup", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured > :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "IntegerValue.findNBeforeDateByValueGroup", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured < :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "IntegerValue.findNBeforeOrAtDateByValueGroup", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured <= :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "IntegerValue.findNAfterOrAtDateByValueGroup", query = "SELECT m FROM IntegerValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "IntegerValue.findCSVRangeByDate", query = "SELECT v.timeMeasured, v.value FROM IntegerValue v WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
})
public class IntegerValue extends Value implements NumericValue, Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "value")
    private Integer value;

    public IntegerValue() {
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = (Integer) value;
    }

    @Override
    public void setValue(Number value) {
        this.value = Math.round(value.floatValue());
    }

    @Override
    public void parseAndSetValue(String value) {
        this.value = Integer.parseInt(value);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IntegerValue)) {
            return false;
        }
        IntegerValue other = (IntegerValue) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "IntegerValue[ id=" + getId() + " ]";
    }
}
