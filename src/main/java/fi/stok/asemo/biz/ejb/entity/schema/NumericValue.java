/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

/**
 *
 * @author Juha Loukkola
 */
public interface NumericValue extends BaseValue{
    
    @Override
    public Number getValue();
    
    public void setValue(Number value);
}
