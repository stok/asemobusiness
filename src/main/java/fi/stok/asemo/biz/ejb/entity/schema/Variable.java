/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "variable")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = { "id", "type", "valueGroups" })
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "variable",
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"customer_id", "data_source_id", "type_id"}))
@NamedQueries({
    @NamedQuery(name = "Variable.findAll", query = "SELECT v FROM Variable v"),
    @NamedQuery(name = "Variable.findByUuid", query = "SELECT v FROM Variable v WHERE v.uuid = :uuid"),
    @NamedQuery(name = "Variable.findAllByCustomerAndDataSource", query = "SELECT v FROM Variable v WHERE v.customer = :customer AND v.dataSource = :dataSource"),
    @NamedQuery(name = "Variable.findByCustomerAndDataSourceAndType", query = "SELECT v FROM Variable v WHERE v.customer = :customer AND v.dataSource = :dataSource AND v.type = :type")
})
public class Variable implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, unique = true)
    protected Integer id;
    
    @XmlID
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
    
//    @XmlTransient
//    @XmlInverseReference(mappedBy = "variables")
    @XmlIDREF
    @ManyToOne(optional = false)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    protected Customer customer;
    
//    @XmlJavaTypeAdapter(DataSourceAdapter.class)
    @XmlIDREF
    @ManyToOne(optional = false)
    @JoinColumn(name = "data_source_id", referencedColumnName = "id")
    protected DataSource dataSource;
    
    @XmlJavaTypeAdapter(TypeAdapter.class)
    @ManyToOne(optional = false)
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    protected VariableType type;
    
    @XmlElementWrapper(name = "valueGroups")
    @XmlElement(name = "valueGroup")
    @OneToMany(mappedBy = "variable", targetEntity = ValueGroup.class,
            cascade = CascadeType.ALL) // fetch = FetchType.LAZY, 
    @OrderColumn
    protected List<ValueGroup> valueGroups;

    void Variable() {
//        this.uuid = UUID.randomUUID().toString();
    }

    public void Variable(String uuid, VariableType type) {
        this.uuid = uuid;
        this.type = type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public VariableType getType() {
        return type;
    }

    public void setType(VariableType type) {
        this.type = type;
    }

    public List<ValueGroup> getValueGroups() {
        return valueGroups;
    }

    public void setValueGroups(List<ValueGroup> valueGroups) {
        this.valueGroups = valueGroups;
    }

    public ValueGroup getValueGroup(Long samplingPeriod) {
        for (ValueGroup vg : this.valueGroups) {
            if (vg.getSamplingPeriod().equals(samplingPeriod)) {
                return vg;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Variable)) {
            return false;
        }
        Variable other = (Variable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Variable[ id=" + id + " ]";
    }

    protected static class TypeAdapter extends XmlAdapter<String, VariableType> {

        @Override
        public String marshal(VariableType v) {
            return v.getUuid();
        }

        @Override
        public VariableType unmarshal(String v) {
            VariableType type = new VariableType();
            type.setUuid(v);
            return type;
        }
    }
    
    private static class DataSourceAdapter extends XmlAdapter<String, DataSource> {

        @Override
        public String marshal(DataSource v) {
            return v.getUuid();        
        }

        @Override
        public DataSource unmarshal(String v) {
            DataSource ds = new DataSource();
            ds.setUuid(v);
            return ds;
        }
    }
    
}
