/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "oauth_accessor")
@NamedQueries({
    @NamedQuery(name = "OAuthAccessor.findAll", query = "SELECT a FROM OAuthAccessor a"),
    @NamedQuery(name = "OAuthAccessor.findByName", query = "SELECT a FROM OAuthAccessor a WHERE a.name = :name"),
    @NamedQuery(name = "OAuthAccessor.findByAccessToken", query = "SELECT a FROM OAuthAccessor a JOIN a.accessCredentials c WHERE c.token = :token"),
    @NamedQuery(name = "OAuthAccessor.findByRequestToken", query = "SELECT a FROM OAuthAccessor a JOIN a.requestCredentials c WHERE c.token = :token")
})
public class OAuthAccessor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private OAuthClient client;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "request_credentials_id", referencedColumnName = "id")
    private OAuthRequestCredentials requestCredentials;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "access_credentials_id", referencedColumnName = "id")
    private OAuthAccessCredentials accessCredentials;
    @ManyToOne
    @JoinColumn(name = "asemo_user", referencedColumnName = "id")
    private User asemoUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OAuthClient getClient() {
        return client;
    }

    public void setClient(OAuthClient client) {
        this.client = client;
    }

    public OAuthRequestCredentials getRequestCredentials() {
        return requestCredentials;
    }

    public void setRequestCredentials(OAuthRequestCredentials requestCredentials) {
        this.requestCredentials = requestCredentials;
    }

    public OAuthAccessCredentials getAccessCredentials() {
        return accessCredentials;
    }

    public void setAccessCredentials(OAuthAccessCredentials accessCredentials) {
        this.accessCredentials = accessCredentials;
    }

    public User getAsemoUser() {
        return asemoUser;
    }

    public void setAsemoUser(User asemoUser) {
        this.asemoUser = asemoUser;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OAuthAccessor)) {
            return false;
        }
        OAuthAccessor other = (OAuthAccessor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.stok.asemo.biz.ejb.entity.schema.OAuthAccessor[ id=" + id + " ]";
    }
}
