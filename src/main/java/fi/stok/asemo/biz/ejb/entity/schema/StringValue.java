/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Table(name = "string_value",
uniqueConstraints =
@UniqueConstraint(columnNames = {"value_group_id", "time_measured"}))
@NamedQueries({
    @NamedQuery(name = "StringValue.findAll", query = "SELECT m FROM StringValue m"),
    @NamedQuery(name = "StringValue.findByTimeMeasured", query = "SELECT m FROM StringValue m WHERE m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "StringValue.findByValueGroupAndTimeMeasured", query = "SELECT m FROM StringValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "StringValue.findTimeMeasuredRangeByData", query = "SELECT m FROM StringValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from AND m.timeMeasured <= :to ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "StringValue.findFirstByData", query = "SELECT m FROM StringValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "StringValue.findLastByData", query = "SELECT m FROM StringValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "StringValue.findNBeforeDateByValueGroup", query = "SELECT m FROM StringValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured < :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "StringValue.findNBeforeOrAtDateByValueGroup", query = "SELECT m FROM StringValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured <= :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "StringValue.findCSVRangeByDate", query = "SELECT v.timeMeasured, v.value FROM StringValue v WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
})
public class StringValue extends Value implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false) 
    @Column(name = "value")
    private String value;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = (String)value;
    }

    @Override
    public void parseAndSetValue(String value) {
        this.value = value;
    }
       
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StringValue)) {
            return false;
        }
        StringValue other = (StringValue) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StringValue[ id=" + getId() + " ]";
    }

}
