package fi.stok.asemo.biz.ejb.entity.schema;

import java.util.Date;

/**
 *
 * @author Juha Loukkola
 */
public interface Job {
    public void setWorkEstimate(Long workEstimate);
    public Long getWorkEstimate();
    
    public void setStatus(String status);
    public String getStatus();
    
    public void setTimeAdded(Date timeAdded);
    public Date getTimeAdded();
    
    public void setTimeStarted(Date timeStarted);
    public Date getTimeStarted();
    
    public void setTimeEnded(Date timeEnded);
    public Date getTimeEnded();
    
    
}
