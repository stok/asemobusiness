/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

/**
 *
 * @author Juha Loukkola
 */
public interface Uuid {
    
    void setUuid(String uuid);
    
    String getUuid();
    
}
