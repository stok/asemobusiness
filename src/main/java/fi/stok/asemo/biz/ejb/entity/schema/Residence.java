package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "residence")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "residence")
public class Residence implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
   
    @XmlElement
    @Column(name = "building_type")
    private String buildingType;
    
    @XmlElement
    @Column(name = "num_residents")
    private int numResidents;
    
    @XmlElement
    @Column(name = "gross_area")
    private int grossArea;
    
    @XmlElement
    @Column(name = "building_volume")
    private int buildingVolume;
    
    @XmlElement
    @Column(name = "certified_energy_consumption")
    private int certifiedEnergyConsumption;
    
    @XmlElement
    @Column(name = "year_of_completion")
    private int yearOfCompletion;
    
    @XmlElement
    @Column(name = "year_of_renovation")
    private int yearOfrenovation;
    
    @XmlElement
    @Column(name = "heating_method")
    private String heatingMethod;
    
    @XmlElement
    @Column(name = "main_insulation_material")
    private String mainInsulationMaterial;
    
    @XmlElement
    @Column(name = "conversion_work", length = 10000)
    private String conversionWork;
    
    @XmlElement
    @Column(name = "heat_recovery_machine")
    private String heatRecoveryMachine;
    
    @XmlElement
    @Column(name = "heat_storing_fireplace")
    private boolean heatStoringFireplace;
    
    @XmlElement
    @Column(name = "solar_heat_collector")
    private boolean solarHeatCollector;
    
    @XmlElement
    @Column(name = "solar_panel")
    private boolean solarPanel;
    
    @XmlElement
    @Column(name = "wind_mill")
    private boolean windMill;
    
    @XmlElement
    @Column(name = "air_heat_pump")
    private boolean airHeatPump;
    
    @XmlElement
    @Column(name = "air_water_heat_pump")
    private boolean airWaterHeatPump;
    
    @XmlElement
    @Column(name = "outgoing_air_heat_pump")
    private boolean outgoingAirHeatPump;
    
    @XmlElement
    @Column(name = "battery")
    private boolean battery;
    
    @XmlElement
    @Column(name = "electric_car")
    private boolean electricCar;
    
    @XmlElement
    @Column(name = "additional_info", length = 10000)
    private String additionalInfo;
    
    @XmlElement
    @Column(name = "other_additional_energy_supply")
    private String otherAdditionalEnergySupply;
    
//    @XmlInverseReference(mappedBy="residence")
    @XmlElement
    @XmlIDREF
    @OneToOne(optional = false)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;

    public Residence() {
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public int getBuildingVolume() {
        return buildingVolume;
    }

    public void setBuildingVolume(int buildingVolume) {
        this.buildingVolume = buildingVolume;
    }

    public int getCertifiedEnergyConsumption() {
        return certifiedEnergyConsumption;
    }

    public void setCertifiedEnergyConsumption(int certifiedEnergyConsumption) {
        this.certifiedEnergyConsumption = certifiedEnergyConsumption;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getGrossArea() {
        return grossArea;
    }

    public void setGrossArea(int grossArea) {
        this.grossArea = grossArea;
    }

    public String getHeatingMethod() {
        return heatingMethod;
    }

    public void setHeatingMethod(String heatingMethod) {
        this.heatingMethod = heatingMethod;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumResidents() {
        return numResidents;
    }

    public void setNumResidents(int numResidents) {
        this.numResidents = numResidents;
    }

    public int getYearOfCompletion() {
        return yearOfCompletion;
    }

    public void setYearOfCompletion(int yearOfCompletion) {
        this.yearOfCompletion = yearOfCompletion;
    }

    public int getYearOfrenovation() {
        return yearOfrenovation;
    }

    public void setYearOfrenovation(int yearOfrenovation) {
        this.yearOfrenovation = yearOfrenovation;
    }

    public String getConversionWork() {
        return conversionWork;
    }

    public void setConversionWork(String conversionWork) {
        this.conversionWork = conversionWork;
    }

    public String getHeatRecoveryMachine() {
        return heatRecoveryMachine;
    }

    public void setHeatRecoveryMachine(String heatRecoveryMachine) {
        this.heatRecoveryMachine = heatRecoveryMachine;
    }

    public String getMainInsulationMaterial() {
        return mainInsulationMaterial;
    }

    public void setMainInsulationMaterial(String mainInsulationMaterial) {
        this.mainInsulationMaterial = mainInsulationMaterial;
    }

    public String getOtherAdditionalEnergySupply() {
        return otherAdditionalEnergySupply;
    }

    public void setOtherAdditionalEnergySupply(String otherAdditionalEnergySupply) {
        this.otherAdditionalEnergySupply = otherAdditionalEnergySupply;
    }

    public boolean isAirHeatPump() {
        return airHeatPump;
    }

    public void setAirHeatPump(boolean airHeatPump) {
        this.airHeatPump = airHeatPump;
    }

    public boolean isAirWaterHeatPump() {
        return airWaterHeatPump;
    }

    public void setAirWaterHeatPump(boolean airWaterHeatPump) {
        this.airWaterHeatPump = airWaterHeatPump;
    }

    public boolean isBattery() {
        return battery;
    }

    public void setBattery(boolean battery) {
        this.battery = battery;
    }

    public boolean isElectricCar() {
        return electricCar;
    }

    public void setElectricCar(boolean electricCar) {
        this.electricCar = electricCar;
    }

    public boolean isHeatStoringFireplace() {
        return heatStoringFireplace;
    }

    public void setHeatStoringFireplace(boolean heatStoringFireplace) {
        this.heatStoringFireplace = heatStoringFireplace;
    }

    public boolean isOutgoingAirHeatPump() {
        return outgoingAirHeatPump;
    }

    public void setOutgoingAirHeatPump(boolean outgoingAirHeatPump) {
        this.outgoingAirHeatPump = outgoingAirHeatPump;
    }

    public boolean isSolarHeatCollector() {
        return solarHeatCollector;
    }

    public void setSolarHeatCollector(boolean solarHeatCollector) {
        this.solarHeatCollector = solarHeatCollector;
    }

    public boolean isSolarPanel() {
        return solarPanel;
    }

    public void setSolarPanel(boolean solarPanel) {
        this.solarPanel = solarPanel;
    }

    public boolean isWindMill() {
        return windMill;
    }

    public void setWindMill(boolean windMill) {
        this.windMill = windMill;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Residence)) {
            return false;
        }
        Residence other = (Residence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Residence[ id=" + id + " ]";
    }
}
