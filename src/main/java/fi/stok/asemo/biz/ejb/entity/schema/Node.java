/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "node")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = { "uuid", "sharedSecret", "hardwareVersion", "softwareVersion", "enabled", "instruments" })
@Entity
@Table(name = "node")
@NamedQueries({
    @NamedQuery(name = "Node.findAll", query = "SELECT n FROM Node n"),
    @NamedQuery(name = "Node.findById", query = "SELECT n FROM Node n WHERE n.id = :id"),
    @NamedQuery(name = "Node.findByDiskJffsFree", query = "SELECT n FROM Node n WHERE n.diskJffsFree = :diskJffsFree"),
    @NamedQuery(name = "Node.findByDiskTmpFree", query = "SELECT n FROM Node n WHERE n.diskTmpFree = :diskTmpFree"),
    @NamedQuery(name = "Node.findByIpAddress", query = "SELECT n FROM Node n WHERE n.ipAddress = :ipAddress"),
    @NamedQuery(name = "Node.findByIpAddressLocal", query = "SELECT n FROM Node n WHERE n.ipAddressLocal = :ipAddressLocal"),
    @NamedQuery(name = "Node.findByLastHeard", query = "SELECT n FROM Node n WHERE n.lastHeard = :lastHeard"),
    @NamedQuery(name = "Node.findByLastRmiFunction", query = "SELECT n FROM Node n WHERE n.lastRmiFunction = :lastRmiFunction"),
    @NamedQuery(name = "Node.findByLastRmiResponse", query = "SELECT n FROM Node n WHERE n.lastRmiResponse = :lastRmiResponse"),
    @NamedQuery(name = "Node.findByUuid", query = "SELECT n FROM Node n WHERE n.uuid = :uuid"),
    @NamedQuery(name = "Node.findByRemoteClock", query = "SELECT n FROM Node n WHERE n.remoteClock = :remoteClock"),
    @NamedQuery(name = "Node.findByRemoteClockDelta", query = "SELECT n FROM Node n WHERE n.remoteClockDelta = :remoteClockDelta"),
    @NamedQuery(name = "Node.findBySharedSecret", query = "SELECT n FROM Node n WHERE n.sharedSecret = :sharedSecret"),
    @NamedQuery(name = "Node.findByCustomer", query = "SELECT n FROM Node n WHERE n.customer = :customer"),
    @NamedQuery(name = "Node.findByCustomerIsNull", query = "SELECT n FROM Node n WHERE n.customer IS NULL")
        })
public class Node implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
     
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @XmlTransient
    @Column(name = "disk_jffs_free")
    private Integer diskJffsFree;
    
    @XmlTransient
    @Column(name = "disk_tmp_free")
    private Integer diskTmpFree;
    
    //TODO: add regular expression pattern for validating IPv4 and IPv6 addresses
    @XmlTransient
    @Size(max = 39)
    @Column(name = "ip_address", length = 39)
    private String ipAddress;
    
    //TODO: add regular expression pattern for validating IPv4 and IPv6 addresses
    @XmlTransient
    @Size(max = 39)
    @Column(name = "ip_address_local", length = 39)
    private String ipAddressLocal;
    
    @XmlTransient
    @Column(name = "last_heard")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastHeard;
    
    @XmlTransient
    @Size(max = 255)
    @Column(name = "last_rmi_function", length = 255)
    private String lastRmiFunction;
    
    @XmlTransient
    @Size(max = 255)
    @Column(name = "last_rmi_response", length = 255)
    private String lastRmiResponse;
    
    @XmlID
    @Size(max = 255)
    @Column(name = "uuid", length = 255, unique = true, nullable = false)
    private String uuid;
    
    @XmlTransient
    @Column(name = "remote_clock")
    @Temporal(TemporalType.TIMESTAMP)
    private Date remoteClock;
    
    @XmlTransient
    @Column(name = "remote_clock_delta")
    private Integer remoteClockDelta;
    
    @Size(max = 255)
    @Column(name = "shared_secret", length = 255)
    private String sharedSecret;

    @Column(name = "enabled")
    private boolean enabled = true;
    
    @XmlIDREF
//    @XmlInverseReference(mappedBy="nodes")
    @ManyToOne(optional = false)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;
    
    @XmlElementWrapper(name = "instruments")
    @XmlElement(name="instrument")
    @OneToMany(mappedBy = "node", targetEntity = Instrument.class,
    fetch = FetchType.EAGER)
    private Collection<Instrument> instruments = new ArrayList<Instrument>();
    
    @Size(max = 255)
    @Column(name = "hardware_version", length = 255)
    private String hardwareVersion;
    
    @Size(max = 255)
    @Column(name = "software_version", length = 255)
    private String softwareVersion;
    
    @XmlTransient
    @Lob 
    @Column(name="last_rmi_message")
    private String lastRmiMessage;

    public Node() {
    }

    public Node(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDiskJffsFree() {
        return diskJffsFree;
    }

    public void setDiskJffsFree(Integer diskJffsFree) {
        this.diskJffsFree = diskJffsFree;
    }

    public Integer getDiskTmpFree() {
        return diskTmpFree;
    }

    public void setDiskTmpFree(Integer diskTmpFree) {
        this.diskTmpFree = diskTmpFree;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddressLocal() {
        return ipAddressLocal;
    }

    public void setIpAddressLocal(String ipAddressLocal) {
        this.ipAddressLocal = ipAddressLocal;
    }

    public Date getLastHeard() {
        return lastHeard;
    }

    public void setLastHeard(Date lastHeard) {
        this.lastHeard = lastHeard;
    }

    public String getLastRmiFunction() {
        return lastRmiFunction;
    }

    public void setLastRmiFunction(String lastRmiFunction) {
        this.lastRmiFunction = lastRmiFunction;
    }

    public String getLastRmiResponse() {
        return lastRmiResponse;
    }

    public void setLastRmiResponse(String lastRmiResponse) {
        this.lastRmiResponse = lastRmiResponse;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getRemoteClock() {
        return remoteClock;
    }

    public void setRemoteClock(Date remoteClock) {
        this.remoteClock = remoteClock;
    }

    public Integer getRemoteClockDelta() {
        return remoteClockDelta;
    }

    public void setRemoteClockDelta(Integer remoteClockDelta) {
        this.remoteClockDelta = remoteClockDelta;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Collection<Instrument> getInstruments() {
        return instruments;
    }

    public void setInstruments(Collection<Instrument> instruments) {
        this.instruments = new HashSet(instruments);
    }

    public String getHardwareVersion() {
        return hardwareVersion;
    }

    public void setHardwareVersion(String hardwareVersion) {
        this.hardwareVersion = hardwareVersion;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getLastRmiMessage() {
        return lastRmiMessage;
    }

    public void setLastRmiMessage(String lastRmiMessage) {
        this.lastRmiMessage = lastRmiMessage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Node)) {
            return false;
        }
        Node other = (Node) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Node[ id=" + id + " ]";
    }
    
    public boolean authenticate(String secret){
        return secret.equals(this.sharedSecret);
    }
    
    public void createSecret() {
        SecureRandom random = new SecureRandom();
        this.setSharedSecret(new BigInteger(256, random).toString(32));
    }
}
