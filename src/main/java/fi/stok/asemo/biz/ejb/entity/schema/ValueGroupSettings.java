/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

/**
 *
 * @author Juha Loukkola
 */

@XmlRootElement(name = "valueGroupSettings")
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(propOrder = { "samplingPeriod", "rotationPeriod", "enabled" })
@Entity
@Table(name = "value_group_settings")
@NamedQueries({
    @NamedQuery(name = "ValueGroupSettings.findAll", query = "SELECT vgs FROM ValueGroupSettings vgs")
})
public class ValueGroupSettings implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @XmlElement
    @Basic(optional = false)
    @Column(name = "sampling_period")   
    private Long samplingPeriod;
    
    @XmlElement
    @Basic(optional = false)
    @Column(name = "rotation_period")
    private Long rotationPeriod;
    
    @XmlElement
    @Basic(optional = false)
    @Column(name = "enabled")
    private Boolean enabled = false;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "variable_type_id", referencedColumnName = "id")
    private VariableType variableType;
    
    @OneToMany(mappedBy = "settings", targetEntity = ValueGroup.class,
    fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ValueGroup> valueGroups;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getSamplingPeriod() {
        return samplingPeriod;
    }

    public void setSamplingPeriod(Long samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }

    public Long getRotationPeriod() {
        return rotationPeriod;
    }

    public void setRotationPeriod(Long rotationPeriod) {
        this.rotationPeriod = rotationPeriod;
    }

    public VariableType getVariableType() {
        return variableType;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setVariableType(VariableType variableType) {
        this.variableType = variableType;
    }

    public List<ValueGroup> getValueGroups() {
        return valueGroups;
    }

    public void setValueGroups(List<ValueGroup> valueGroups) {
        this.valueGroups = valueGroups;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ValueGroupSettings other = (ValueGroupSettings) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ValueGroupSettings{" + "id=" + id + '}';
    }
    
}
