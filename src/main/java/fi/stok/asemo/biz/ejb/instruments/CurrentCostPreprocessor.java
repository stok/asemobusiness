/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.instruments;

import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public class CurrentCostPreprocessor extends Preprocessor {

    /*
     * NOTE!: Values should only contain one line
     *
     */
    @Override
    public List<List<String>> preprocess(List<List<String>> values, Instrument instrument) {
        for (List<String> line : values) {
            // Remove all but the first 2 elements from a line
            Iterator iterator = line.listIterator(2);
            while (iterator.hasNext()) {
                iterator.next();
                iterator.remove();                
            }
        }
        return values;
    }
}
