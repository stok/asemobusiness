/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "oauth_access_credentials")
@NamedQueries({
    @NamedQuery(name = "OAuthAccessCredentials.findAll", query = "SELECT c FROM OAuthAccessCredentials c"),
    @NamedQuery(name = "OAuthAccessCredentials.findByToken", query = "SELECT c FROM OAuthAccessCredentials c WHERE c.token = :token")})
//@Embeddable
public class OAuthAccessCredentials implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "token", nullable = false)
    private String token;
    @Column(name = "secret", nullable = false)
    private String secret;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "valid_until", nullable = false)
    private Date validUntil;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

}
