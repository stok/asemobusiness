/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.*;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "valueGroup")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"enabled", "samplingPeriod", "rotationPeriod"})
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "value_group")
@NamedQueries({
    @NamedQuery(name = "ValueGroup.findAllOrderById", query = "SELECT vg FROM ValueGroup vg ORDER BY vg.id ASC")
})
public class ValueGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    protected Integer id;
    
    @Basic(optional = true)
    @Column(name = "sampling_period", nullable = true)
    private Long samplingPeriod;
    
    @Basic(optional = false)
    @Column(name = "rotation_period")
    private Long rotationPeriod;
    
    @Basic(optional = false)
    @Column(name = "enabled")
    private Boolean enabled = false;

    //    @XmlIDREF
    @XmlTransient
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "variable_id", referencedColumnName = "id")
    private Variable variable = null;
    
    @XmlTransient
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "settings_id", referencedColumnName = "id")
    private ValueGroupSettings settings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getSamplingPeriod() {
        return samplingPeriod;
    }

    public void setSamplingPeriod(Long samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }

    public Long getRotationPeriod() {
        return rotationPeriod;
    }

    public void setRotationPeriod(Long rotationPeriod) {
        this.rotationPeriod = rotationPeriod;
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    public ValueGroupSettings getSettings() {
        return settings;
    }

    public void setSettings(ValueGroupSettings settings) {
        this.settings = settings;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValueGroup)) {
            return false;
        }
        ValueGroup other = (ValueGroup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "ValueGroup[ id=" + id + " ]";
    }
    public static Comparator<ValueGroup> SamplingPeriodComparator = new Comparator<ValueGroup>() {
        @Override
        public int compare(ValueGroup vg1, ValueGroup vg2) {
            return vg1.getSamplingPeriod().compareTo(vg2.getSamplingPeriod());
        }
    };
//    @Override
//    public int compareTo(Object o) {
//        ValueGroup vg = (ValueGroup) o;
//        return this.getSamplingPeriod().compareTo(vg.getSamplingPeriod());
//    }

    public void afterUnmarshal(Unmarshaller u, Object parent) {
        this.variable = (Variable) parent;
    }
}
