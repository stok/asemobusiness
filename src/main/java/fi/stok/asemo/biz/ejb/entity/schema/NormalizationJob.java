/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "normalization_job")
@NamedQueries({
    @NamedQuery(name = "NormalizationJob.findAll", query = "SELECT j FROM NormalizationJob j"),
    @NamedQuery(name = "NormalizationJob.findByStatusOrderedByPriorityDesc", query = "SELECT j FROM NormalizationJob j WHERE j.status = :status ORDER BY j.priority DESC"),
    @NamedQuery(name = "NormalizationJob.deleteOlderThanByStatus", query = "DELETE FROM NormalizationJob j WHERE j.status = :status AND j.timeEnded < :from")
})
public class NormalizationJob implements Serializable, Job {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "JobSequence")
    @SequenceGenerator(name = "JobSequence", sequenceName = "JOB_SEQUENCE", allocationSize = 1)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
//    @ManyToOne(optional = true)
//    @JoinColumn(name = "job_queue_id", referencedColumnName = "id")
//    private NormalizationJobQueue jobQueue;
    @Basic(optional = false)
    @Column(name = "priority")
    private int priority;
    @Basic(optional = false)
    @Column(name = "work_estimate")
    private Long workEstimate;
    @Basic(optional = false)
    @Column(name = "work_done")
    private Long workDone;
    @Basic(optional = false)
    @Column(name = "status")
    private String status;
    @Basic(optional = true)
    @Column(name = "time_added")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeAdded;
    @Basic(optional = true)
    @Column(name = "time_started")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStarted;
    @Basic(optional = true)
    @Column(name = "time_ended")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeEnded;
//    @OneToOne(optional = false)
//    @JoinColumn(name = "variable_id", referencedColumnName = "id")    
//    private Variable variable;  
//    
//    @Basic(optional = false)
//    @Column(name = "samplingPeriod")
//    private Long samplingPeriod;
    @ManyToOne(optional = false)
    @JoinColumn(name = "value_group_id", referencedColumnName = "id")
    private ValueGroup valueGroup;
    @Basic(optional = false)
    @Column(name = "n_from")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date from;
    private Long from;
    @Basic(optional = false)
    @Column(name = "n_to")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date to;
    private Long to;

    public NormalizationJob() {
    }

    //private List<NormalizationJob> dependsOn = new ArrayList<NormalizationJob>();
//    public NormalizationJob(NormalizationJobQueue jobQueue, ValueGroup valueGroup, Date from, Date to, Long workEstimate) {
//        this.jobQueue = jobQueue;
//        this.valueGroup = valueGroup;
//        this.timeAdded = new Date();
//        this.status = "READY";
//        this.from = from;
//        this.to = to;
//        this.workEstimate = workEstimate;
//    }
    public NormalizationJob(ValueGroup valueGroup, Long from, Long to, Long workEstimate) {
        this.valueGroup = valueGroup;
        this.timeAdded = new Date();
        this.status = "READY";
        this.from = from;
        this.to = to;
        this.workEstimate = workEstimate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public Long getWorkEstimate() {
        return workEstimate;
    }

    @Override
    public void setWorkEstimate(Long workEstimate) {
        this.workEstimate = workEstimate;
    }

    public Long getWorkDone() {
        return workDone;
    }

    public void setWorkDone(Long workDone) {
        this.workDone = workDone;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Date getTimeAdded() {
        return timeAdded;
    }

    @Override
    public void setTimeAdded(Date timeAdded) {
        this.timeAdded = timeAdded;
    }

    @Override
    public Date getTimeStarted() {
        return timeStarted;
    }

    @Override
    public void setTimeStarted(Date timeStarted) {
        this.timeStarted = timeStarted;
    }

    @Override
    public Date getTimeEnded() {
        return timeEnded;
    }

    @Override
    public void setTimeEnded(Date timeEnded) {
        this.timeEnded = timeEnded;
    }

    public ValueGroup getValueGroup() {
        return valueGroup;
    }

    public void setValueGroup(ValueGroup valueGroup) {
        this.valueGroup = valueGroup;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NormalizationJob)) {
            return false;
        }
        NormalizationJob other = (NormalizationJob) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[ id=" + id + " ]";
    }
    /**
     * TSLV Time since last normalized value in days. Prioritize time series
     * that are behind others. WE Work estimate. Prioritize larger jobs over
     * small ones. TSA Time since added in minutes. Prioritize jobs that have
     * been longer on the queue. This should dominate over all other parameters
     * when a job have been waiting more than 60 min.
     *
     * TSLV + B*WE + [C*MAX(TSA - 60, 0)]
     *
     *
     */
    public static Comparator<NormalizationJob> DescendingPriorityComparator = new Comparator<NormalizationJob>() {
        @Override
        public int compare(NormalizationJob job1, NormalizationJob job2) {
            return -1 * Integer.valueOf(job1.getPriority()).compareTo(job2.getPriority());
        }
    };
}
