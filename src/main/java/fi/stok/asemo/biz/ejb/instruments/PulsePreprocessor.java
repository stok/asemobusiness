/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.instruments;

import fi.stok.asemo.biz.WSBusinessLocal;
import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * IN: (unixtime, pulse )
 *
 * OUT: (unixtime, pulse, pulseconsumption)
 *
 * @author Juha Loukkola, kristian
 */
public class PulsePreprocessor extends Preprocessor {

    WSBusinessLocal wSBusiness = lookupWSBusinessBeanLocal();

    /*
     * NOTE!: Values should only contain one line
     *
     */
    @Override
    public List<List<String>> preprocess(List<List<String>> values,
            Instrument instrument) {
        DataSchema dataSchema =
                instrument.getDataSource().getType().getDataSchema();
        ArrayList<VariableType> schemaElements = new ArrayList(dataSchema.getSchemaElementCollection());

        for (List<String> line : values) {

            int timestamp = Integer.parseInt(line.get(0));

            /*
             * Calculate pulse measured consumption
             *
             * Pulse values are cumulative counters. Therefore a previous value
             * of the counter is needed for calculating the change which
             * represent amount of counted pulses at that interval
             */
            int indexOfValue = 1;
            Variable var = instrument.getDataSource().getVariable(schemaElements.get(indexOfValue - 1));
            Value lastValue = this.wSBusiness.getLastValue(var);
            float pulseConsumption = 0;
            if (lastValue != null) {
                pulseConsumption = derivatePulseCounter((Integer) lastValue.getValue(), Integer.parseInt(line.get(indexOfValue)),
                        lastValue.getTimeMeasured(), timestamp * 1000L, 1).floatValue();
            }
            line.add(Float.toString(pulseConsumption));
        }
        return values;
    }

    private Number derivatePulseCounter(Integer lastValue, Integer value,
            Long lastTimestamp, Long timestamp, float pulseMultiplier) {
        // Check if the counter has been reseted 
        if (value < lastValue) {
            lastValue = 0;
        }
        return (pulseMultiplier * (value - lastValue)) / (timestamp - lastTimestamp);
    }

    private WSBusinessLocal lookupWSBusinessBeanLocal() {
        try {
            Context c = new InitialContext();
            return (WSBusinessLocal) c.lookup("java:global/Asemo/AsemoBizModule/WSBusinessBean!fi.stok.asemo.biz.ejb.WSBusinessLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
