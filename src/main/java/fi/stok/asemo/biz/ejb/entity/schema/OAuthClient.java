/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/*
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "oauth_client")
@NamedQueries({
    @NamedQuery(name = "OAuthClient.findAll", query = "SELECT c FROM OAuthClient c"),
    @NamedQuery(name = "OAuthClient.findByName", query = "SELECT c FROM OAuthClient c WHERE c.name = :name"),
    @NamedQuery(name = "OAuthClient.findByToken", query = "SELECT c FROM OAuthClient c WHERE c.token = :token")})
public class OAuthClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description", length = 2000)
    private String description;
    @Column(name = "token", nullable = false)
    private String token;
    @Column(name = "secret", nullable = false)
    private String secret;
    @OneToMany(mappedBy = "client", targetEntity = OAuthAccessor.class,
            fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<OAuthAccessor> accessors;

    public OAuthClient() {
    }

    public OAuthClient(String token,
            String secret) {
        this.token = token;
        this.secret = secret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public List<OAuthAccessor> getAccessors() {
        return accessors;
    }

    public void setAccessors(List<OAuthAccessor> accessors) {
        this.accessors = accessors;
    }

}
