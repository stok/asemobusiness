/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "oauth_request_credentials")
@NamedQueries({
    @NamedQuery(name = "OAuthRequestCredentials.findAll", query = "SELECT c FROM OAuthRequestCredentials c"),
    @NamedQuery(name = "OAuthRequestCredentials.findByToken", query = "SELECT c FROM OAuthRequestCredentials c WHERE c.token = :token")})
public class OAuthRequestCredentials implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "token", nullable = false)
    private String token;
    @Column(name = "secret", nullable = false)
    private String secret;
    @Column(name = "authorized", nullable = false)
    private Boolean authorized;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(Boolean authorized) {
        this.authorized = authorized;
    }

}
