/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Table(name = "double_value",
uniqueConstraints =
@UniqueConstraint(columnNames = {"value_group_id", "time_measured"}))
@NamedQueries({
    @NamedQuery(name = "DoubleValue.findAll", query = "SELECT m FROM DoubleValue m"),
    @NamedQuery(name = "DoubleValue.findByTimeMeasured", query = "SELECT m FROM DoubleValue m WHERE m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "DoubleValue.findByValueGroupAndTimeMeasured", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "DoubleValue.findTimeMeasuredRangeByData", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from AND m.timeMeasured <= :to ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "DoubleValue.findFirstByData", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "DoubleValue.findLastByData", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "DoubleValue.findNAfterDateByValueGroup", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured > :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "DoubleValue.findNBeforeDateByValueGroup", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured < :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "DoubleValue.findNBeforeOrAtDateByValueGroup", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured <= :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "DoubleValue.findNAfterOrAtDateByValueGroup", query = "SELECT m FROM DoubleValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "DoubleValue.findCSVRangeByDate", query = "SELECT v.timeMeasured, v.value FROM DoubleValue v WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
})
public class DoubleValue extends Value implements NumericValue, Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "value")
    private double value;

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = (Double) value;
    }

    @Override
    public void setValue(Number value) {
        this.value = value.doubleValue();
    }

    @Override
    public void parseAndSetValue(String value) {
        this.value = Double.parseDouble(value);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DoubleValue)) {
            return false;
        }
        DoubleValue other = (DoubleValue) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DoubleValue[ id=" + getId() + " ]";
    }
}
