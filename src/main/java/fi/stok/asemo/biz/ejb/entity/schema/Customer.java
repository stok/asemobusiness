package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"uuid", "name", "email", "phone", "streetAddress", "zipCode", "city", "user", "residence", "nodes", "variables"})
@Entity
@Table(name = "customer")
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
    @NamedQuery(name = "Customer.findById", query = "SELECT c FROM Customer c WHERE c.id = :id"),
    @NamedQuery(name = "Customer.findByUuid", query = "SELECT c FROM Customer c WHERE c.uuid = :uuid"),
    @NamedQuery(name = "Customer.findByCity", query = "SELECT c FROM Customer c WHERE c.city = :city"),
    @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email"),
    @NamedQuery(name = "Customer.findByName", query = "SELECT c FROM Customer c WHERE c.name = :name"),
    @NamedQuery(name = "Customer.findByStreetAddress", query = "SELECT c FROM Customer c WHERE c.streetAddress = :streetAddress"),
    @NamedQuery(name = "Customer.findByZipCode", query = "SELECT c FROM Customer c WHERE c.zipCode = :zipCode")})
public class Customer implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
//    @XmlElement
//    @NotNull
    @XmlID
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
//    @XmlElement
    //@Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")
    @Size(max = 256)
    @Column(name = "email", length = 256)
    private String email;
//    @XmlElement
    @Size(max = 20)
    @Column(name = "phone", length = 20)
    private String phone;
//    @XmlElement
    @Size(max = 255)
    @Column(name = "name", length = 255)
    private String name;
//    @XmlElement
    @Size(max = 255)
    @Column(name = "street_address", length = 255)
    private String streetAddress;
//    @XmlElement
    @Size(max = 10)
    @Column(name = "zip_code", length = 10)
    private String zipCode;
//    @XmlElement
    @Size(max = 255)
    @Column(name = "city", length = 255)
    private String city;
//    @Size(max = 255)
//    @Column(name = "country", length = 255)
//    private String country;
//    @XmlElement
    @OneToOne(mappedBy = "customer", targetEntity = User.class, cascade = CascadeType.REMOVE)
    private User user;
    @XmlElementWrapper(name = "nodes")
    @XmlElement(name = "node")
    @OneToMany(mappedBy = "customer", targetEntity = Node.class,
            fetch = FetchType.EAGER)
    private Collection<Node> nodes = new ArrayList<Node>();
//    @XmlElement
    @OneToOne(mappedBy = "customer", targetEntity = Residence.class,
            cascade = CascadeType.ALL)
    private Residence residence;
    @XmlElementWrapper(name = "variables")
    @XmlElement(name = "variable")
    @OneToMany(mappedBy = "customer", targetEntity = Variable.class,
            fetch = FetchType.EAGER) //, cascade = CascadeType.REMOVE)
    private Collection<Variable> variables = new ArrayList<Variable>();

    public Customer() {
//        this.uuid = UUID.randomUUID().toString();
    }

    public Customer(Integer id) {
        this();
        this.id = id;
    }

    public Customer(String name, String streetAddress, String zipCode, String city, String email, String phone) {
        this();
        this.city = city;
        this.email = email;
        this.name = name;
        this.streetAddress = streetAddress;
        this.zipCode = zipCode;
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User users) {
        this.user = users;
    }

    public Collection<Node> getNodes() {
        return nodes;
    }

    public void setNodes(Collection<Node> nodes) {
        this.nodes = nodes;
    }

    public Residence getResidence() {
        return residence;
    }

    public void setResidence(Residence residence) {
        this.residence = residence;
    }

    public Collection<Variable> getVariables() {
        return variables;
    }

    public Variable getVariable(String uuid) {
        for (Variable var : this.variables) {
            if (var.getUuid().equals(uuid)) {
                return var;
            }
        }
        return null;
    }

    public void setVariables(Collection<Variable> variables) {
        this.variables = variables;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Customer[ id=" + id + " ]";
    }
}
