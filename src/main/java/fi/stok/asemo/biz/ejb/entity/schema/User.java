package fi.stok.asemo.biz.ejb.entity.schema;

import fi.stok.asemo.biz.exception.PasswordException;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "user")
//@XmlAccessorType(XmlAccessType.NONE)
@XmlType(propOrder = {"username", "passwordHash", "customer", "userSettings"})
@Entity
@Table(name = "user",
        uniqueConstraints =
        @UniqueConstraint(columnNames = {"username"}))
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
    @NamedQuery(name = "User.findByPasswordHash", query = "SELECT u FROM User u WHERE u.passwordHash = :passwordHash"),
    @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
    @NamedQuery(name = "User.findByCustomer", query = "SELECT u FROM User u WHERE u.customer = :customer"),
    @NamedQuery(name = "User.findByCustomerIsNull", query = "SELECT u FROM User u WHERE u.customer IS NULL")
})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "password_hash")
    private byte[] passwordHash;
    
    @Basic(optional = false)
    @Size(min = 1, max = 255)
    @Column(name = "username", length = 255)
    private String username;
    
    @OneToOne(optional = true)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;
    
    @OneToOne(mappedBy = "user", targetEntity = UserSettings.class,
            cascade = CascadeType.ALL)
    private UserSettings userSettings;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_group_mapping",
            joinColumns = {
        @JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {
        @JoinColumn(name = "group_id", referencedColumnName = "id")})
    private List<UserGroup> groups;

    public User() {
        this.groups = new ArrayList<UserGroup>();
    }

    public User(Integer id) {
        this();
        this.id = id;
    }

    public User(String username, String password) throws PasswordException, IllegalArgumentException, NullPointerException {
        this();
        if (!username.isEmpty() && !password.isEmpty()) {
            this.username = username;
            this.storePassword(password);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public User(String username, String password, Customer customer) throws PasswordException, IllegalArgumentException, NullPointerException {
        this(username, password);
        if (customer != null) {
            this.customer = customer;
        } else {
            throw new NullPointerException();
        }
    }

    @XmlTransient
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    @XmlID
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlIDREF
//    @XmlInverseReference(mappedBy = "user")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    @XmlTransient
    public List<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<UserGroup> groups) {
        this.groups = groups;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User[ id=" + id + " ]";
    }

    public final void storePassword(String password) throws PasswordException {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            this.passwordHash = md.digest(password.getBytes());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw new PasswordException();
        }
    }

    public final String generateAndStorePassword() throws PasswordException {
        SecureRandom random = new SecureRandom();
        String pw = new BigInteger(40, random).toString(32);
        this.storePassword(pw);
        return pw;
    }

    public boolean authenticate(String password) throws PasswordException {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(password.getBytes());
            return Arrays.equals(hash, this.passwordHash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            throw new PasswordException();
        }
    }

    public boolean inRole(String role) {
        for (UserGroup group : this.groups) {
            if (group.getName().equals(role)) {
                return true;
            }
        }
        return false;
    }
}
