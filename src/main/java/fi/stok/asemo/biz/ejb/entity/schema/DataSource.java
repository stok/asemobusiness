package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "dataSource")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "data_source")
@NamedQueries({
    @NamedQuery(name = "DataSource.findAll", query = "SELECT i FROM DataSource i"),
    @NamedQuery(name = "DataSource.findById", query = "SELECT i FROM DataSource i WHERE i.id = :id"),
    @NamedQuery(name = "DataSource.findByUuid", query = "SELECT i FROM DataSource i WHERE i.uuid = :uuid")
})
public class DataSource implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    
//    @NotNull
    @XmlID
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
    
    @XmlJavaTypeAdapter(DataSourceTypeAdapter.class)
    @ManyToOne(optional = false)
//    @NotNull
    @JoinColumn(name = "type_id", referencedColumnName = "id", nullable = false)
    private DataSourceType type;
    
    @XmlIDREF
    @XmlElementWrapper(name = "variables")
    @XmlElement(name = "variable")
    @OneToMany(mappedBy = "dataSource", targetEntity = Variable.class,
    fetch = FetchType.LAZY)
    @OrderColumn
    private List<Variable> variables = new ArrayList<Variable>();
    
    public DataSource() {
    }

    public DataSource(String uuid, DataSourceType type) {
        this.uuid = uuid;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public DataSourceType getType() {
        return type;
    }

    public void setType(DataSourceType type) {
        this.type = type;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public Variable getVariable(VariableType se) {
        for (Variable var : this.variables) {
            if (var.getType().equals(se)) {
                return var;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataSource)) {
            return false;
        }
        DataSource other = (DataSource) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataSource[ id=" + id + " ]";
    }
    
    protected static class DataSourceTypeAdapter extends XmlAdapter<String, DataSourceType> {

        @Override
        public DataSourceType unmarshal(String v) throws Exception {
            DataSourceType type = new DataSourceType();
            type.setUuid(v);
            return type;
        }

        @Override
        public String marshal(DataSourceType v) throws Exception {
            return v.getUuid();
        }
    }
    
}
