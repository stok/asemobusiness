/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.instruments;

import fi.stok.asemo.biz.WSBusinessLocal;
import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * IN: (unixtime, current1, current2, current3, watts1, watts2, watts3, voltage,
 * backupinterval, pulse1, pulse2, pulse3, cumwatts1, cumwatts2, cumwatts3)
 *
 * OUT: (unixtime, current1, current2, current3, watts1, watts2, watts3,
 * voltage, backupinterval, pulse1, pulse2, pulse3, cumwatts1, cumwatts2,
 * cumwatts3, current_total, watts_total, cumwatts_total, teleheating_watts,
 * waterconsumption)
 *
 * @author Juha Loukkola
 */
//@Stateless
public class EOhjainPreprocessor extends Preprocessor {

    WSBusinessLocal wSBusiness = lookupWSBusinessBeanLocal();

    /*
     * NOTE!: Values should only contain one line
     *
     */
    @Override
    public List<List<String>> preprocess(List<List<String>> values, Instrument instrument) {
        DataSchema dataSchema = instrument.getDataSource().getType().getDataSchema();
        ArrayList<VariableType> schemaElements = new ArrayList(dataSchema.getSchemaElementCollection());

        for (List<String> line : values) {
            // Calculate total current
            Integer current1 = Integer.parseInt(line.get(1)) / 1000;
            line.set(1, current1.toString());
            Integer current2 = Integer.parseInt(line.get(2)) / 1000;
            line.set(2, current2.toString());
            Integer current3 = Integer.parseInt(line.get(3)) / 1000;
            line.set(3, current3.toString());
            int totalCurrent = current1 + current2 + current3;
            line.add(Integer.toString(totalCurrent));

            // Calculate total power
            int watts = Integer.parseInt(line.get(4))
                    + Integer.parseInt(line.get(5))
                    + Integer.parseInt(line.get(6));
            line.add(Integer.toString(watts));

            // Calculate total energy consumption
            int cumwatts = Integer.parseInt(line.get(12))
                    + Integer.parseInt(line.get(13))
                    + Integer.parseInt(line.get(14));
            line.add(Integer.toString(cumwatts));

            int timestamp = Integer.parseInt(line.get(0));

            /*
             * Calculate teleheating power
             *
             * Pulse values are cumulative counters. Therefore a previous value
             * of the counter is needed for calculating the change which
             * represent heating power at that interval
             */
            int indexOfValue = 10;
            Variable var = instrument.getDataSource().getVariable(schemaElements.get(indexOfValue - 1));
            Value lastValue = this.wSBusiness.getLastValue(var);
            float teleheatingPower = 0;
            if (lastValue != null) {
                teleheatingPower = derivatePulseCounter((Integer) lastValue.getValue(), Integer.parseInt(line.get(indexOfValue)), lastValue.getTimeMeasured(), timestamp * 1000L, 10).floatValue();
            }
            line.add(Float.toString(teleheatingPower));


            /*
             * Calculate water consumption
             *
             * Pulse values are cumulative counters. Therefore a previous value
             * of the counter is needed for calculating the change which
             * represent heating power at that interval
             */
            indexOfValue = 9;
            var = instrument.getDataSource().getVariable(schemaElements.get(indexOfValue - 1));
            lastValue = this.wSBusiness.getLastValue(var);
            float waterConsumption = 0;
            if (lastValue != null) {
                waterConsumption = derivatePulseCounter((Integer) lastValue.getValue(), Integer.parseInt(line.get(indexOfValue)), lastValue.getTimeMeasured(), timestamp * 1000L, 10).floatValue();
            }
            line.add(Float.toString(waterConsumption));
        }
        return values;
    }

    private Number derivatePulseCounter(Integer lastValue, Integer value, Long lastTimestamp, Long timestamp, float pulseMultiplier) {
        // Check if the counter has been reseted 
        if (value < lastValue) {
            lastValue = 0;
        }
        return (pulseMultiplier * (value - lastValue)) / (timestamp - lastTimestamp);
    }

    private WSBusinessLocal lookupWSBusinessBeanLocal() {
        try {
            Context c = new InitialContext();
            return (WSBusinessLocal) c.lookup("java:global/Asemo/AsemoBizModule/WSBusinessBean!fi.stok.asemo.biz.ejb.WSBusinessLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
