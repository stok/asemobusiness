package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Value implements Serializable, BaseValue {

    public static final List<String> subClasses = new ArrayList<String>();

    static {
        subClasses.add(IntegerValue.class.getName());
        subClasses.add(LongValue.class.getName());
        subClasses.add(FloatValue.class.getName());
        subClasses.add(DoubleValue.class.getName());
        subClasses.add(StringValue.class.getName());
        subClasses.add(BooleanValue.class.getName());
    }
    private static final long serialVersionUID = 1L;
    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @GeneratedValue(generator = "ValueSequence")
    @SequenceGenerator(name = "ValueSequence", sequenceName = "VALUE_SEQUENCE", allocationSize = 100)
    @Basic(optional = false)
    @Column(name = "id")
    protected Long id;
    @NotNull
    @Basic(optional = false)
    @Column(name = "time_stored")
//    @Temporal(TemporalType.TIMESTAMP)
//    protected Date timeStored;
    protected Long timeStored;
    @NotNull
    @Basic(optional = false)
    @Column(name = "time_measured", unique = false)
//    @Temporal(TemporalType.TIMESTAMP)
//    protected Date timeMeasured;
    protected Long timeMeasured;
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "value_group_id", referencedColumnName = "id")
    protected ValueGroup valueGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getTimeStored() {
        return timeStored;
    }

    public void setTimeStored(Long timeStored) {
        this.timeStored = timeStored;
    }

    @Override
    public Long getTimeMeasured() {
        return timeMeasured;
    }

    public void setTimeMeasured(Long timeMeasured) {
        this.timeMeasured = timeMeasured;
    }

    @Override
    public ValueGroup getValueGroup() {
        return valueGroup;
    }

    @Override
    public void setValueGroup(ValueGroup valueGroup) {
        this.valueGroup = valueGroup;
    }

    @Override
    public abstract Object getValue();

    @Override
    public abstract void setValue(Object value);

    public abstract void parseAndSetValue(String value);
    public static Comparator<BaseValue> TimeMeasuredComparator = new Comparator<BaseValue>() {
        @Override
        public int compare(BaseValue value1, BaseValue value2) {
            return Long.valueOf(value1.getTimeMeasured()).compareTo(value2.getTimeMeasured());
        }
    };
}
