/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.util.Date;

/**
 *
 * @author Juha Loukkola
 */
public interface BaseValue {

    public Long getTimeMeasured();

    public void setTimeMeasured(Long timeMeasured);

    public Long getTimeStored();

    public void setTimeStored(Long timeStored);

    public ValueGroup getValueGroup();

    public void setValueGroup(ValueGroup valueGroup);

    public Object getValue();

    public void setValue(Object value);
}
