package fi.stok.asemo.biz.ejb.entity.schema;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * NOTE: This class could use Join or Separate_table inheritance strategies
 *
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "listDataSchema")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
//@Table(name = "list_data_schema")
@NamedQueries({
    @NamedQuery(name = "ListDataSchema.findAll", query = "SELECT ds FROM ListDataSchema ds")
})
public class ListDataSchema extends DataSchema {

    private static final long serialVersionUID = 1L;
    
    @XmlElementWrapper(name = "schema")
    @XmlElement(name = "variableType")
    @OneToMany(mappedBy = "dataSchema", targetEntity = VariableType.class,
    fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderColumn
    private List<VariableType> schema = new ArrayList<VariableType>();

    public ListDataSchema() {
        this.type = "ListDataSchema";
    }
    
    public ListDataSchema(String name, String description) {
        this();
        this.name = name;
        this.description = description;
    }

    public List<VariableType> getElements() {
        return schema;
    }

    public void setElements(List<VariableType> elements) {
        this.schema = elements;
    }

    @Override
    public Collection<VariableType> getSchemaElementCollection() {
        return this.schema;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListDataSchema)) {
            return false;
        }
        ListDataSchema other = (ListDataSchema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ListDataSchema[ id=" + id + " ]";
    }
}
