package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juha Loukkola
 */
@XmlRootElement(name = "dataSourceType")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ListDataSchema.class, DataSchema.class})
@Entity
@Table(name = "data_source_type")
@NamedQueries({
    @NamedQuery(name = "DataSourceType.findAll", query = "SELECT i FROM DataSourceType i"),
    @NamedQuery(name = "DataSourceType.findByUuid", query = "SELECT i FROM DataSourceType i WHERE i.uuid = :uuid"),
    @NamedQuery(name = "DataSourceType.findByName", query = "SELECT i FROM DataSourceType i WHERE i.name = :name")
})
public class DataSourceType implements Serializable, Uuid {

    private static final long serialVersionUID = 1L;
    
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @XmlID
    @Basic(optional = false)
    @Column(name = "uuid", nullable = false, unique = true)
    private String uuid;
    
    @Size(min = 1, max = 255)
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Size(min = 1, max = 255)
    @Basic(optional = false)
    @Column(name = "description")   
    private String description;

    //    @NotNull
    @XmlElementRef
    @ManyToOne(optional = false)
    @JoinColumn(name = "data_schema_id", referencedColumnName = "id")
    private DataSchema dataSchema;
    
    @Basic(optional = false)
    @Column(name = "preprocessor")
    private String preprocessor;

    public DataSourceType() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataSchema getDataSchema() {
        return dataSchema;
    }

    public void setDataSchema(DataSchema dataSchema) {
        this.dataSchema = dataSchema;
    }

    public String getPreprocessor() {
        return preprocessor;
    }

    public void setPreprocessor(String preprocessor) {
        this.preprocessor = preprocessor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataSourceType)) {
            return false;
        }
        DataSourceType other = (DataSourceType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataSourceType[ id=" + id + " ]";
    }
    
//    private static class DataSchemaAdapter extends XmlAdapter<String, DataSchema> {
//
//        @Override
//        public DataSchema unmarshal(String v) throws Exception {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public String marshal(DataSchema v) throws Exception {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//        
//    }
}
