/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.instruments;

import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public abstract class Preprocessor {
    
    public static final List<String> subClasses = new ArrayList<String>();
   
    static {
        subClasses.add(EOhjainPreprocessor.class.getName());
        subClasses.add(CurrentCostPreprocessor.class.getName());
    }
    
    public abstract List<List<String>> preprocess(List<List<String>> values, Instrument instrument);
}
