/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Table(name = "long_value",
uniqueConstraints =
@UniqueConstraint(columnNames = {"value_group_id", "time_measured"}))
@NamedQueries({
    @NamedQuery(name = "LongValue.findAll", query = "SELECT m FROM LongValue m"),
    @NamedQuery(name = "LongValue.findByTimeMeasured", query = "SELECT m FROM LongValue m WHERE m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "LongValue.findByValueGroupAndTimeMeasured", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "LongValue.findTimeMeasuredRangeByData", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from AND m.timeMeasured <= :to ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "LongValue.findFirstByData", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "LongValue.findLastByData", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "LongValue.findNAfterDateByValueGroup", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured > :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "LongValue.findNBeforeDateByValueGroup", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured < :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "LongValue.findNBeforeOrAtDateByValueGroup", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured <= :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "LongValue.findNAfterOrAtDateByValueGroup", query = "SELECT m FROM LongValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "LongValue.findCSVRangeByDate", query = "SELECT v.timeMeasured, v.value FROM LongValue v WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
    
})
public class LongValue extends Value implements NumericValue, Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "value")
    private long value;

    @Override
    public Long getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = (Long) value;
    }

    @Override
    public void setValue(Number value) {
        this.value = Math.round(value.doubleValue());
    }

    @Override
    public void parseAndSetValue(String value) {
        this.value = Long.parseLong(value);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LongValue)) {
            return false;
        }
        LongValue other = (LongValue) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LongValue[ id=" + getId() + " ]";
    }
}
