/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Table(name = "combining_stream" //uniqueConstraints =
//@UniqueConstraint(columnNames = {"customer", "instrument", "type"})
)
@NamedQueries({
    @NamedQuery(name = "CombiningStream.findAll", query = "SELECT s FROM Stream s")
})
public class CombiningStream implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    protected Integer id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "stream_mapping",
    joinColumns = {
        @JoinColumn(name = "stream_id", referencedColumnName = "id")},
    inverseJoinColumns = {
        @JoinColumn(name = "operator_id", referencedColumnName = "id")})
    @OrderColumn
    private List<StreamOperator> operators;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "variable_combination_mapping",
    joinColumns = {
        @JoinColumn(name = "stream_id", referencedColumnName = "id")},
    inverseJoinColumns = {
        @JoinColumn(name = "variable_id", referencedColumnName = "id")})
    @OrderColumn
    private List<Variable> variables;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_settings_id", referencedColumnName = "id")
    private UserSettings userSettings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<StreamOperator> getOperators() {
        return operators;
    }

    public void setOperators(List<StreamOperator> operators) {
        this.operators = operators;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CombiningStream other = (CombiningStream) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CombiningStream{" + "id=" + id + '}';
    }
}
