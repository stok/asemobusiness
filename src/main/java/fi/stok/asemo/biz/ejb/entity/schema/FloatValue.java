/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Table(name = "float_value",
uniqueConstraints =
@UniqueConstraint(columnNames = {"value_group_id", "time_measured"}))
@NamedQueries({
    @NamedQuery(name = "FloatValue.findAll", query = "SELECT m FROM FloatValue m"),
    @NamedQuery(name = "FloatValue.findByTimeMeasured", query = "SELECT m FROM FloatValue m WHERE m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "FloatValue.findByValueGroupAndTimeMeasured", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "FloatValue.findTimeMeasuredRangeByData", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from AND m.timeMeasured <= :to ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "FloatValue.findFirstByData", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "FloatValue.findLastByData", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "FloatValue.findNAfterDateByValueGroup", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured > :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "FloatValue.findNBeforeDateByValueGroup", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured < :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "FloatValue.findNBeforeOrAtDateByValueGroup", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured <= :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "FloatValue.findNAfterOrAtDateByValueGroup", query = "SELECT m FROM FloatValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "FloatValue.findCSVRangeByDate", query = "SELECT v.timeMeasured, v.value FROM FloatValue v WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
})
public class FloatValue extends Value implements NumericValue, Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "value")
    private float value;

    @Override
    public Float getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = (Float) value;
    }

    @Override
    public void setValue(Number value) {
        this.value = value.floatValue();
    }

    @Override
    public void parseAndSetValue(String value) {
        this.value = Float.parseFloat(value);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FloatValue)) {
            return false;
        }
        FloatValue other = (FloatValue) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FloatValue[ id=" + getId() + " ]";
    }
}
