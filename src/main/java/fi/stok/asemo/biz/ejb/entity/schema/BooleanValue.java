/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.ejb.entity.schema;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juha Loukkola
 */
@Entity
@Cacheable(false)
@Table(name = "boolean_value",
uniqueConstraints =
@UniqueConstraint(columnNames = {"value_group_id", "time_measured"}))
@NamedQueries({
    @NamedQuery(name = "BooleanValue.findAll", query = "SELECT m FROM BooleanValue m"),
    @NamedQuery(name = "BooleanValue.findByTimeMeasured", query = "SELECT m FROM BooleanValue m WHERE m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "BooleanValue.findByValueGroupAndTimeMeasured", query = "SELECT m FROM BooleanValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured = :timeMeasured"),
    @NamedQuery(name = "BooleanValue.findTimeMeasuredRangeByData", query = "SELECT m FROM BooleanValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured >= :from AND m.timeMeasured <= :to ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "BooleanValue.findFirstByData", query = "SELECT m FROM BooleanValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured ASC"),
    @NamedQuery(name = "BooleanValue.findLastByData", query = "SELECT m FROM BooleanValue m WHERE m.valueGroup = :valueGroup ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "BooleanValue.findNBeforeDateByValueGroup", query = "SELECT m FROM BooleanValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured < :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "BooleanValue.findNBeforeOrAtDateByValueGroup", query = "SELECT m FROM BooleanValue m WHERE m.valueGroup = :valueGroup AND m.timeMeasured <= :from ORDER BY m.timeMeasured DESC"),
    @NamedQuery(name = "BooleanValue.findCSVRangeByDate", query = "SELECT v.timeMeasured, v.value FROM BooleanValue v WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
})
public class BooleanValue extends Value implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false) 
    @Column(name = "value")
    private Boolean value;

    @Override
    public Boolean getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = (Boolean)value;
    }
    
    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public void parseAndSetValue(String value) {
        this.value = Boolean.parseBoolean(value);
    }
       
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BooleanValue)) {
            return false;
        }
        BooleanValue other = (BooleanValue) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BooleanValue[ id=" + getId() + " ]";
    }

}
