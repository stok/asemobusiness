/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.BusinessBean;
import fi.stok.asemo.biz.admin.VariableDAO;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.user.StreamDAO;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class StreamDAOImpl implements StreamDAO {

    @EJB
    VariableDAO variables;
    @PersistenceContext
    private EntityManager em;

    @Override
    public Stream getStream(int id) {
        return this.em.find(Stream.class, id);
    }

    @Override
    public List<Stream> getStreams(int userId) {
        return this.em.find(User.class, userId).getUserSettings().getStreams();
    }
    
    @Override
    public List<Value> getValuesForStream(int streamId, Long logLevel, Date from, Date to) throws Exception {
        return this.variables.getValues(this.getStream(streamId).getVariable().getUuid(), this.getNearestLogLevel(streamId, logLevel), from, to);
    }

    @Override
    public List<Value> getValuesForStream(int streamId, Long logLevel, Date from, Date to, int maxReuslts) throws Exception {
        return this.variables.getValues(this.getStream(streamId).getVariable().getUuid(), this.getNearestLogLevel(streamId, logLevel), from, to, maxReuslts);
    }

    @Override
    public Long getNearestLogLevel(int streamId, Long normalizationPeriod) {
        Stream stream = this.getStream(streamId);

        Long previous = null;
        Long current;
        Long delta = null;

        for (ValueGroup vg : stream.getVariable().getValueGroups()) {
            //current = sp.getValue();
            if (vg.isEnabled()) {
                Long samplingPeriod = vg.getSamplingPeriod();
                if (normalizationPeriod <= samplingPeriod) { // iterated over all interesting log levels --> method will return in all cases
                    if (delta != null) {
                        Long tmp_delta = Math.abs(normalizationPeriod - samplingPeriod);
                        if (tmp_delta > delta) { // the previous log level is the nearest one
                            return previous;
                        } else {
                            return samplingPeriod;
                        }
                    } else { // the requested sampling period is shorter than the shortest log level --< return the first log level
                        return samplingPeriod;
                    }
                } else {
                    delta = Math.abs(normalizationPeriod - samplingPeriod);
                    previous = samplingPeriod;
                }
            }
        }
        return previous;
    }
    
}
