/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.admin.CustomerDAO;
import fi.stok.asemo.biz.admin.UserGroupDAO;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserSettings;
import fi.stok.asemo.biz.exception.EmailNotConfiguredException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class UserDAOImpl implements fi.stok.asemo.biz.user.UserDAO, fi.stok.asemo.biz.admin.UserDAO {

    @PersistenceContext
    private EntityManager em;
    @EJB
    CustomerDAO customers;
    @EJB
    UserGroupDAO userGroups;
    @Resource(name = "mail/AsemoMailSession")
    private Session mailSession;

    @Override
    public User getUser(int id) {
        return this.em.find(User.class, id);
    }

    @Override
    public User getUser(String username) {
        try {
            return this.em.createNamedQuery("User.findByUsername", User.class)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public void setUserName(int id, String userName) {
        try {
            this.em.find(User.class, id).setUsername(userName);
        } catch (Exception ex) {
            // User name not unique
            // User not found
        }
    }

    @Override
    public void setPassword(int id, String password) {
        try {
            this.em.find(User.class, id).storePassword(password);
        } catch (Exception ex) {
            // PasswordException
            // User not found
        }
    }

    @Override
    public boolean isUsernameUnique(String uname) {
        if (em.createNamedQuery("User.findByUsername", User.class)
                .setParameter("username", uname)
                .getResultList()
                .isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void requestNewPassword(String uname) throws UserNotFoundException, EmailNotConfiguredException, PasswordException {
        User user = this.getUser(uname);//em.createNamedQuery("User.findByUsername", User.class).setParameter("username", uname).getSingleResult();
        try {
            String email = user.getCustomer().getEmail();
            String subject = "asemo.fi - uusi salasanasi";
            String content = "Hei,\n\n"
                    + "Tässä uusi salasanasi: " + user.generateAndStorePassword()
                    + "\nVaihda salasana menemällä asemo.fi-palvelun Käyttäjätiedot-sivulle ja valitsemalla \"vaihda salasana\""
                    + "\n\nYstävällisin terveisin"
                    + "\nasemo.fi:n ylläpito";
            try {
                this.sendMail(email, subject, content);
            } catch (NullPointerException ex) {
                throw new EmailNotConfiguredException();
            }
        } catch (NullPointerException ex) {
            throw new UserNotFoundException();
        }

    }

    @Override
    public User createUser(String username, String password, int customerId, Collection<String> groups) throws PasswordException {
        Customer customer = this.customers.getCustomer(customerId);
        User user = new User(username, password, customer);
        this.em.persist(user);
        customer.setUser(user);
        for (String group : groups) {
            this.addUserToGroup(user.getId(), group);
        }
        return user;
    }

    @Override
    public User createUser(String username, String password, Collection<String> groups) throws PasswordException {
        User user = new User(username, password);
        this.em.persist(user);
        for (String group : groups) {
            this.addUserToGroup(user.getId(), group);
        }
        return user;
    }

    @Override
    public void addUserToGroup(int userId, String groupName) {
        this.getUser(userId).getGroups().add(this.userGroups.getUserGroup(groupName));
    }

    @Override
    public void removeUserFromGoup(int userId, String groupName) {
        this.getUser(userId).getGroups().remove(this.userGroups.getUserGroup(groupName));
    }

    private void createUser(User user) {
        // Make the new user managed
        this.em.persist(user);

        // Bind customer
        if (user.getCustomer() != null) {
            if (user.getCustomer().getId() != null) {
                // Make customer managed
                user.setCustomer(this.customers.getCustomer(user.getCustomer().getId()));
            }
            user.getCustomer().setUser(user);
        }

        // Create user settings object
        if (user.getUserSettings() == null) {
            user.setUserSettings(new UserSettings());
            user.getUserSettings().setUser(user);
            user.getUserSettings().setStreams(new ArrayList<Stream>());
        }
    }

    private void updateUser(User user) {
        User currentUser = this.getUser(user.getId());

        // Manage changes in customer         
        // If the customer has changed ...
        if (!(currentUser.getCustomer() == user.getCustomer() || (currentUser.getCustomer() != null && user.getCustomer() != null && currentUser.getCustomer().equals(user.getCustomer())))) {
            if (currentUser.getCustomer() != null) {
                // Remove binding to previous customer
                currentUser.getCustomer().setUser(null);
            }
            if (user.getCustomer() != null) {
                // Add bind to a new customer
                if (user.getCustomer().getId() != null) {
                    // Make customer managed
                    user.setCustomer(this.customers.getCustomer(user.getCustomer().getId()));
                }
                user.getCustomer().setUser(user);
            }
        }
        this.em.merge(user);
    }

    @Override
    public void createOrUpdateUser(User user) {
        if (user.getId() == null) {
            this.createUser(user);
        } else {
            this.updateUser(user);
        }
    }

    @Override
    public void deleteUser(int id) {
        this.em.remove(this.em.find(User.class, id));
    }

    @Override
    public Collection<User> getUsers() {
        return this.em.createNamedQuery("User.findAll", User.class).getResultList();
    }

    // SEE: http://spitballer.blogspot.com/2010/02/sending-email-via-glassfish-v3.html
    private void sendMail(String to, String subject, String messageBody) {
        // Create the message object
        Message message = new MimeMessage(mailSession);

        try {
            // Adjust the recipients. Here we have only one
            // recipient. The recipient's address must be
            // an object of the InternetAddress class.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to, false));

            // Set the message's subject
            message.setSubject(subject);

            // Insert the message's body
            message.setText(messageBody);

            // This is not mandatory, however, it is a good
            // practice to indicate the software which
            // constructed the message.
            message.setHeader("X-Mailer", "Asemo");

            // Adjust the date of sending the message
            Date timeStamp = new Date();
            message.setSentDate(timeStamp);

            // Use the 'send' static method of the Transport
            // class to send the message
            Transport.send(message);
        } catch (Exception e) {
            // TODO: throw sendMailException
        }
    }
}
