/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.LogLevels;
import fi.stok.asemo.biz.admin.InstrumentDAO;
import fi.stok.asemo.biz.admin.VariableDAO;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import fi.stok.asemo.biz.exception.UuidNotUniqueException;
import fi.stok.asemo.biz.ejb.instruments.Preprocessor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class InstrumentDAOImpl implements InstrumentDAO {

    private static final Logger LOG = Logger.getLogger(InstrumentDAOImpl.class.getName());
    @Resource
    private EJBContext context;
    @PersistenceContext
    private EntityManager em;
    @EJB
    VariableDAO variableDAO;

    @Override
    public void createOrUpdateInstrument(Instrument instrument) throws UuidNotUniqueException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteInstrument(int id) {
        this.em.remove(this.getInstrument(id));
    }

    @Override
    public Instrument getInstrument(int id) {
        return this.em.find(Instrument.class, id);
    }

    @Override
    public Instrument getInstrument(String uuid) {
        return em.createNamedQuery("Instrument.findByUuid", Instrument.class).setParameter("uuid", uuid).getSingleResult();
    }

    @Override
    public List<Instrument> getInstruments() {
        return em.createNamedQuery("Instrument.findAll", Instrument.class).getResultList();
    }

    @Override
    public List<Instrument> getNonAttachedInstruments() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isInstrumentUuidUnique(String uuid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String addValues(String uuid, List<List<String>> values, boolean rollBackOnError) throws Exception {
        String response = "";
        Instrument instrument = this.getInstrument(uuid);

        // Get elements of the data schema
        ArrayList<VariableType> schemaElements = new ArrayList(instrument.getDataSource().getType().getDataSchema().getSchemaElementCollection());

        // Instantiate preprocessor if available
        Preprocessor preprocessor = this.getPreprocessor(instrument);

        try {
            // Load data type classes and variables into lookup lists
            List<Class> dataTypeClasses = new ArrayList<Class>();
            List<Variable> variables = new ArrayList<Variable>();
            for (VariableType se : schemaElements) {
                dataTypeClasses.add(Class.forName(se.getDataType()).asSubclass(fi.stok.asemo.biz.ejb.entity.schema.Value.class));
                variables.add(instrument.getDataSource().getVariable(se));
            }
            // Process lines
            for (int j = 0; j < values.size(); j++) {
                try {
                    List<String> line = values.get(j);
                    // Preprocess a line
                    if (preprocessor != null) {
                        // We are preprocessign only one line at a time but prepocess() takes list of lines
                        // so we give it only a list of length one.
                        List<List<String>> lines = new ArrayList<List<String>>();
                        lines.add(line);
                        line = preprocessor.preprocess(lines, instrument).get(0);
                    }

                    // There must be on value per schema element plus timestamp value
                    if (line.size() == schemaElements.size() + 1) {
                        String type = instrument.getDataSource().getType().getName();

                        // The first column represents a timestamp
                        Long timestamp = Long.parseLong(line.get(0)) * 1000l;

                        /*
                         * The rest of the columns represent values. Create a new
                         * Value object for every value/column.
                         */
                        for (int i = 0; i < schemaElements.size(); i++) {
                            VariableType schemaElement = schemaElements.get(i);
                            String token = line.get(i + 1);
                            try {
                                // Resolve data type of the value
                                Class dataTypeClass = dataTypeClasses.get(i);
                                // Construct and persist a new Value object
                                Value value = null; // = (Value) dataTypeClass.newInstance();
                                Variable variable = variables.get(i);
                                value = (Value) dataTypeClass.newInstance();
                                ValueGroup valueGroup = variable.getValueGroup(LogLevels.RAW);
                                value.setValueGroup(valueGroup);
                                value.setTimeStored(new Date().getTime());
                                value.setTimeMeasured(timestamp);
                                value.parseAndSetValue(token);
                                // Check uniqueness of value and persist.
//                                if (variableDAO.getValue(variable.getUuid(), LogLevels.RAW, timestamp) == null) {
                                this.em.persist(value);
                                this.em.flush();
//                                } else {
//                                    // TODO: Handle a dublicate!
//                                    response += "Error while storing a value at index " + i + " at line " + j
//                                        + " for instrument " + instrument.getUuid() + ". Dublicate value.\n";
//                                }

                            } catch (Exception ex) {
                                LOG.log(Level.WARNING, "Error while storing a value at index {2} at line {3} for instrument {0}. Exception: {1}",
                                        new Object[]{instrument.getUuid(), ex.getMessage(), i, j});
                                if (rollBackOnError) {
                                    this.context.setRollbackOnly();
                                    throw new Exception();
                                }
                                response += "Error while storing a value at index " + i + " at line " + j
                                        + " for instrument " + instrument.getUuid() + ". Exception " + ex.getMessage() + ".\n";
                            }
                        }
                    } else {
                        LOG.log(Level.INFO, "Error while storing values for an instrument {0}: wrong number of variables at line {1}",
                                new Object[]{instrument.getUuid(), j});
                        if (rollBackOnError) {
                            this.context.setRollbackOnly();
                            throw new Exception();
                        }
                        response += "Error while storing values for an instrument: " + instrument.getUuid()
                                + ": wrong number of variables at line " + j + ".\n";
                    }
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Error while storing a values at line {2} for instrument: {0}. Exception: {1}",
                            new Object[]{instrument.getUuid(), ex.getMessage(), j});
                    if (rollBackOnError) {
                        this.context.setRollbackOnly();
                        throw new Exception();
                    }
                    response += "Error while storing a values at line " + j + " for instrument: "
                            + instrument.getUuid() + ". Exception: " + ex.getMessage() + ".\n";
                }
            }
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Error while storing values for an instrument {0}. Exception: {1}", new Object[]{instrument.getUuid(), e.getMessage()});
            if (rollBackOnError) {
                this.context.setRollbackOnly();
                throw new Exception();
            }
            response += "Error while storing values for an instrument " + instrument.getUuid() + ". Exception: " + e.getMessage() + ".\n";
        }
        if (response.isEmpty()) {
            response = "OK";
        }
        return response;

    }

    private Preprocessor getPreprocessor(Instrument instrument) throws Exception {
        if (instrument.getDataSource().getType().getPreprocessor() != null) {
            try {
                Class preprocessorClass = Class.forName(instrument.getDataSource().getType().getPreprocessor()).asSubclass(fi.stok.asemo.biz.ejb.instruments.Preprocessor.class);
                return (Preprocessor) preprocessorClass.newInstance();
            } catch (Exception ex) {
                LOG.log(Level.SEVERE,
                        "Exception instantiating preprocessor for instrument {0}. Exception: {1}",
                        new Object[]{instrument.getUuid(), ex.getMessage()});
                throw new Exception();
            }
        } else {
            return null;
        }
    }
}
