package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.admin.UserGroupDAO;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class UserGroupDAOImpl implements UserGroupDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void createUserGroup(String name) {
        this.em.persist(new UserGroup(name));
    }

    @Override
    public UserGroup getUserGroup(String name) {
        try {
            return em.createNamedQuery("UserGroup.findByUserGroupName", UserGroup.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public Collection<UserGroup> getUserGroups() {
        return em.createNamedQuery("UserGroup.findAll", UserGroup.class)
                .getResultList();
    }
}
