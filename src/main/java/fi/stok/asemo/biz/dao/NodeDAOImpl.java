/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.admin.CustomerDAO;
import fi.stok.asemo.biz.admin.NodeDAO;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class NodeDAOImpl implements NodeDAO {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private CustomerDAO customers;

    private void createNode(Node node) {
        // Make the new node managed
        this.em.persist(node);
        if (node.getCustomer() != null && node.getCustomer().getId() != null) {
            // Make customer managed
            node.setCustomer(this.customers.getCustomer(node.getCustomer().getId()));
            // Bind customer
            node.getCustomer().getNodes().add(node);
        }
    }

    private void updateNode(Node node) {
        // Get current and managed instanse of the node
        Node currentNodeSate = this.getNode(node.getId());

        // If the owner(customer) has changed
        if (!(currentNodeSate.getCustomer() == node.getCustomer()
                || (currentNodeSate.getCustomer() != null
                && node.getCustomer() != null
                && currentNodeSate.getCustomer().equals(node.getCustomer())))) {

            // Detach node from the previous owner(customer)
            if (currentNodeSate.getCustomer() != null) {
                currentNodeSate.getCustomer().getNodes().remove(currentNodeSate);
            }

            // Attach node to a new customer
            if (node.getCustomer() != null) {
                if (node.getCustomer().getId() != null) {
                    // Make customer managed
                    node.setCustomer(this.customers.getCustomer(node.getCustomer().getId()));
                }
                // Bind customer
                node.getCustomer().getNodes().add(node);
            }
        }
        // Merge new state to current state
        this.em.merge(node);
    }

    @Override
    public void createOrUpdateNode(Node node) {
        if (node.getId() == null) {
            this.createNode(node);
        } else {
            this.updateNode(node);
        }
    }

    @Override
    public void deleteNode(int id) {
        this.em.remove(this.getNode(id));
    }

    @Override
    public Node getNode(int id) {
        return this.em.find(Node.class, id);
    }

    @Override
    public Node getNode(String uuid) {
        return em.createNamedQuery("Node.findByUuid", Node.class).setParameter("uuid", uuid).getSingleResult();
    }

    @Override
    public Collection<Node> getNodes() {
        return em.createNamedQuery("Node.findAll", Node.class).getResultList();
    }

    @Override
    public Collection<Node> getNonAttachedNodes() {
        return em.createNamedQuery("Node.findByCustomerIsNull", Node.class).getResultList();
    }

    @Override
    public boolean isNodeUuidUnique(String uuid) {
        return em.createNamedQuery("Node.findByUuid", Node.class).setParameter("uuid", uuid).getResultList().isEmpty();
    }
    
}
