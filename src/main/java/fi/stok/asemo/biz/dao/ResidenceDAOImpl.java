/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.ejb.entity.schema.Residence;
import fi.stok.asemo.biz.user.ResidenceDAO;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class ResidenceDAOImpl implements ResidenceDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void createOrUpdateResidence(Residence residence) {
        this.em.merge(residence);
    }
}
