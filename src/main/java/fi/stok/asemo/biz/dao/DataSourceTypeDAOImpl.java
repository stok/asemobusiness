/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.admin.DataSchemaDAO;
import fi.stok.asemo.biz.admin.DataSourceTypeDAO;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import java.util.Collection;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class DataSourceTypeDAOImpl implements DataSourceTypeDAO {

    @EJB
    private DataSchemaDAO dataSchemaDAO;
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void importDataSourceType(DataSourceType type) {
        if (this.dataSchemaDAO.isDataSchemaUuidUnique(type.getDataSchema().getUuid())) {
            this.em.persist(type.getDataSchema());       
        }
        this.em.persist(type);
    }
    
    @Override
    public DataSourceType createDataSourceType(DataSourceType type) {
        // Create schema if needed
        if (type.getDataSchema().getUuid() == null || this.dataSchemaDAO.isDataSchemaUuidUnique(type.getDataSchema().getUuid())) {
            this.dataSchemaDAO.createDataSchema(type.getDataSchema());
        }
        // Generate uuid if empty
        if (type.getUuid() == null || type.getUuid().isEmpty()) {
            type.setUuid(UUID.randomUUID().toString());
        }
        this.em.persist(type);
        return type;
    }
    
    @Override
    public void createOrUpdateDataSourceType(DataSourceType type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteDataSourceType(String uuid) {
        this.em.remove(this.getDataSourceType(uuid));
    }

    @Override
    public DataSourceType getDataSourceType(int id) {
        return this.em.find(DataSourceType.class, id);
    }

    @Override
    public DataSourceType getDataSourceType(String uuid) {
        try {
            return this.em.createNamedQuery("DataSourceType.findByUuid", DataSourceType.class)
                    .setParameter("uuid", uuid).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public Collection<DataSourceType> getDataSourceTypes() {
        return this.em.createNamedQuery("DataSourceType.findAll", DataSourceType.class).getResultList();
    }

    @Override
    public boolean isDataSourceTypeUuidUnique(String uuid) {
        return this.getDataSourceType(uuid) == null;
    }
}
