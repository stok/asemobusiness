/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.BusinessBean;
import fi.stok.asemo.biz.LogLevels;
import fi.stok.asemo.biz.admin.CustomerDAO;
import fi.stok.asemo.biz.admin.DataSourceDAO;
import fi.stok.asemo.biz.admin.VariableDAO;
import fi.stok.asemo.biz.admin.VariableTypeDAO;
import fi.stok.asemo.biz.ejb.entity.schema.DataSource;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.eclipse.persistence.queries.CursoredStream;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class VariableDAOImpl implements VariableDAO {

    @EJB
    CustomerDAO customers;
    @EJB
    DataSourceDAO dataSources;
    @EJB
    VariableTypeDAO variableTypes;
    @PersistenceContext
    private EntityManager em;

    public void createVariable(String uuid, String customerUuid, String dataSourceUuid, String typeUuid) {
        Variable variable = new Variable();
        variable.setUuid(uuid);
        em.persist(variable);
        // Set customer
        variable.setCustomer(this.customers.getCustomer(customerUuid));
        // Set data source
        DataSource dataSource = this.dataSources.getDataSource(dataSourceUuid);
        variable.setDataSource(dataSource);
        dataSource.getVariables().add(variable);
        // Set type
        variable.setType(variableTypes.get(uuid));
    }

    @Override
    public void addVariable(Variable variable) {
        // Setup variable

        // Setup value groups for a variable
        List<ValueGroup> valueGroups = new ArrayList<ValueGroup>();
        for (LogLevels.LogLevel level : LogLevels.levels) {
//            if (level.isEnabled()) {
            ValueGroup valueGroup = new ValueGroup();
            em.persist(valueGroup);
            valueGroup.setSamplingPeriod(level.getSamplingPeriod());
            valueGroup.setRotationPeriod(level.getRotationPeriod());
            valueGroup.setEnabled(level.isEnabled());
            valueGroup.setVariable(variable);
            valueGroups.add(valueGroup);
//            }
        }
        variable.setValueGroups(valueGroups);
    }

    @Override
    public void deleteVariable(String uuid) {
        this.em.remove(this.getVariable(uuid));
    }

    @Override
    public Variable getVariable(String uuid) {
        return em.createNamedQuery("Variable.findByUuid", Variable.class).setParameter("uuid", uuid).getSingleResult();
    }

    @Override
    public void updateVariable(Variable variable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Value getValue(String uuid, Long logLevel, Long timestamp) throws ClassNotFoundException {
        Variable var = this.getVariable(uuid);
        // Resolve data type for the log
        Class dataTypeClass = Class.forName(var.getType().getDataType()).asSubclass(Value.class);
        try {
            // Get the log
            return (Value) em.createNamedQuery(dataTypeClass.getSimpleName() + ".findByValueGroupAndTimeMeasured", dataTypeClass)
                    .setParameter("valueGroup", var.getValueGroup(logLevel))
                    .setParameter("timeMeasured", timestamp)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (Exception ex) {
            Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Value> getValues(String uuid, Long logLevel, Date from, Date to) throws Exception {
        return this.getValues(uuid, logLevel, from, to, -1);
    }

    @Override
    public List<Value> getValues(String uuid, Long logLevel, Date from, Date to, int maxReuslts) throws Exception {
        Variable var = this.getVariable(uuid);
        // Resolve data type for the log
        Class dataTypeClass = Class.forName(var.getType().getDataType()).asSubclass(Value.class);
        try {
            // Get the log
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass)
                    .setParameter("valueGroup", var.getValueGroup(logLevel))
                    .setParameter("from", from.getTime())
                    .setParameter("to", to.getTime());
            if (maxReuslts > 0) {
                query.setMaxResults(maxReuslts);
            }
            return query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(); // TODO: Define more spesific exception.
        }
    }

    @Override
    public CursoredStream getValueStream(String variableUuid, Long logLevel, Date from, Date to) throws Exception {
        Variable var = this.getVariable(variableUuid);
        // Resolve data type for the log
        Class dataTypeClass = Class.forName(var.getType().getDataType()).asSubclass(Value.class);
        try {
            return (CursoredStream) this.em.createQuery("SELECT v.timeMeasured, v.value FROM " + dataTypeClass.getSimpleName() + " V WHERE v.valueGroup = :valueGroup AND v.timeMeasured >= :from AND v.timeMeasured <= :to ORDER BY v.timeMeasured ASC")
                    .setParameter("valueGroup", var.getValueGroup(logLevel))
                    .setParameter("from", from.getTime())
                    .setParameter("to", to.getTime())
                    .setHint("eclipselink.cursor", true)
                    .setHint("eclipselink.read-only", true)
                    .setHint("eclipselink.maintain-cache", false)
                    .setHint("eclipselink.cursor.initial-size", 1000)
                    .setHint("eclipselink.cursor.page-size", 1000)
                    .getSingleResult();
        } catch (Exception ex) {
            Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(); // TODO: Define more spesific exception.
        }
    }

    @Override
    public List<Object[]> getPartialValues(String uuid, Long logLevel, Date from, Date to, int maxReuslts) throws Exception {
        Variable var = this.getVariable(uuid);
        // Resolve data type for the log
        Class dataTypeClass = Class.forName(var.getType().getDataType()).asSubclass(Value.class);
        try {
            // Get the log
            TypedQuery<Object[]> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findCSVRangeByDate", dataTypeClass)
                    .setParameter("valueGroup", var.getValueGroup(logLevel))
                    .setParameter("from", from.getTime())
                    .setParameter("to", to.getTime());
            if (maxReuslts > 0) {
                query.setMaxResults(maxReuslts);
            }
            return query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(); // TODO: Define more spesific exception.
        }
    }
}
