/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.admin.DataSourceTypeDAO;
import fi.stok.asemo.biz.admin.VariableDAO;
import fi.stok.asemo.biz.admin.VariableTypeDAO;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.biz.ejb.entity.schema.DataSource;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import java.util.Collection;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class CustomerDAOImpl implements fi.stok.asemo.biz.user.CustomerDAO, fi.stok.asemo.biz.admin.CustomerDAO {

    @EJB
    private Business biz;
    @EJB
    private DataSourceTypeDAO dataSourceTypeDAO;
    @EJB
    private VariableTypeDAO variableTypeDAO;
    @EJB
    private VariableDAO variableDAO;
    @PersistenceContext
    private EntityManager em;

    @Override
    public Customer getCustomer(int id) {
        return this.em.find(Customer.class, id);
    }

    @Override
    public Customer getCustomer(String uuid) {
        try {
            return this.em.createNamedQuery("Customer.findByUuid", Customer.class)
                    .setParameter("uuid", uuid)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public void importCustomer(Customer customer) {
        /* Validation: id fields of objects must not be set to prevent security holes
         * where bindings between existing objects could be modified.
         * UUIDs must be unique. Enforced by DB
         * Username must be unique. Enforced by DB
         */

        // TODO: Update UserGroups???
        // Set references Variable --> VariableType
        for (Variable var : customer.getVariables()) {
            VariableType type = this.variableTypeDAO.get(var.getType().getUuid());
            var.setType(type);
        }

        // Set reference DataSource --> DataSourceType
        for (Node node : customer.getNodes()) {
            for (Instrument instrument : node.getInstruments()) {
                DataSource ds = instrument.getDataSource();
                DataSourceType dst = this.dataSourceTypeDAO.getDataSourceType(ds.getType().getUuid());
                ds.setType(dst);
            }
        }

        // Persist Customer
        this.em.persist(customer);
        // Persist User
        this.em.persist(customer.getUser());
        // Persist Nodes & Instruments
        for (Node node : customer.getNodes()) {
            em.persist(node);
            for (Instrument instrument : node.getInstruments()) {
                this.em.persist(instrument.getDataSource());
                this.em.persist(instrument);
            }
        }
        // Persist Variables
        for (Variable var : customer.getVariables()) {
            this.em.persist(var);
        }

    }

    @Override
    public Customer createCustomer(String name, String streetArddess, String zipcode, String city, String email, String phone) {
        Customer customer = new Customer(name, streetArddess, zipcode, city, email, phone);
        customer.setUuid(UUID.randomUUID().toString());
        this.em.persist(customer);
        return customer;
    }

    @Override
    public void updateCustomer(int id, String name, String streetArddess, String zipcode, String city, String email, String phone) {
        this.setName(id, name);
        this.setAddress(id, streetArddess, zipcode, city);
        this.setEmail(id, email);
        this.setPhone(id, phone);
    }

    @Override
    public void deleteCustomer(int id) {
        Customer customer = this.em.find(Customer.class, id);
        this.em.remove(customer);
    }

    @Override
    public Collection<Customer> getCustomers() {
        return this.em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
    }

    @Override
    public void setAddress(int id, String streetArddess, String zipcode, String city) {
        Customer customer = this.getCustomer(id);
        customer.setStreetAddress(streetArddess);
        customer.setZipCode(zipcode);
        customer.setCity(city);
    }

    @Override
    public void setEmail(int id, String email) {
        Customer customer = this.getCustomer(id);
        customer.setEmail(email);
    }

    @Override
    public void setPhone(int id, String phone) {
        Customer customer = this.getCustomer(id);
        customer.setPhone(phone);
    }

    @Override
    public void setName(int id, String name) {
        Customer customer = this.getCustomer(id);
        customer.setName(name);
    }
}
