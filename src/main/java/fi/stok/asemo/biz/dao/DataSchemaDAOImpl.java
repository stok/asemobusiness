/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.LogLevels;
import fi.stok.asemo.biz.admin.DataSchemaDAO;
import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.ListDataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroupSettings;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class DataSchemaDAOImpl implements DataSchemaDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public DataSchema createDataSchema(DataSchema schema) {
        // Persist schema. VariableTypes and ValueGroupSettings will also be persisted via cascade rules.
        this.em.persist(schema);
        return schema;
    }

    @Override
    public DataSchema createListDataSchema(String name, String description, List<VariableType> elements) {
        ListDataSchema schema = new ListDataSchema(name, description);
        // Bind elements to the schema      
        for (VariableType element : elements) {
            element.setDataSchema(schema);
            // Initialize value group settings
            element.setValueGroupSettings(new ArrayList<ValueGroupSettings>());
            for (LogLevels.LogLevel logLevel : LogLevels.levels) {
                ValueGroupSettings vgs = new ValueGroupSettings();
                vgs.setSamplingPeriod(logLevel.getSamplingPeriod());
                vgs.setRotationPeriod(logLevel.getRotationPeriod());
                vgs.setEnabled(logLevel.isEnabled());
                vgs.setVariableType(element);
                element.getValueGroupSettings().add(vgs);
            }
        }
        schema.setElements(elements);

        // Generate uuid if empty
        if (schema.getUuid() == null || schema.getUuid().isEmpty()) {
            schema.setUuid(UUID.randomUUID().toString());
        }
        
        // Persist schema. Elements will also be persisted via cascade rules
        this.em.persist(schema);
        return schema;
    }

    @Override
    public void createOrUpdateDataSchema(DataSchema schema) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteDataSchema(String uuid) {
        this.em.remove(this.getDataSchema(uuid));
    }

    @Override
    public DataSchema getDataSchema(int id) {
        return this.em.find(DataSchema.class, id);
    }

    @Override
    public DataSchema getDataSchema(String uuid) {
        try {
            return this.em.createNamedQuery("DataSchema.findByUuid", DataSchema.class)
                    .setParameter("uuid", uuid).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public Collection<DataSchema> getDataSchemas() {
        return em.createNamedQuery("DataSchema.findAll").getResultList();
    }

    @Override
    public boolean isDataSchemaUuidUnique(String uuid) {
        return this.getDataSchema(uuid) == null;
    }
}
