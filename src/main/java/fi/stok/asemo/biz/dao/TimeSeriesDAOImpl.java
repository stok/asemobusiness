/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.BusinessBean;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.user.StreamDAO;
import fi.stok.asemo.biz.user.TimeSeriesDAO;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class TimeSeriesDAOImpl implements TimeSeriesDAO {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private StreamDAO streams;

    @Override
    public List<Value> getValuesForStream(int streamId, Long logLevel, Date from, Date to) throws Exception {
        return this.getValuesForStream(streamId, logLevel, from, to, -1);
    }

    @Override
    public List<Value> getValuesForStream(int streamId, Long logLevel, Date from, Date to, int maxReuslts) throws Exception {
        Stream stream = this.streams.getStream(streamId);
        // Resolve data type for the log
        Class dataTypeClass = Class.forName(stream.getVariable().getType().getDataType()).asSubclass(Value.class);
        try {
            // Get the log
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass)
                    .setParameter("valueGroup", stream.getVariable().getValueGroup(this.getNearestLogLevel(streamId, logLevel)))
                    .setParameter("from", from).setParameter("to", to);
            if (maxReuslts > 0) {
                query.setMaxResults(maxReuslts);
            }
            return query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(); // TODO: Define more spesific exception.
        }
    }

    @Override
    public Long getNearestLogLevel(int streamId, Long normalizationPeriod) {
        Stream stream = this.streams.getStream(streamId);

        Long previous = null;
        Long current;
        Long delta = null;

        for (ValueGroup vg : stream.getVariable().getValueGroups()) {
            //current = sp.getValue();
            if (vg.isEnabled()) {
                Long samplingPeriod = vg.getSamplingPeriod();
                if (normalizationPeriod <= samplingPeriod) { // iterated over all interesting log levels --> method will return in all cases
                    if (delta != null) {
                        Long tmp_delta = Math.abs(normalizationPeriod - samplingPeriod);
                        if (tmp_delta > delta) { // the previous log level is the nearest one
                            return previous;
                        } else {
                            return samplingPeriod;
                        }
                    } else { // the requested sampling period is shorter than the shortest log level --< return the first log level
                        return samplingPeriod;
                    }
                } else {
                    delta = Math.abs(normalizationPeriod - samplingPeriod);
                    previous = samplingPeriod;
                }
            }
        }
        return previous;
    }
}
