/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.admin.VariableTypeDAO;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class VariableTypeDAOImpl implements VariableTypeDAO{

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public VariableType get(int id) {
        return this.em.find(VariableType.class, id);
    }

    @Override
    public VariableType get(String uuid) {
        try {
            return this.em.createNamedQuery("VariableType.findByUuid", VariableType.class)
                    .setParameter("uuid", uuid)
                    .getSingleResult();
        }catch (NoResultException ex) {
            return null;
        }
    }
    
}
