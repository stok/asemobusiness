/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.dao;

import fi.stok.asemo.biz.admin.DataSourceDAO;
import fi.stok.asemo.biz.ejb.entity.schema.DataSource;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
public class DataSourceDAOImpl implements DataSourceDAO{
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public void addDataSource(DataSource dataSource) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createDataSource() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteDataSource(String uuid) {
        this.em.remove(this.getDataSource(uuid));
    }

    @Override
    public void updateDataSource() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DataSource getDataSource(String uuid) {
        return em.createNamedQuery("DataSource.findByUuid", DataSource.class).setParameter("uuid", uuid).getSingleResult();
    }
    
}
