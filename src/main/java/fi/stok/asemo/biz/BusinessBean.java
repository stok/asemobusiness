/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.LogLevels.LogLevel;
import fi.stok.asemo.biz.admin.CustomerDAO;
import fi.stok.asemo.biz.admin.DataSourceTypeDAO;
import fi.stok.asemo.biz.admin.UserDAO;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.DataSource;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.ListDataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.biz.ejb.entity.schema.Residence;
import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import fi.stok.asemo.biz.ejb.entity.schema.UserSettings;
import fi.stok.asemo.biz.ejb.entity.schema.Uuid;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroup;
import fi.stok.asemo.biz.ejb.entity.schema.ValueGroupSettings;
import fi.stok.asemo.biz.ejb.entity.schema.Variable;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import fi.stok.asemo.biz.exception.EmailNotConfiguredException;
import fi.stok.asemo.biz.exception.IncorrectPasswordException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import fi.stok.asemo.biz.exception.UuidNotUniqueException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.NoSuchEntityException;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.*;

/**
 * @author Juha Loukkola
 */
@Stateless
public class BusinessBean implements Business {

    @EJB
    private DataSourceTypeDAO dataSourceTypeDAO;
    @EJB
    private CustomerDAO customerDAO;
    @EJB
    private UserDAO userDAO;
    @PersistenceContext
    private EntityManager em;
    @Resource(name = "mail/AsemoMailSession")
    private Session mailSession;

    public BusinessBean() {
    }

    @Override
    public void addCustomer(Customer customer) throws PasswordException, UuidNotUniqueException {
        DataSourceType curentCost = this.dataSourceTypeDAO.getDataSourceType("abc");

        Customer importedCustomer = this.customerDAO.createCustomer(customer.getName(), customer.getStreetAddress(), customer.getCity(), customer.getZipCode(), customer.getEmail(), customer.getPhone());

        String userGroups[] = {"USER"};
        User user = this.userDAO.createUser(customer.getUser().getUsername(), "salasana", Arrays.asList(userGroups));
        user.setCustomer(importedCustomer);
        importedCustomer.setUser(user);
        user.setPasswordHash(customer.getUser().getPasswordHash());

        for (Node n : customer.getNodes()) {
            Node node = new Node();
            node.setUuid(n.getUuid());
            node.setSharedSecret(n.getSharedSecret());
            node.setCustomer(importedCustomer);
            importedCustomer.getNodes().add(node);
            node.setEnabled(true);
            this.createNode(node);

            for (Instrument i : node.getInstruments()) {
                Instrument instrument = this.createInstrument(i.getUuid(), curentCost.getId());
                this.attachInstrumentToNode(instrument, node);
            }
        }
        importedCustomer.setResidence(customer.getResidence());

        // TODO: Validate customer information.
        // TODO: Create necesary db tables.
        // TODO: Populate db tables.
        this.saveCustomer(customer);
    }

    @Override
    public void createCustomer(Customer customer) {
        // Make the new customer managed
        this.em.persist(customer);

        // Generate uuid if empty
        if (customer.getUuid() == null || customer.getUuid().isEmpty()) {
            customer.setUuid(UUID.randomUUID().toString());
        }

        // Add binding to a user
        if (customer.getUser() != null) {
            // If the user alredy exists then mage it managed
            if (customer.getUser().getId() != null) {
                customer.setUser(this.getUser(customer.getUser().getId()));
            }
            customer.getUser().setCustomer(customer);
        }
    }

    /* 
     * Updates only the customer entity and the reverse relation ships
     */
    @Override
    public void updateCustomer(Customer customer) {
        if (customer.getId() != null) {
            // Get the currently managed instanse of the customer
            Customer currentCustomer = this.getCustomer(customer.getId());
            // If user has changed
            if (!(currentCustomer.getUser() == customer.getUser()
                    || currentCustomer.getUser() != null
                    && customer.getUser() != null
                    && currentCustomer.getUser().equals(customer.getUser()))) {
                // Remove binding between the previous user and the customer
                if (currentCustomer.getUser() != null) {
                    currentCustomer.getUser().setCustomer(null);
                    //this.saveUser(currentCustomer.getUser()); // Not required since currentCustomer is managed
                }

                // Add binding to a new user
                if (customer.getUser() != null) {
                    // If the new user alredy exists then mage it managed
                    if (customer.getUser().getId() != null) {
                        customer.setUser(this.getUser(customer.getUser().getId()));
                    }
                    customer.getUser().setCustomer(customer);
                }
            }
            // Merge new state to current state
            this.em.merge(customer);
        } else {
            // Create a new customer
            this.createCustomer(customer);
        }
    }

    @Override
    public void deleteCustomer(int id) {
        try {
            Customer c = em.find(Customer.class, id);
            em.remove(c);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public Customer getCustomer(int id) {
        Customer c = null;
        try {
            c = em.createNamedQuery("Customer.findById", Customer.class).setParameter("id", id).getSingleResult();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return c;
    }

    @Override
    public void saveCustomer(Customer customer) {
        try {
            em.merge(customer);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public List<Customer> getCustomers() {
        List<Customer> c = null;
        try {
            c = em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return c;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="User Related Functionality">
    @Deprecated
    @Override
    public void addUser(User user) {
        this.saveUser(user);
    }

    @Override
    public void createUser(User user) {
        // Make the new user managed
        this.em.persist(user);

        // Bind customer
        if (user.getCustomer() != null) {
            if (user.getCustomer().getId() != null) {
                // Make customer managed
                user.setCustomer(this.getCustomer(user.getCustomer().getId()));
            }
            user.getCustomer().setUser(user);
        }

        // Create user settings object
        if (user.getUserSettings() == null) {
            user.setUserSettings(new UserSettings());
            user.getUserSettings().setUser(user);
            user.getUserSettings().setStreams(new ArrayList<Stream>());
        }
    }

    @Override
    public void updateUser(User user) {
        if (user.getId() != null) {
            User currentUser = this.getUser(user.getId());

            // Manage changes in customer         
            // If the customer has changed ...
            if (!(currentUser.getCustomer() == user.getCustomer() || (currentUser.getCustomer() != null && user.getCustomer() != null && currentUser.getCustomer().equals(user.getCustomer())))) {
                if (currentUser.getCustomer() != null) {
                    // Remove binding to previous customer
                    currentUser.getCustomer().setUser(null);
                }
                if (user.getCustomer() != null) {
                    // Add bind to a new customer
                    if (user.getCustomer().getId() != null) {
                        // Make customer managed
                        user.setCustomer(this.getCustomer(user.getCustomer().getId()));
                    }
                    user.getCustomer().setUser(user);
                }
            }
            this.em.merge(user);
        } else {
            this.createUser(user);
        }
    }

    @Override
    public void deleteUser(int id) {
        try {
            User u = em.find(User.class, id);
            em.remove(u);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public User getUser(int id) {
        User u = null;
        try {
            u = em.createNamedQuery("User.findById", User.class).setParameter("id", id).getSingleResult();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return u;
    }

    @Override
    public void saveUser(User user) {
        try {
            em.merge(user);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public List<User> getUsers() {
        List<User> u = null;
        try {
            u = em.createNamedQuery("User.findAll", User.class).getResultList();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return u;
    }

    @Override
    public User authenticateUser(String uname, String passwd) throws IncorrectPasswordException, UserNotFoundException, PasswordException {
        try {
            // Search db for user with specified "uname".
            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", uname).getSingleResult();
            // Validate password
            if (user.authenticate(passwd)) {
                return user;
            } else {
                throw new IncorrectPasswordException();
            }
        } catch (NoResultException ex) {
            throw new UserNotFoundException();
        }
    }

    @Override
    public boolean isUsernameUnique(String uname) {
        // Query for a MAC
        TypedQuery<User> query = em.createNamedQuery("User.findByUsername", User.class);
        query.setParameter(
                "username", uname);
        try {
            List results = query.getResultList();
            if (results.isEmpty()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public UserGroup getUserGroup(String name) {
        TypedQuery<UserGroup> query = em.createNamedQuery("UserGroup.findByUserGroupName", UserGroup.class);
        query.setParameter("name", name);
        try {
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean userInRole(User user, String role) {
        UserGroup ug = this.getUserGroup(role);
        if (ug != null) {
            return user.getGroups().contains(ug);
        }
        return false;
    }

    @Override
    public User getUser(String username) {
        TypedQuery<User> query = em.createNamedQuery("User.findByUsername", User.class);
        query.setParameter("username", username);
        try {
            return query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public List<UserGroup> getUserGroups() {
        try {
            return em.createNamedQuery("UserGroup.findAll", UserGroup.class).getResultList();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void addUserGroup(UserGroup group) {
        try {
            this.em.persist(group);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Deprecated
    @Override
    public void addUserIntoRole(User user, UserGroup role) {
        // Check if user is already in a role if not add user into role
        if (!this.userInRole(user, role.getName())) {
            user.getGroups().add(role);
            try {
                em.merge(user);
            } catch (Exception ex) {
                throw new EJBException(ex);
            }
        }
    }

    @Override
    public void saveUserGroup(UserGroup group) {
        try {
            em.merge(group);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    // NOTE: Security can be improved by sending a link to a password changing form authenticated by one time password  
    @Override
    public void processPasswordRequest(String uname) throws UserNotFoundException, EmailNotConfiguredException {
        try {
            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", uname).getSingleResult();
            String email = user.getCustomer().getEmail();
            if (email == null) {
                throw new EmailNotConfiguredException();
            }
            String subject = "asemo.fi - uusi salasanasi";
            String content = "Hei,\n\n"
                    + "Tässä uusi salasanasi: " + user.generateAndStorePassword()
                    + "\nVaihda salasana menemällä asemo.fi-palvelun Käyttäjätiedot-sivulle ja valitsemalla \"vaihda salasana\""
                    + "\n\nYstävällisin terveisin"
                    + "\nasemo.fi:n ylläpito";
            sendMail(email, subject, content);


        } catch (NoResultException ex) {
            throw new UserNotFoundException();
        } catch (Exception ex) {
        }
    }

    @Override
    public List<User> getNonAttachedUsers() {
        List<User> u = null;
        try {
            u = em.createNamedQuery("User.findByCustomerIsNull", User.class).getResultList();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return u;
    }
//
//    @Deprecated
//    private boolean isUserAuthorizedForLog(User user, ValueContext mc) {
//        return mc.getCustomer().getUser().equals(user);
//    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Node Related Functionality">
    @Override
    public void createNode(Node node) {
        // Make sure that node has no id set priot persisting it since id should 
        // be autogenerated by persistance provider.
        node.setId(null);
        // Make the new node managed
        this.em.persist(node);
        if (node.getCustomer() != null && node.getCustomer().getId() != null) {
            // Make customer managed
            node.setCustomer(this.getCustomer(node.getCustomer().getId()));
            // Bind customer
            node.getCustomer().getNodes().add(node);
        }
    }

    /*
     * Updates node and its reverse relation to cutomer
     */
    @Override
    public void updateNode(Node node) {
        if (node.getId() != null) {
            // Get current and managed instanse of the node
            Node currentNodeSate = this.getNode(node.getId());

            // If the owner(customer) has changed
            if (!(currentNodeSate.getCustomer() == node.getCustomer()
                    || (currentNodeSate.getCustomer() != null
                    && node.getCustomer() != null
                    && currentNodeSate.getCustomer().equals(node.getCustomer())))) {

                // Detach node from the previous owner(customer)
                if (currentNodeSate.getCustomer() != null) {
                    currentNodeSate.getCustomer().getNodes().remove(currentNodeSate);
                }

                // Attach node to a new customer
                if (node.getCustomer() != null) {
                    if (node.getCustomer().getId() != null) {
                        // Make customer managed
                        node.setCustomer(this.getCustomer(node.getCustomer().getId()));
                    }
                    // Bind customer
                    node.getCustomer().getNodes().add(node);
                }
            }
            // Merge new state to current state
            this.em.merge(node);
        } else {
            this.createNode(node);
        }
    }

    @Deprecated
    @Override
    public void addNode(Node node) {
        this.saveNode(node);
    }

    @Override
    public void deleteNode(int id) {
        try {
            Node n = em.find(Node.class, id);
            em.remove(n);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public Node getNode(int id) {
        Node n = null;
        try {
            n = em.createNamedQuery("Node.findById", Node.class).setParameter("id", id).getSingleResult();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return n;
    }

    @Override
    public Node getNode(String uuid) {
        Node n = null;
        try {
            n = em.createNamedQuery("Node.findByUuid", Node.class).setParameter("uuid", uuid).getSingleResult();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return n;
    }

    @Override
    public void saveNode(Node node) {
        try {
            em.merge(node);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public List<Node> getNonAttachedNodes() {
        List<Node> n = null;
        try {
            n = em.createNamedQuery("Node.findByCustomerIsNull", Node.class).getResultList();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return n;
    }

    @Override
    public List<Node> getAllNodes() {
        List<Node> n = null;
        try {
            n = em.createNamedQuery("Node.findAll", Node.class).getResultList();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return n;
    }

    @Override
    public boolean isUuidUnique(String mac) {
        // Query for a MAC
        TypedQuery<Node> query = em.createNamedQuery("Node.findByUuid", Node.class);
        query.setParameter(
                "uuid", mac);
        try {
            List results = query.getResultList();
            if (results.isEmpty()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Instrument Related Functionality">
    @Override
    public void attachInstrumentToNode(Instrument instrument, Node node) {
        instrument.setEnabled(true);
        instrument.setNode(node);
        node.getInstruments().add(instrument);

        // Create variables if neccesary
        if (node.getCustomer() != null) {
            this.setupVariables(instrument.getDataSource(), node.getCustomer());
        }
    }
    /*
     * Instrument have to be managed by an entity manager
     */

    private void detachInstrumentFromNode(Instrument instrument, Node node) {
        instrument.setEnabled(false);
        node.getInstruments().remove(instrument);
        if (instrument.getNode().equals(node)) {
            instrument.setNode(null);
        }
    }

    /*
     * Creates a DB state for a new instrument and updates reverse relations to related objects.
     * Instrument object should be initialized so
     * that it has uuid, data source and data source type assigned prior invocating this method.
     * Id should be null, since it is autogenerated by the persistance provider.
     * 
     */
    @Override
    public Instrument createInstrument(String uuid, int typeId) throws UuidNotUniqueException {
        if (!this.isUuidTaken(uuid)) {
            DataSourceType dst = this.getDataSourceType(typeId);
            Instrument instrument = new Instrument(uuid, dst);
            this.em.persist(instrument);
            return instrument;
        } else {
            throw new UuidNotUniqueException();
        }
    }

    @Override
    public Instrument createInstrument(String uuid, int typeId, int nodeId) throws UuidNotUniqueException {
        Instrument instrument = this.createInstrument(uuid, typeId);
        this.attachInstrumentToNode(instrument, this.getNode(nodeId));
        return instrument;
    }

    @Override
    public void updateInstrument(int id, Integer nodeId) {
        Instrument instrument = this.getInstrument(id);
        Node oldNode = instrument.getNode();
        Node newNode = (nodeId != null) ? this.getNode(nodeId) : null;

        //Manage changes in nodes
        if (!((oldNode == newNode)
                || (oldNode != null
                && newNode != null
                && oldNode.equals(newNode)))) {

            // Detach from current node
            if (oldNode != null) {
                this.detachInstrumentFromNode(instrument, oldNode);
            }

            // Attach to a new node
            if (newNode != null) {
                this.attachInstrumentToNode(instrument, newNode);
            }
        }
    }

    @Override
    public void deleteInstrument(int id) throws NoSuchEntityException {
        try {
            Instrument i = em.find(Instrument.class, id);
            em.remove(i);
        } catch (IllegalArgumentException ex) {
            throw new NoSuchEntityException(ex);
        }
    }

    @Override
    public Instrument getInstrument(int id) {
        return this.em.find(Instrument.class, id);
    }

    @Override
    public Instrument getInstrument(String uuid) {
        try {
            return em.createNamedQuery("Instrument.findByUuid", Instrument.class).setParameter("uuid", uuid).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public void saveInstrument(Instrument instrument) {
        em.merge(instrument);
    }

    @Override
    public List<Instrument> getInstruments() {
        return em.createNamedQuery("Instrument.findAll", Instrument.class).getResultList();
    }

    @Override
    public List<Instrument> getInstrumentsByUser(int userId) {
        try {
            return em.createNamedQuery("Instrument.findByCustomerId", Instrument.class).setParameter("customerId", this.getUser(userId).getCustomer().getId()).getResultList();
        } catch (NullPointerException ex) {
            return null;
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }

    @Override
    public List<Instrument> getNonAttachedInstruments() {
        return em.createNamedQuery("Instrument.findByNodeIsNull", Instrument.class).getResultList();
    }

    @Override
    public boolean isUuidTaken(String uuid) {
        // Query for an UUID
        return !em.createNamedQuery("Instrument.findByUuid", Instrument.class).setParameter("uuid", uuid).getResultList().isEmpty();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Data Source Type Related Functionality">
    @Override
    public void createDataSourceType(DataSourceType dataSourceType) {
        // Make the data source type managed.
        this.em.persist(dataSourceType);
        // Set data schema
        dataSourceType.setDataSchema(this.getDataSchema(dataSourceType.getDataSchema().getId()));
        // Generate uuid if empty
        if (dataSourceType.getUuid() == null || dataSourceType.getUuid().isEmpty()) {
            dataSourceType.setUuid(UUID.randomUUID().toString());
        }
    }

    @Override
    public void updateDataSourceType(DataSourceType dataSourceType) {
        this.em.merge(dataSourceType);
    }

    @Override
    public void createOrUpdateDataSourceType(DataSourceType dataSourceType) {
        this.em.persist(dataSourceType);
    }

    @Override
    public void deleteDataSourceType(int id) {
        try {
            em.remove(em.find(DataSourceType.class, id));
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public DataSourceType getDataSourceType(int id) {
        try {
            return em.find(DataSourceType.class, id);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

//    @Override
//    public void saveDataSourceType(DataSourceType instrumentType) {
//        try {
//            if (instrumentType.getId() != null) {
//                em.merge(instrumentType);
//            } else {
//                em.persist(instrumentType);
//            }
//        } catch (Exception ex) {
//            throw new EJBException(ex);
//        }
//    }
    @Override
    public List<DataSourceType> getDataSourceTypes() {
        List<DataSourceType> it = null;
        try {
            it = em.createNamedQuery("DataSourceType.findAll", DataSourceType.class).getResultList();
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        return it;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variable & ValueGroup Related Functionality">
    private void setupValueGroups(Variable variable) {
        // Setup value groups for a variable
        List<ValueGroup> valueGroups = new ArrayList<ValueGroup>();
        for (LogLevels.LogLevel level : LogLevels.levels) {
//            if (level.isEnabled()) {
            ValueGroup valueGroup = new ValueGroup();
            em.persist(valueGroup);
            valueGroup.setSamplingPeriod(level.getSamplingPeriod());
            valueGroup.setRotationPeriod(level.getRotationPeriod());
            valueGroup.setEnabled(level.isEnabled());
            valueGroup.setVariable(variable);
            valueGroups.add(valueGroup);
//            }
        }
        variable.setValueGroups(valueGroups);
    }

    @Override
    public void setupVariables(DataSource dataSource, Customer customer) {
        // Create variables if needed
        Collection<VariableType> variableTypes = dataSource.getType().getDataSchema().getSchemaElementCollection();
        for (VariableType variableType : variableTypes) {
            TypedQuery<Variable> query = em.createNamedQuery("Variable.findByCustomerAndDataSourceAndType", Variable.class);
            query.setParameter("customer", customer);
            query.setParameter("dataSource", dataSource);
            query.setParameter("type", variableType);
            List<Variable> variables = query.getResultList();

            if (variables.isEmpty()) {
                // Setup variable
                Variable variable = new Variable();
                em.persist(variable);
                variable.setCustomer(customer);
                customer.getVariables().add(variable);
                variable.setDataSource(dataSource);
                dataSource.getVariables().add(variable);
                variable.setType(variableType);
                variable.setUuid(UUID.randomUUID().toString());

                // Map a variable to a stream. This only a temporary sollution and will be removed when 
                // UI has editor for mapping streams
                if (!variable.getType().isHidden()) {
                    Stream stream = new Stream();
                    stream.setName(variable.getType().getName());
                    stream.setDescription(variable.getType().getDescription());
                    stream.setVariable(variable);
                    stream.setUserSettings(customer.getUser().getUserSettings());
                    customer.getUser().getUserSettings().getStreams().add(stream);
                }

                this.setupValueGroups(variable);
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Residence Related Functionality">
    @Override
    public void addResidence(Residence residence) {
        try {
            em.persist(residence);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateResidence(Residence residence) {
        try {
            em.merge(residence);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }

        //throw new UnsupportedOperationException("Not supported yet.");
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Log Related Functionality">
    @Override
    public List<Value> getValues(User user, int instrument_id, VariableType schemaElement, Long logLevel, Date from, Date to) throws Exception {
        Instrument instrument = em.find(Instrument.class, instrument_id);
        if (instrument != null) {
            Variable variable = instrument.getDataSource().getVariable(schemaElement);
            ValueGroup valueGroup = variable.getValueGroup(logLevel);

            // Resolve data type for the log
            Class dataTypeClass;
            try {
                dataTypeClass = Class.forName(schemaElement.getDataType()).asSubclass(Value.class);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
                // TODO: Should exception be thrown instead?!
                return null;
            }

            if (valueGroup != null) {
                try {
                    // Get the log
                    TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass);
                    query.setParameter("valueGroup", valueGroup);
                    query.setParameter("from", from);
                    query.setParameter("to", to);
                    return query.getResultList();
                } catch (Exception ex) {
                    Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
                    throw new Exception();
                }
            } else {
                // TODO: ThrowNoSuchLogLevelException
                throw new Exception();
            }
        } else {
            // TODO: Throw InstrumentDoesNotExistException
            throw new Exception();
        }
    }

    @Override
    public List<Value> getValues(int streamId, Long logLevel, Date from, Date to) throws Exception {
        Stream stream = this.getStream(streamId);
        // Resolve data type for the log
        Class dataTypeClass = Class.forName(stream.getVariable().getType().getDataType()).asSubclass(Value.class);

        try {
            // Get the log
            TypedQuery<Value> query = em.createNamedQuery(dataTypeClass.getSimpleName() + ".findTimeMeasuredRangeByData", dataTypeClass);
            query.setParameter("valueGroup", stream.getVariable().getValueGroup(this.getNearestLogLevel(logLevel)));
            query.setParameter("from", from.getTime());
            query.setParameter("to", to.getTime());
            return query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(BusinessBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(); // TODO: Define more spesific exception.
        }
    }

    @Override
    public Long getNearestLogLevel(Long normalizationPeriod) {
        Long previous = null;
        Long current;
        Long delta = null;

        for (LogLevel level : LogLevels.levels) {
            if (level.isEnabled()) {
                Long sp = level.getSamplingPeriod();
                if (normalizationPeriod <= sp) { // iterated over all interesting log levels --> method will return in all cases
                    if (delta != null) {
                        Long tmp_delta = Math.abs(normalizationPeriod - sp);
                        if (tmp_delta > delta) { // the previous log level is the nearest one
                            return previous;
                        } else {
                            return sp;
                        }
                    } else { // the requested sampling period is shorter than the shortest log level --< return the first log level
                        return sp;
                    }
                } else {
                    delta = Math.abs(normalizationPeriod - sp);
                    previous = sp;
                }
            }
        }
        return previous;
    }

//    // TODO: Fix: Also customer should be taken into account for security reasins when retrieving ValueContect!
//    @Override
//    public List<IntMeasurement> getLog(int instrument_id, Long logLevel, Date from, Date to) {
//
//        // Get value context for the instrument
//        Instrument inst = em.find(Instrument.class, instrument_id);
//        TypedQuery<ValueContext> contextQuery = em.createNamedQuery("ValueContext.findByInstrumentAndRate", ValueContext.class);
//
//        contextQuery.setParameter(
//                "instrument", inst);
//        contextQuery.setParameter(
//                "rate", logLevel);
//        ValueContext mc;
//
//
//        try {
//            mc = contextQuery.getSingleResult();
//        } catch (Exception ex) {
//            return null;
//        }
//        // Create query for measurements
//        TypedQuery<IntMeasurement> query = em.createNamedQuery("IntMeasurement.findFrombyValueContext", IntMeasurement.class);
//        query.setParameter("valueContext", mc);
//        query.setParameter("from", from);
//
//        try {
//            return query.getResultList();
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//    @Override
//    public void addLog(User user, LogLevel logLevel, List<IntMeasurement> measurements) {
//        throw new UnsupportedOperationException("Not supported yet.");
//    }
//    @Deprecated
//    @Override
//    public List<Value> getLog(User user, int instrument_id, Long logLevel, Date from, Date to) {
//        // Get a value context for the instrument with a given log level
//        Instrument inst = em.find(Instrument.class, instrument_id);
//        TypedQuery<ValueContext> contextQuery = em.createNamedQuery("ValueContext.findByInstrumentAndRate", ValueContext.class);
//
//        contextQuery.setParameter(
//                "instrument", inst);
//        contextQuery.setParameter(
//                "rate", logLevel);
//        ValueContext mc;
//
//
//        try {
//            mc = contextQuery.getSingleResult();
//        } catch (Exception ex) {
//            return null;
//        }
//
//        // Check for authorization and get the log
//        if (isUserAuthorizedForLog(user, mc)) {
//            // Create query for measurements
//            TypedQuery<Value> query = em.createNamedQuery("Value.findTimeMeasuredRangeByValueContext", Value.class);
//            query.setParameter("valueContext", mc);
//            query.setParameter("from", from);
//            query.setParameter("to", to);
//
//            try {
//                return query.getResultList();
//            } catch (Exception ex) {
//                return null;
//            }
//        } else {
//            // TODO: Should throw an exception: User not authorized!
//            return null;
//        }
//    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Stream Related Functionality">
    @Override
    public Stream getStream(int id) {
        return this.em.find(Stream.class, id);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Schema Related Functionality">
    @Override
    public List<DataSchema> getDataSchemas() {
        Query query = em.createNamedQuery("DataSchema.findAll");
//        try {
        List<DataSchema> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        } else {
            return results;
        }
//        } catch (Exception ex) {
//            return null;
//        }
    }

    @Override
    public DataSchema getDataSchema(int id) {
        try {
            return this.em.find(DataSchema.class, id);
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void saveDataSchema(DataSchema schema) {
        try {
            em.merge(schema);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public void createDataSchema(String name, String description, List<VariableType> elements) {
        ListDataSchema schema = new ListDataSchema(name, description);
        // Bind elements to the schema      
        for (VariableType element : elements) {
            element.setDataSchema(schema);
            // Initialize value group settings
            element.setValueGroupSettings(new ArrayList<ValueGroupSettings>());
            for (LogLevels.LogLevel logLevel : LogLevels.levels) {
                ValueGroupSettings vgs = new ValueGroupSettings();
                vgs.setSamplingPeriod(logLevel.getSamplingPeriod());
                vgs.setRotationPeriod(logLevel.getRotationPeriod());
                vgs.setEnabled(logLevel.isEnabled());
                vgs.setVariableType(element);
                element.getValueGroupSettings().add(vgs);
            }

            // Generate uuid if empty
            if (element.getUuid() == null || element.getUuid().isEmpty()) {
                element.setUuid(UUID.randomUUID().toString());
            }
        }
        schema.setElements(elements);
        // Persist schema. Elements will also be persisted via cascade rules
        this.em.persist(schema);

        // Generate uuid if empty
        if (schema.getUuid() == null || schema.getUuid().isEmpty()) {
            schema.setUuid(UUID.randomUUID().toString());
        }
    }

    @Override
    public void updateDataSchema(Integer id, String name, String description, List<VariableType> elements) {
        ListDataSchema schema = (ListDataSchema) this.getDataSchema(id);
        schema.setName(name);
        schema.setDescription(description);

        // Remove deleted elements
        if (!elements.containsAll(schema.getElements())) {
            for (VariableType element : schema.getElements()) {
                if (!elements.contains(element)) {
                    schema.getElements().remove(element);
                    this.deleteSchemaElement(element);
                }
            }
        }
        // Update elements
        for (VariableType element : elements) {
            if (schema.getElements().contains(element)) {
                this.copyVariableType(element, schema.getElements().get(schema.getElements().indexOf(element)));
            } else {
                addVariableTypeToSchema(schema.getId(), element);
            }
        }
        schema.setElements(elements);
        this.em.merge(schema);
    }

    @Override
    public void addVariableTypeToSchema(int id, String name, String description, String unitOfMeasure, Boolean hidden, Boolean store) {
        VariableType vt = new VariableType(name, description, unitOfMeasure, hidden, store);
        this.addVariableTypeToSchema(id, vt);
    }

    private void addVariableTypeToSchema(int id, VariableType vt) {
        ListDataSchema schema = (ListDataSchema) this.getDataSchema(id);
        // Bind elements to the schema      
        vt.setDataSchema(schema);
        schema.getElements().add(vt);
        // Initialize value group settings
        vt.setValueGroupSettings(new ArrayList<ValueGroupSettings>());
        for (LogLevels.LogLevel logLevel : LogLevels.levels) {
            ValueGroupSettings vgs = new ValueGroupSettings();
            vgs.setSamplingPeriod(logLevel.getSamplingPeriod());
            vgs.setRotationPeriod(logLevel.getRotationPeriod());
            vgs.setEnabled(logLevel.isEnabled());
            vgs.setVariableType(vt);
            vt.getValueGroupSettings().add(vgs);
        }
        this.em.persist(vt);
    }

    @Override
    public void removeVariableType(int id) {
        VariableType vt = this.getSchemaElement(id);
        vt.getDataSchema().getSchemaElementCollection().remove(vt);
        this.em.remove(vt);
    }

    @Override
    public void updateVariableType(int id, String name, String description, String unitOfMeasure, Boolean hidden, Boolean store) {
        VariableType vt = this.getSchemaElement(id);
        vt.setName(name);
        vt.setDescription(description);
        vt.setUnitOfMeasure(unitOfMeasure);
        vt.setHidden(hidden);
        vt.setStore(store);
    }

    private void copyVariableType(VariableType origin, VariableType copy) {
        copy.setName(origin.getName());
        copy.setDescription(origin.getDescription());
        copy.setUnitOfMeasure(origin.getUnitOfMeasure());
        copy.setHidden(origin.getHidden());
        copy.setStore(origin.getStore());
    }

    @Override
    public VariableType getSchemaElement(int id) {
        return this.em.find(VariableType.class, id);
    }

    @Override
    public void saveSchemaElement(VariableType element) {
        try {
            em.merge(element);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public void deleteDataSchema(int schemaId) {
        try {
            em.remove(em.find(DataSchema.class, schemaId));
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }

    @Override
    public void deleteSchemaElement(VariableType element) {
        try {
            em.remove(element);
        } catch (Exception ex) {
            throw new EJBException(ex);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variable Related Functionality">
    @Override
    public Variable getVariable(int id) {
        return this.em.find(Variable.class, id);
    }
    // </editor-fold>

    @Override
    public ValueGroup getValueGroup(int id) {
        return em.find(ValueGroup.class, id);
    }

    // SEE: http://spitballer.blogspot.com/2010/02/sending-email-via-glassfish-v3.html
    private void sendMail(String to, String subject, String messageBody) {
        // Create the message object
        Message message = new MimeMessage(mailSession);

        try {
            // Adjust the recipients. Here we have only one
            // recipient. The recipient's address must be
            // an object of the InternetAddress class.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to, false));

            // Set the message's subject
            message.setSubject(subject);

            // Insert the message's body
            message.setText(messageBody);

            // This is not mandatory, however, it is a good
            // practice to indicate the software which
            // constructed the message.
            message.setHeader("X-Mailer", "Asemo");

            // Adjust the date of sending the message
            Date timeStamp = new Date();
            message.setSentDate(timeStamp);

            // Use the 'send' static method of the Transport
            // class to send the message
            Transport.send(message);
        } catch (Exception e) {
            // TODO: throw sendMailException
        }
    }

    @Override
    public void setUuids(String className) {
        List<Uuid> results = this.em.createQuery("SELECT v FROM " + className + " v WHERE v.uuid IS NULL OR v.uuid = ''").getResultList();
        for (Uuid o : results) {
            o.setUuid(UUID.randomUUID().toString());
        }
    }
}
