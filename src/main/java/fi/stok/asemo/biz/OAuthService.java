/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz;

import fi.stok.asemo.biz.ejb.entity.schema.OAuthAccessCredentials;
import fi.stok.asemo.biz.ejb.entity.schema.OAuthClient;
import fi.stok.asemo.biz.ejb.entity.schema.OAuthAccessor;
import fi.stok.asemo.biz.ejb.entity.schema.OAuthRequestCredentials;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.exception.IncorrectPasswordException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.oauth.OAuth;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.SimpleOAuthValidator;
import net.oauth.server.OAuthServlet;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Juha Loukkola
 */
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class OAuthService {

    private static final OAuthValidator VALIDATOR = new SimpleOAuthValidator();

    @PersistenceContext
    private EntityManager em;

    public OAuthRequestCredentials getRequestCredentialsForRequest(OAuthMessage request)
            throws OAuthException, IOException, URISyntaxException {
        // Validate OAuth message.
        this.validateMessage(request,
                new net.oauth.OAuthAccessor(this.getConsumerByClientToken(request.getConsumerKey())));

        // Create an accessor for the request.
        OAuthAccessor peristentAccessor = new OAuthAccessor();
        peristentAccessor.setClient(getClientByToken(request.getConsumerKey()));
        peristentAccessor.setRequestCredentials(
                this.generateRequestCredentials(peristentAccessor.getClient().getToken()));

//        {
//            // Support the 'Variable Accessor Secret' extension
//            // described in http://oauth.pbwiki.com/AccessorSecret
//            String secret = request.getParameter("oauth_accessor_secret");
//            if (secret != null) {
//                accessor.setProperty(OAuthConsumer.ACCESSOR_SECRET, secret);
//            }
//        }
        //Persist accessor
        this.saveAccessor(peristentAccessor);

        return peristentAccessor.getRequestCredentials();
    }

    public OAuthAccessCredentials getAccessCredentialsForRequest(OAuthMessage request)
            throws OAuthException, IOException, URISyntaxException {

        OAuthAccessor accessor = this.getAccessorByRequestToken(request.getToken());

        // Validate OAuth message.
        this.validateMessage(request, this.accessorToAccessor(accessor));

        // Make sure token is authorized.
        if (!accessor.getRequestCredentials().isAuthorized()) {
            throw new OAuthProblemException("permission_denied");
        }
        // Set access credentials
        accessor.setAccessCredentials(
                this.generateAccessCredentials(accessor.getClient().getToken(), 1000l * 60l * 60l * 24l * 365l * 10l));
        // TODO: Delete request credentials
        OAuthRequestCredentials rc = accessor.getRequestCredentials();
        accessor.setRequestCredentials(null);
        this.em.remove(rc);

        return accessor.getAccessCredentials();
    }

    public void authorize(String username, String password, OAuthAccessor accessor) throws IncorrectPasswordException, UserNotFoundException, PasswordException {
        OAuthAccessor persistedAccessor = this.getAccessor(accessor.getId());
        User user = this.authenticateUser(username, password);
        persistedAccessor.setAsemoUser(user);
        persistedAccessor.getRequestCredentials().setAuthorized(Boolean.TRUE);
    }

    private User authenticateUser(String uname, String passwd) throws IncorrectPasswordException, UserNotFoundException, PasswordException {
        try {
            // Search db for user with specified "uname".
            User user = em.createNamedQuery("User.findByUsername", User.class).setParameter("username", uname).getSingleResult();
            // Validate password
            if (user.authenticate(passwd)) {
                return user;
            } else {
                throw new IncorrectPasswordException();
            }
        } catch (NoResultException ex) {
            throw new UserNotFoundException();
        }
    }

    public User authenticateRequest(HttpServletRequest request)
            throws IOException, OAuthProblemException, OAuthException, URISyntaxException {
        OAuthMessage requestMessage = OAuthServlet.getMessage(request, null);
        // Get accessor 
        OAuthAccessor accessor = this.getAccessorByAccessToken(requestMessage.getToken());
        // Validate OAuth message.
        this.validateMessage(requestMessage, this.accessorToAccessor(accessor));

        return accessor.getAsemoUser();
    }

    private Collection<OAuthClient> getClients() {
        return this.em.createNamedQuery("OAuthClient.findAll", OAuthClient.class).getResultList();
    }

    private OAuthClient getClientByToken(String token) {
        try {
            return this.em.createNamedQuery("OAuthClient.findByToken", OAuthClient.class)
                    .setParameter("token", token)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    private OAuthConsumer getConsumerByClientToken(String token) throws OAuthProblemException {
        try {
            return this.clientToConsumer(this.getClientByToken(token));
        } catch (NullPointerException ex) {
            throw new OAuthProblemException("token_rejected");
        }
    }

    private void saveAccessor(OAuthAccessor accessor) {
        this.em.persist(accessor);
    }
    
    private OAuthAccessor getAccessor(Long id) {
        return this.em.find(OAuthAccessor.class, id);
    }
    
    public OAuthAccessor getAccessorByRequestToken(String token) throws OAuthProblemException {
        try {
            return this.em.createNamedQuery("OAuthAccessor.findByRequestToken", OAuthAccessor.class)
                    .setParameter("token", token)
                    .getSingleResult();
        } catch (NoResultException ex) {
            throw new OAuthProblemException("token_expired");
        }
    }

    private OAuthAccessor getAccessorByAccessToken(String token) throws OAuthProblemException {
        try {
            OAuthAccessor accessor = this.em.createNamedQuery("OAuthAccessor.findByAccessToken", OAuthAccessor.class)
                    .setParameter("token", token)
                    .getSingleResult();
//            if (accessor.getAccessCredentials().getValidUntil().before(new Date())) {
//                // TODO: remove token
//                throw new OAuthProblemException("token_expired");
//            }
            return accessor;
        } catch (NoResultException ex) {
            throw new OAuthProblemException("token_expired");
        }
    }

    private void validateMessage(OAuthMessage message, net.oauth.OAuthAccessor accessor) throws OAuthException, IOException, URISyntaxException {
        OAuthService.VALIDATOR.validateMessage(message, accessor);
    }

    private OAuthRequestCredentials generateRequestCredentials(String consumerKey) throws OAuthException {
        // generate token and secret based on consumer_key
        // for now use md5 of name + current time as token
        String token_data = consumerKey + System.nanoTime();
        String token = DigestUtils.md5Hex(token_data);
        // for now use md5 of name + current time + token as secret
        String secret_data = consumerKey + System.nanoTime() + token;
        String secret = DigestUtils.md5Hex(secret_data);

        OAuthRequestCredentials credentials = new OAuthRequestCredentials();
        credentials.setToken(token);
        credentials.setSecret(secret);
        credentials.setAuthorized(Boolean.FALSE);

        return credentials;
    }

    private OAuthAccessCredentials generateAccessCredentials(String consumerKey, Long ttl) throws OAuthException {
        // generate token and secret based on consumer_key
        // for now use md5 of name + current time as token
        String token_data = consumerKey + System.nanoTime();
        String token = DigestUtils.md5Hex(token_data);

        // for now use md5 of name + current time + token as secret
        String secret_data = consumerKey + System.nanoTime() + token;
        String secret = DigestUtils.md5Hex(secret_data);

        OAuthAccessCredentials credentials = new OAuthAccessCredentials();
        credentials.setToken(token);
        credentials.setSecret(secret);
        Date foo = new Date();
        foo.setTime(foo.getTime() + ttl);
        credentials.setValidUntil(foo);

        return credentials;
    }

    private OAuthConsumer clientToConsumer(OAuthClient client) {
        return new OAuthConsumer(null, client.getToken(), client.getSecret(), null);
    }

    private net.oauth.OAuthAccessor accessorToAccessor(OAuthAccessor accessor) {
        net.oauth.OAuthAccessor a = new net.oauth.OAuthAccessor(this.clientToConsumer(accessor.getClient()));
        if (accessor.getAccessCredentials() != null && accessor.getAccessCredentials().getValidUntil().after(new Date())) {
            a.accessToken = accessor.getAccessCredentials().getToken();
            a.tokenSecret = accessor.getAccessCredentials().getSecret();
        } else if (accessor.getRequestCredentials() != null) {
            a.requestToken = accessor.getRequestCredentials().getToken();
            a.tokenSecret = accessor.getRequestCredentials().getSecret();
        }
        return a;
    }

    public static void handleException(
            Exception e,
            HttpServletRequest request,
            HttpServletResponse response,
            boolean sendBody)
            throws IOException, ServletException {
        String realm = (request.isSecure()) ? "https://" : "http://";
        realm += request.getLocalName();
        OAuthServlet.handleException(response, e, realm, sendBody);
    }
    
}
