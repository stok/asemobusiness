/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.exception;

/**
 *
 * @author Juha Loukkola
 */
public class NodeAlreadyRegistered extends Exception {

    public NodeAlreadyRegistered() {
    }

    public NodeAlreadyRegistered(Long msg) {
        super(msg.toString());
    }
}