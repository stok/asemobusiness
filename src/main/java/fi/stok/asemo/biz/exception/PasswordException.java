/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.exception;

/**
 *
 * @author Juha Loukkola
 */
public class PasswordException extends Exception{

    public PasswordException() {
    }

    public PasswordException(Long msg) {
        super(msg.toString());
    } 
}
