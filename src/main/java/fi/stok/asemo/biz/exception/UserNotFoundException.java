/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.exception;

/**
 *
 * @author Juha Loukkola
 */
public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
    }

    public UserNotFoundException(Long msg) {
        super(msg.toString());
    }
}
