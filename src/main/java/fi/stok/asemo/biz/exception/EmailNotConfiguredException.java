
package fi.stok.asemo.biz.exception;

/**
 *
 * @author Juha Loukkola
 */
public class EmailNotConfiguredException extends Exception {

    public EmailNotConfiguredException() {
    }

    public EmailNotConfiguredException(Long msg) {
        super(msg.toString());
    }
}
