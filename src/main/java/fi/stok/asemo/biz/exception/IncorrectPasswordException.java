/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author Juha Loukkola
 */
@ApplicationException
public class IncorrectPasswordException extends Exception {

    public IncorrectPasswordException() {
    }

    public IncorrectPasswordException(Long msg) {
        super(msg.toString());
    }
}
