/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.exception;

/**
 *
 * @author Juha Loukkola
 */
public class UuidNotUniqueException extends Exception{

    public UuidNotUniqueException() {
    }

    public UuidNotUniqueException(Long msg) {
        super(msg.toString());
    }
}
