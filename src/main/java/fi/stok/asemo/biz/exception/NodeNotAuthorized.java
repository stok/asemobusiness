/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.exception;

/**
 *
 * @author Juha Loukkola
 */
public class NodeNotAuthorized extends Exception {

    public NodeNotAuthorized() {
    }

    public NodeNotAuthorized(Long msg) {
        super(msg.toString());
    }
}
