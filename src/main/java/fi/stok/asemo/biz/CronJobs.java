package fi.stok.asemo.biz;

import fi.stok.asemo.biz.normalization.NormalizationJobRunner;
import fi.stok.asemo.biz.normalization.NormalizationCoordinator;
import java.util.concurrent.TimeUnit;
import javax.ejb.*;

/**
 *
 * @author Juha Loukkola
 */
@Singleton
@LocalBean
public class CronJobs {

    @EJB
    private NormalizationCoordinator coordinator;
    @EJB
    private NormalizationJobRunner runner;

    @Lock(LockType.WRITE)
    @AccessTimeout(value = 30, unit = TimeUnit.SECONDS)
    @Schedule(minute = "*/1", hour = "*")
    public void CronNormalize() {
        this.coordinator.updateSchedule();
        this.runner.runJobs(30000l);
    }

    @Lock(LockType.WRITE)
    @AccessTimeout(value = 30, unit = TimeUnit.SECONDS)
    @Schedule(minute = "*/30", hour = "*")
    public void cleanJobHistory() {
        this.coordinator.removeCompletedJobs(1l);
    }
}
