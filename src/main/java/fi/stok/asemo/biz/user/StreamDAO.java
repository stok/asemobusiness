/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.user;

import fi.stok.asemo.biz.ejb.entity.schema.Stream;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Juha Loukkola
 */
public interface StreamDAO {

    Stream getStream(int id);

    List<Stream> getStreams(int userId);

    List<Value> getValuesForStream(int streamId, Long logLevel, Date from, Date to) throws Exception;

    List<Value> getValuesForStream(int streamId, Long logLevel, Date from, Date to, int maxReuslts) throws Exception;
    
    Long getNearestLogLevel(int streamId, Long normalizationPeriod);
}
