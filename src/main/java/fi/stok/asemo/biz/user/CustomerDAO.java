/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.user;

import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Local;

/**
 *
 * @author Juha Loukkola
 */
@RolesAllowed({"USER", "ADMIN"})
@Local
public interface CustomerDAO {

    Customer getCustomer(int id);

    void setAddress(int id, String streetArddess, String zipcode, String city);
    
    void setEmail(int id, String email);
    
    void setPhone(int id, String phone);
    
    void setName(int id, String name);
}
