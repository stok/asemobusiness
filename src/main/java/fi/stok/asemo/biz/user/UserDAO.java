/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.user;

import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.exception.EmailNotConfiguredException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;

/**
 *
 * @author Juha Loukkola
 */
public interface UserDAO {

    User getUser(int id);

    User getUser(String username);

    void setUserName(int id, String userName);
    
    void setPassword(int id, String password);
    
    boolean isUsernameUnique(String uname);

    void requestNewPassword(String uname) throws UserNotFoundException, EmailNotConfiguredException, PasswordException;
}
