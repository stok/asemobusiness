/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.user;

import fi.stok.asemo.biz.ejb.entity.schema.Residence;

/**
 *
 * @author Juha Loukkola
 */
public interface ResidenceDAO {

    void createOrUpdateResidence(Residence residence);
}
