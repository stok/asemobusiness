/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.biz.user;

import javax.ejb.Local;

/**
 * A collecting facade for all user business methods.
 * 
 * @author Juha Loukkola
 */
@Local
public interface UserBusiness extends CustomerDAO, UserDAO, ResidenceDAO, StreamDAO, TimeSeriesDAO{

}
